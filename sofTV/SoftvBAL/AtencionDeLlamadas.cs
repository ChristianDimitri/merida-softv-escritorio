using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using Softv.Providers;
using Softv.Entities;

namespace Softv.BAL
{
  /// <summary>
  /// Class                   : Softv.BAL.AtencionDeLlamadas.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : AtencionDeLlamadas entity
  /// File                    : AtencionDeLlamadasEntity.cs
  /// Creation date           : 25/05/2011
  /// Creation time           : 01:00:28 p.m.
  /// </summary>
  [DataObject]
  [Serializable]
  public class AtencionDeLlamadas
  {

      #region attributes

      private long?  _clv_llamada;
      /// <summary>
      /// clv_llamada
      /// </summary>
      public long? clv_llamada
      {
          get { return _clv_llamada; }
          set { _clv_llamada = value; }
      }

      private int?  _Clv_Usuario;
      /// <summary>
      /// Clv_Usuario
      /// </summary>
      public int? Clv_Usuario
      {
          get { return _Clv_Usuario; }
          set { _Clv_Usuario = value; }
      }

      private long?  _Contrato;
      /// <summary>
      /// Contrato
      /// </summary>
      public long? Contrato
      {
          get { return _Contrato; }
          set { _Contrato = value; }
      }

      private String _Descripcion;
      /// <summary>
      /// Descripcion
      /// </summary>
      public String Descripcion
      {
          get { return _Descripcion; }
          set { _Descripcion = value; }
      }

      private String _Solucion;
      /// <summary>
      /// Solucion
      /// </summary>
      public String Solucion
      {
          get { return _Solucion; }
          set { _Solucion = value; }
      }

      private String _HoraInicial;
      /// <summary>
      /// HoraInicial
      /// </summary>
      public String HoraInicial
      {
          get { return _HoraInicial; }
          set { _HoraInicial = value; }
      }

      private String _HoraFinal;
      /// <summary>
      /// HoraFinal
      /// </summary>
      public String HoraFinal
      {
          get { return _HoraFinal; }
          set { _HoraFinal = value; }
      }

      private DateTime?  _Fecha;
      /// <summary>
      /// Fecha
      /// </summary>
      public DateTime? Fecha
      {
          get { return _Fecha; }
          set { _Fecha = value; }
      }

      private int?  _Clv_trabajo;
      /// <summary>
      /// Clv_trabajo
      /// </summary>
      public int? Clv_trabajo
      {
          get { return _Clv_trabajo; }
          set { _Clv_trabajo = value; }
      }

      private long?  _clv_queja;
      /// <summary>
      /// clv_queja
      /// </summary>
      public long? clv_queja
      {
          get { return _clv_queja; }
          set { _clv_queja = value; }
      }

      private int?  _CLV_TIPSER;
      /// <summary>
      /// CLV_TIPSER
      /// </summary>
      public int? CLV_TIPSER
      {
          get { return _CLV_TIPSER; }
          set { _CLV_TIPSER = value; }
      }

      private int?  _Turno;
      /// <summary>
      /// Turno
      /// </summary>
      public int? Turno
      {
          get { return _Turno; }
          set { _Turno = value; }
      }
      #endregion

      #region Constructors
      public AtencionDeLlamadas(){}

      public AtencionDeLlamadas(long? clv_llamada, int? Clv_Usuario, long? Contrato, String Descripcion, String Solucion, String HoraInicial, String HoraFinal, DateTime? Fecha, int? Clv_trabajo, long? clv_queja, int? CLV_TIPSER, int? Turno)
      {
          this._clv_llamada = clv_llamada;
          this._Clv_Usuario = Clv_Usuario;
          this._Contrato = Contrato;
          this._Descripcion = Descripcion;
          this._Solucion = Solucion;
          this._HoraInicial = HoraInicial;
          this._HoraFinal = HoraFinal;
          this._Fecha = Fecha;
          this._Clv_trabajo = Clv_trabajo;
          this._clv_queja = clv_queja;
          this._CLV_TIPSER = CLV_TIPSER;
          this._Turno = Turno;
      }
      #endregion

      #region static Methods

      /// <summary>
      ///Adds AtencionDeLlamadas
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Insert, true)]
     public static int Add(long? clv_llamada, int? Clv_Usuario, long? Contrato, String Descripcion, String Solucion, String HoraInicial, String HoraFinal, DateTime? Fecha, int? Clv_trabajo, long? clv_queja, int? CLV_TIPSER, int? Turno)
      {
          AtencionDeLlamadasEntity entity_AtencionDeLlamadas = new AtencionDeLlamadasEntity();
          entity_AtencionDeLlamadas.clv_llamada = clv_llamada;
          entity_AtencionDeLlamadas.Clv_Usuario = Clv_Usuario;
          entity_AtencionDeLlamadas.Contrato = Contrato;
          entity_AtencionDeLlamadas.Descripcion = Descripcion;
          entity_AtencionDeLlamadas.Solucion = Solucion;
          entity_AtencionDeLlamadas.HoraInicial = HoraInicial;
          entity_AtencionDeLlamadas.HoraFinal = HoraFinal;
          entity_AtencionDeLlamadas.Fecha = Fecha;
          entity_AtencionDeLlamadas.Clv_trabajo = Clv_trabajo;
          entity_AtencionDeLlamadas.clv_queja = clv_queja;
          entity_AtencionDeLlamadas.CLV_TIPSER = CLV_TIPSER;
          entity_AtencionDeLlamadas.Turno = Turno;
          int result = ProviderSoftv.AtencionDeLlamadas.AddAtencionDeLlamadas(entity_AtencionDeLlamadas);
          return result;
      }

      /// <summary>
      /// Delete AtencionDeLlamadas
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Delete, true)]
      public static int Delete(long? clv_llamada)
      {
          int resultado = ProviderSoftv.AtencionDeLlamadas.DeleteAtencionDeLlamadas(clv_llamada);
          return resultado;
      }

      /// <summary>
      /// Edit AtencionDeLlamadas
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Update, true)]
      public static int Edit(long? clv_llamada, int? Clv_Usuario, long? Contrato, String Descripcion, String Solucion, String HoraInicial, String HoraFinal, DateTime? Fecha, int? Clv_trabajo, long? clv_queja, int? CLV_TIPSER, int? Turno)
      {
    
          AtencionDeLlamadasEntity entity_AtencionDeLlamadas = new AtencionDeLlamadasEntity();
          entity_AtencionDeLlamadas.clv_llamada = clv_llamada;
          entity_AtencionDeLlamadas.Clv_Usuario = Clv_Usuario;
          entity_AtencionDeLlamadas.Contrato = Contrato;
          entity_AtencionDeLlamadas.Descripcion = Descripcion;
          entity_AtencionDeLlamadas.Solucion = Solucion;
          entity_AtencionDeLlamadas.HoraInicial = HoraInicial;
          entity_AtencionDeLlamadas.HoraFinal = HoraFinal;
          entity_AtencionDeLlamadas.Fecha = Fecha;
          entity_AtencionDeLlamadas.Clv_trabajo = Clv_trabajo;
          entity_AtencionDeLlamadas.clv_queja = clv_queja;
          entity_AtencionDeLlamadas.CLV_TIPSER = CLV_TIPSER;
          entity_AtencionDeLlamadas.Turno = Turno;
          int result = ProviderSoftv.AtencionDeLlamadas.EditAtencionDeLlamadas(entity_AtencionDeLlamadas);
          return result;
      }

      /// <summary>
      /// Get AtencionDeLlamadas
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Select, true)]
      public static List<AtencionDeLlamadas> GetAll()
      {
          List<AtencionDeLlamadas> list = new List<AtencionDeLlamadas>();
          List<AtencionDeLlamadasEntity> entities = new List<AtencionDeLlamadasEntity>();
          entities = ProviderSoftv.AtencionDeLlamadas.GetAtencionDeLlamadas();
          if(entities != null)
          {
              foreach(AtencionDeLlamadasEntity gAtencionDeLlamadas in  entities)
              {
                  AtencionDeLlamadas entity_AtencionDeLlamadas = new AtencionDeLlamadas(gAtencionDeLlamadas.clv_llamada, gAtencionDeLlamadas.Clv_Usuario, gAtencionDeLlamadas.Contrato, gAtencionDeLlamadas.Descripcion, gAtencionDeLlamadas.Solucion, gAtencionDeLlamadas.HoraInicial, gAtencionDeLlamadas.HoraFinal, gAtencionDeLlamadas.Fecha, gAtencionDeLlamadas.Clv_trabajo, gAtencionDeLlamadas.clv_queja, gAtencionDeLlamadas.CLV_TIPSER, gAtencionDeLlamadas.Turno);
                  list.Add(entity_AtencionDeLlamadas);
              }
          }
          return list;
      }

      /// <summary>
      /// ConsultaPorId AtencionDeLlamadas
      /// </summary>
      [DataObjectMethod(DataObjectMethodType.Select)]
      public static AtencionDeLlamadas GetOne(long? clv_llamada)
      {
          AtencionDeLlamadas entity_AtencionDeLlamadas = null;
          AtencionDeLlamadasEntity result = ProviderSoftv.AtencionDeLlamadas.GetAtencionDeLlamadasById(clv_llamada);
          if (result != null)
              entity_AtencionDeLlamadas = new AtencionDeLlamadas(result.clv_llamada, result.Clv_Usuario, result.Contrato, result.Descripcion, result.Solucion, result.HoraInicial, result.HoraFinal, result.Fecha, result.Clv_trabajo, result.clv_queja, result.CLV_TIPSER, result.Turno);
          return entity_AtencionDeLlamadas;
      }

      public AtencionDeLlamadas Instance()
      {
          return new AtencionDeLlamadas();
      }
      #endregion

      #region Instance Methods
      /// <summary>
      ///Adds AtencionDeLlamadas
      /// </summary>
      public int Add()
      {
          int result = Add(this.clv_llamada, this.Clv_Usuario, this.Contrato, this.Descripcion, this.Solucion, this.HoraInicial, this.HoraFinal, this.Fecha, this.Clv_trabajo, this.clv_queja, this.CLV_TIPSER, this.Turno);
          return result;
      }

      /// <summary>
      /// Delete AtencionDeLlamadas
      /// </summary>
      public int Delete()
      {
          int result = Delete(this.clv_llamada);
          return result;
      }
      /// <summary>
      /// Edit AtencionDeLlamadas
      /// </summary>
      public int Edit()
      {
          int result = Edit(this.clv_llamada, this.Clv_Usuario, this.Contrato, this.Descripcion, this.Solucion, this.HoraInicial, this.HoraFinal, this.Fecha, this.Clv_trabajo, this.clv_queja, this.CLV_TIPSER, this.Turno);
          return result;
      }
      #endregion
  }
}
