﻿Imports System.Data.SqlClient

Public Class FrmHub

    Private Desc_sector As String = Nothing
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing

    Private Sub damedatosbitacora()
        Try
            If eOpcion = "M" Then
                Desc_sector = Me.DescripcionTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub guarda_bitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If eOpcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nuevo HUB", " ", "Nuevo HUB: " + Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
                    ElseIf eOpcion = "M" Then
                        'Desc_sector = Me.DescripcionTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name, Desc_sector, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Nueva Colonia Para un HUB", " ", "Nueva una Colonia Para El HUB: " + Me.NombreComboBox.Text, LocClv_Ciudad)
                Case 2
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Elimino Colonia Para un HUB", " ", "Elimino una Colonia Para El HUB: " + Me.Clv_ColoniaTextBox.Text, LocClv_Ciudad)
                Case 3
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino un HUB", " ", "Se Elimino el HUB: " + Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub ConsultarHUB(ByVal ClvHub As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim reader As SqlDataReader

        Try

            Dim comando As New SqlCommand("ConHub", conexion)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Clv_Sector", ClvHub)
            comando.Parameters("@Clv_Sector").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Clv_Txt", "")
            comando.Parameters("@Clv_Txt").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Descripcion", "")
            comando.Parameters("@Descripcion").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Op", 3)
            comando.Parameters("@Op").Direction = ParameterDirection.Input

            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read)
                Clv_TxtTextBox.Text = reader(1).ToString()
                DescripcionTextBox.Text = reader(2).ToString()
            End While

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub ConsultarRelcoloniaHub(ByVal ClvHub As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim dtConRelHubColonia As New DataTable
        Dim daConRelHubColonia As New SqlDataAdapter

        Try
            Dim comando2 As New SqlCommand("ConRelHubColonia", conexion)
            comando2.CommandType = CommandType.StoredProcedure

            comando2.Parameters.AddWithValue("@Clv_Sector", ClvHub)
            comando2.Parameters("@Clv_Sector").Direction = ParameterDirection.Input

            daConRelHubColonia = New SqlDataAdapter(comando2)

            conexion.Open()
            daConRelHubColonia.Fill(dtConRelHubColonia)

            ConRelSectorColoniaDataGridView.DataSource = dtConRelHubColonia
            ConRelSectorColoniaDataGridView.AutoGenerateColumns = False
            ConRelSectorColoniaDataGridView.Columns("Clv_RelSecCol").Visible = False
            ConRelSectorColoniaDataGridView.Columns("Clv_Sector").Visible = False
            ConRelSectorColoniaDataGridView.Columns("Clv_Colonia").Visible = False
            ConRelSectorColoniaDataGridView.Columns("Nombre").Visible = True
            ConRelSectorColoniaDataGridView.Columns(0).Width = 100
            ConRelSectorColoniaDataGridView.Columns(1).Width = 100
            ConRelSectorColoniaDataGridView.Columns(2).Width = 100
            ConRelSectorColoniaDataGridView.Columns(3).Width = 400
            ConRelSectorColoniaDataGridView.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            With ConRelSectorColoniaDataGridView
                .Columns("Clv_RelSecCol").DataPropertyName = "Clv_RelSecCol"
                .Columns("Clv_Sector").DataPropertyName = "Clv_Sector"
                .Columns("Clv_Colonia").DataPropertyName = "Clv_Colonia"
                .Columns("Nombre").DataPropertyName = "Nombre"
            End With

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub EliminarHUB(ByVal ClvHub As Integer)

        Dim conexion As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlCommand("BorHub", conexion)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Clv_Sector", ClvHub)
            comando.Parameters("@Clv_Sector").Direction = ParameterDirection.Input

            comando.Parameters.Add("@Res", SqlDbType.Int)
            comando.Parameters("@Res").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 150)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output

            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = comando.Parameters("@Res").Value
            eMsg = comando.Parameters("@Msg").Value.ToString()

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub BorRelHubColonia(ByVal ClvSector As Integer, ByVal ClvColonia As Integer)

        Dim conexion As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlCommand("BorRelHubColonia", conexion)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Clv_Sector", ClvSector)
            comando.Parameters("@Clv_Sector").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Clv_Colonia", ClvColonia)
            comando.Parameters("@Clv_Colonia").Direction = ParameterDirection.Input

            comando.Parameters.Add("@Res", SqlDbType.Int)
            comando.Parameters("@Res").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 150)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output

            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = comando.Parameters("@Res").Value
            eMsg = comando.Parameters("@Msg").Value.ToString()

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub NueRelHubColonia(ByVal ClvSector As Integer, ByVal ClvColonia As Integer)

        Dim conexion As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlCommand("NueRelHubColonia", conexion)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Clv_Sector", ClvSector)
            comando.Parameters("@Clv_Sector").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Clv_Colonia", ClvColonia)
            comando.Parameters("@Clv_Colonia").Direction = ParameterDirection.Input

            comando.Parameters.Add("@Res", SqlDbType.Int)
            comando.Parameters("@Res").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 150)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output

            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = comando.Parameters("@Res").Value
            eMsg = comando.Parameters("@Msg").Value.ToString()

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub NuevoHUB(ByVal ClvTxt As String, ByVal Descripcion As String)

        Dim conexion As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlCommand("NueHub", conexion)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Clv_Txt", ClvTxt)
            comando.Parameters("@Clv_Txt").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Descripcion", Descripcion)
            comando.Parameters("@Descripcion").Direction = ParameterDirection.Input

            comando.Parameters.Add("@Res", SqlDbType.Int)
            comando.Parameters("@Res").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 150)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Clv_Sector", SqlDbType.BigInt)
            comando.Parameters("@Clv_Sector").Direction = ParameterDirection.Output


            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = comando.Parameters("@Res").Value
            eMsg = comando.Parameters("@Msg").Value.ToString()
            eClv_Sector = comando.Parameters("@Clv_Sector").Value

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub ModificarHUB(ByVal ClvSector As Integer, ByVal ClvTxt As String, ByVal Descripcion As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim reader As SqlDataReader

        Try

            Dim comando As New SqlCommand("ModHub", conexion)
            comando.CommandType = CommandType.StoredProcedure

            comando.Parameters.AddWithValue("@Clv_Sector", ClvSector)
            comando.Parameters("@Clv_Sector").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Clv_Txt", ClvTxt)
            comando.Parameters("@Clv_Txt").Direction = ParameterDirection.Input

            comando.Parameters.AddWithValue("@Descripcion", Descripcion)
            comando.Parameters("@Descripcion").Direction = ParameterDirection.Input

            comando.Parameters.Add("@Res", SqlDbType.Int)
            comando.Parameters("@Res").Direction = ParameterDirection.Output

            comando.Parameters.Add("@Msg", SqlDbType.VarChar, 150)
            comando.Parameters("@Msg").Direction = ParameterDirection.Output

            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = comando.Parameters("@Res").Value
            eMsg = comando.Parameters("@Msg").Value.ToString()

        Catch ex As Exception
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub MuestraColonia()
        Dim dtMuestraColonia As New DataTable
        Dim daMuestraColonia As New SqlDataAdapter

        Dim conexion As New SqlConnection(MiConexion)
        daMuestraColonia = New SqlDataAdapter("EXEC MuestraColoniaHub 0,0,0", conexion)
        daMuestraColonia.Fill(dtMuestraColonia)

        NombreComboBox.DataSource = dtMuestraColonia
        NombreComboBox.DisplayMember = "NOMBRE"
        NombreComboBox.ValueMember = "CLV_COLONIA"

    End Sub

    Private Sub FrmSectore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()

        MuestraColonia()

        If eOpcion = "N" Then
            Me.ToolStripButton2.Enabled = False
            Me.NombreComboBox.Enabled = False
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
        End If

        If eOpcion = "C" Then

            ConsultarHUB(eClv_Sector)
            ConsultarRelcoloniaHub(eClv_Sector)
            Me.BindingNavigator1.Enabled = False
            Me.Clv_TxtTextBox.ReadOnly = True
            Me.DescripcionTextBox.ReadOnly = True
            Me.Button1.Enabled = False
            Me.Button2.Enabled = False
            Me.NombreComboBox.Enabled = False
        End If

        If eOpcion = "M" Then
            ConsultarHUB(eClv_Sector)
            ConsultarRelcoloniaHub(eClv_Sector)
            Me.Clv_TxtTextBox.Enabled = False
            damedatosbitacora()
        End If
        CON.Close()
        If eOpcion = "N" Or eOpcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click

        eRes = 0
        eMsg = ""

        If Me.Clv_TxtTextBox.Text.Length = 0 Then
            MsgBox("Captura la Clave.", , "Atención")
            Exit Sub
        End If
        If Me.DescripcionTextBox.Text.Length = 0 Then
            MsgBox("Captura la Descripción.", , "Atención")
            Exit Sub
        End If

        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If eOpcion = "M" Then

                ModificarHUB(eClv_Sector, Me.Clv_TxtTextBox.Text, Me.DescripcionTextBox.Text)
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje5)
                    guarda_bitacora(0)
                End If
            End If


            If eOpcion = "N" Then

                NuevoHUB(Me.Clv_TxtTextBox.Text, Me.DescripcionTextBox.Text)
                If eRes = 1 Then
                    MsgBox(eMsg)

                Else
                    ConsultarHUB(eClv_Sector)
                    ConsultarRelcoloniaHub(eClv_Sector)
                    Me.BindingNavigator1.Enabled = False
                    Me.Clv_TxtTextBox.ReadOnly = True
                    Me.DescripcionTextBox.ReadOnly = True
                    Me.NombreComboBox.Enabled = True
                    Me.Button1.Enabled = True
                    Me.Button2.Enabled = True
                    eOpcion = "M"
                    MsgBox(mensaje5)
                    guarda_bitacora(0)
                End If
            End If


            CON.Close()
        Catch

        End Try
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click

        eRes = 0
        eMsg = ""
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            EliminarHUB(eClv_Sector)

            If eRes = 1 Then
                MsgBox(eMsg)
            Else
                guarda_bitacora(3)
                ConsultarRelcoloniaHub(eClv_Sector)
                MsgBox(mensaje6)
                Me.Close()
            End If
            CON.Close()
        Catch

        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Agregar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If Me.ConRelSectorColoniaDataGridView.RowCount > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            BorRelHubColonia(eClv_Sector, ConRelSectorColoniaDataGridView.CurrentRow.Cells(2).Value)
            If eRes = 1 Then
                MsgBox(eMsg)
            Else
                ConsultarRelcoloniaHub(eClv_Sector)
                guarda_bitacora(2)
            End If
            CON.Close()
        Else
            MsgBox("Seleccione una Colonia a Eliminar", , "Atención")
        End If
    End Sub

    Private Sub NombreComboBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

    Private Sub Agregar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        eRes = 0
        eMsg = ""
        NueRelHubColonia(eClv_Sector, CLng(Me.NombreComboBox.SelectedValue))
        If eRes = 1 Then
            MsgBox(eMsg)
        Else
            ConsultarRelcoloniaHub(eClv_Sector)
            guarda_bitacora(1)
        End If
        CON.Close()
    End Sub

    Private Sub NombreComboBox_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreComboBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Agregar()
        End If
    End Sub

End Class