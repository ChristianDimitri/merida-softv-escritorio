﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDireccionesPiratas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tbIdpirata = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TbNombre = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.labelNoext = New System.Windows.Forms.Label()
        Me.tbNoext = New System.Windows.Forms.TextBox()
        Me.labelNoint = New System.Windows.Forms.Label()
        Me.tbNoint = New System.Windows.Forms.TextBox()
        Me.labelEntrecalles = New System.Windows.Forms.Label()
        Me.tbEntrecalles = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbTel = New System.Windows.Forms.TextBox()
        Me.lbColonias = New System.Windows.Forms.Label()
        Me.cbColonias = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbCel = New System.Windows.Forms.TextBox()
        Me.labelCp = New System.Windows.Forms.Label()
        Me.tbCp = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cbCiudad = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cbTap = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CbCalle = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'tbIdpirata
        '
        Me.tbIdpirata.Enabled = False
        Me.tbIdpirata.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbIdpirata.Location = New System.Drawing.Point(12, 30)
        Me.tbIdpirata.Name = "tbIdpirata"
        Me.tbIdpirata.Size = New System.Drawing.Size(135, 22)
        Me.tbIdpirata.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(50, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Id Pirata"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(345, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Nombre"
        '
        'TbNombre
        '
        Me.TbNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.TbNombre.Location = New System.Drawing.Point(153, 30)
        Me.TbNombre.Name = "TbNombre"
        Me.TbNombre.Size = New System.Drawing.Size(456, 22)
        Me.TbNombre.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(167, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 15)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Calle"
        '
        'labelNoext
        '
        Me.labelNoext.AutoSize = True
        Me.labelNoext.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.labelNoext.Location = New System.Drawing.Point(382, 59)
        Me.labelNoext.Name = "labelNoext"
        Me.labelNoext.Size = New System.Drawing.Size(86, 15)
        Me.labelNoext.TabIndex = 8
        Me.labelNoext.Text = "Numero Ext."
        '
        'tbNoext
        '
        Me.tbNoext.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbNoext.Location = New System.Drawing.Point(365, 77)
        Me.tbNoext.Name = "tbNoext"
        Me.tbNoext.Size = New System.Drawing.Size(119, 22)
        Me.tbNoext.TabIndex = 7
        '
        'labelNoint
        '
        Me.labelNoint.AutoSize = True
        Me.labelNoint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.labelNoint.Location = New System.Drawing.Point(507, 59)
        Me.labelNoint.Name = "labelNoint"
        Me.labelNoint.Size = New System.Drawing.Size(82, 15)
        Me.labelNoint.TabIndex = 10
        Me.labelNoint.Text = "Numero Int."
        '
        'tbNoint
        '
        Me.tbNoint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbNoint.Location = New System.Drawing.Point(490, 77)
        Me.tbNoint.Name = "tbNoint"
        Me.tbNoint.Size = New System.Drawing.Size(119, 22)
        Me.tbNoint.TabIndex = 9
        '
        'labelEntrecalles
        '
        Me.labelEntrecalles.AutoSize = True
        Me.labelEntrecalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.labelEntrecalles.Location = New System.Drawing.Point(203, 104)
        Me.labelEntrecalles.Name = "labelEntrecalles"
        Me.labelEntrecalles.Size = New System.Drawing.Size(79, 15)
        Me.labelEntrecalles.TabIndex = 12
        Me.labelEntrecalles.Text = "Entrecalles"
        '
        'tbEntrecalles
        '
        Me.tbEntrecalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbEntrecalles.Location = New System.Drawing.Point(12, 122)
        Me.tbEntrecalles.Name = "tbEntrecalles"
        Me.tbEntrecalles.Size = New System.Drawing.Size(472, 22)
        Me.tbEntrecalles.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(519, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 15)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Teléfono"
        '
        'tbTel
        '
        Me.tbTel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbTel.Location = New System.Drawing.Point(490, 122)
        Me.tbTel.Name = "tbTel"
        Me.tbTel.Size = New System.Drawing.Size(119, 22)
        Me.tbTel.TabIndex = 13
        '
        'lbColonias
        '
        Me.lbColonias.AutoSize = True
        Me.lbColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.lbColonias.Location = New System.Drawing.Point(158, 148)
        Me.lbColonias.Name = "lbColonias"
        Me.lbColonias.Size = New System.Drawing.Size(63, 15)
        Me.lbColonias.TabIndex = 16
        Me.lbColonias.Text = "Colonias"
        '
        'cbColonias
        '
        Me.cbColonias.DisplayMember = "Colonia"
        Me.cbColonias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbColonias.FormattingEnabled = True
        Me.cbColonias.Location = New System.Drawing.Point(12, 166)
        Me.cbColonias.Name = "cbColonias"
        Me.cbColonias.Size = New System.Drawing.Size(347, 24)
        Me.cbColonias.TabIndex = 15
        Me.cbColonias.ValueMember = "Clv_Colonia"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(522, 148)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 15)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Celular"
        '
        'tbCel
        '
        Me.tbCel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbCel.Location = New System.Drawing.Point(490, 166)
        Me.tbCel.Name = "tbCel"
        Me.tbCel.Size = New System.Drawing.Size(119, 22)
        Me.tbCel.TabIndex = 19
        '
        'labelCp
        '
        Me.labelCp.AutoSize = True
        Me.labelCp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.labelCp.Location = New System.Drawing.Point(377, 148)
        Me.labelCp.Name = "labelCp"
        Me.labelCp.Size = New System.Drawing.Size(96, 15)
        Me.labelCp.TabIndex = 18
        Me.labelCp.Text = "Codigo Postal"
        '
        'tbCp
        '
        Me.tbCp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbCp.Location = New System.Drawing.Point(365, 166)
        Me.tbCp.Name = "tbCp"
        Me.tbCp.Size = New System.Drawing.Size(119, 22)
        Me.tbCp.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(52, 193)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(169, 15)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Ciudad/Region/Municipio"
        '
        'cbCiudad
        '
        Me.cbCiudad.DisplayMember = "Nombre"
        Me.cbCiudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbCiudad.FormattingEnabled = True
        Me.cbCiudad.Location = New System.Drawing.Point(12, 211)
        Me.cbCiudad.Name = "cbCiudad"
        Me.cbCiudad.Size = New System.Drawing.Size(247, 24)
        Me.cbCiudad.TabIndex = 21
        Me.cbCiudad.ValueMember = "Clv_ciudad"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(415, 193)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 15)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Tap"
        '
        'cbTap
        '
        Me.cbTap.DisplayMember = "clave"
        Me.cbTap.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbTap.FormattingEnabled = True
        Me.cbTap.Location = New System.Drawing.Point(265, 211)
        Me.cbTap.Name = "cbTap"
        Me.cbTap.Size = New System.Drawing.Size(344, 24)
        Me.cbTap.TabIndex = 23
        Me.cbTap.ValueMember = "Idtap"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Button1.Location = New System.Drawing.Point(385, 254)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 32)
        Me.Button1.TabIndex = 25
        Me.Button1.Text = "Guardar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Button2.Location = New System.Drawing.Point(500, 254)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(109, 32)
        Me.Button2.TabIndex = 26
        Me.Button2.Text = "Salir"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'CbCalle
        '
        Me.CbCalle.DisplayMember = "Nombre"
        Me.CbCalle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.CbCalle.FormattingEnabled = True
        Me.CbCalle.Location = New System.Drawing.Point(12, 75)
        Me.CbCalle.Name = "CbCalle"
        Me.CbCalle.Size = New System.Drawing.Size(347, 24)
        Me.CbCalle.TabIndex = 5
        Me.CbCalle.ValueMember = "Clv_calle"
        '
        'FrmDireccionesPiratas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(627, 298)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cbTap)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cbCiudad)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbCel)
        Me.Controls.Add(Me.labelCp)
        Me.Controls.Add(Me.tbCp)
        Me.Controls.Add(Me.lbColonias)
        Me.Controls.Add(Me.cbColonias)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbTel)
        Me.Controls.Add(Me.labelEntrecalles)
        Me.Controls.Add(Me.tbEntrecalles)
        Me.Controls.Add(Me.labelNoint)
        Me.Controls.Add(Me.tbNoint)
        Me.Controls.Add(Me.labelNoext)
        Me.Controls.Add(Me.tbNoext)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CbCalle)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TbNombre)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbIdpirata)
        Me.Name = "FrmDireccionesPiratas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Direcciones Piratas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbIdpirata As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TbNombre As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents labelNoext As System.Windows.Forms.Label
    Friend WithEvents tbNoext As System.Windows.Forms.TextBox
    Friend WithEvents labelNoint As System.Windows.Forms.Label
    Friend WithEvents tbNoint As System.Windows.Forms.TextBox
    Friend WithEvents labelEntrecalles As System.Windows.Forms.Label
    Friend WithEvents tbEntrecalles As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbTel As System.Windows.Forms.TextBox
    Friend WithEvents lbColonias As System.Windows.Forms.Label
    Friend WithEvents cbColonias As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbCel As System.Windows.Forms.TextBox
    Friend WithEvents labelCp As System.Windows.Forms.Label
    Friend WithEvents tbCp As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbTap As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents CbCalle As System.Windows.Forms.ComboBox
End Class
