Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmQuitaServicios
    Dim tipserv As Integer
    Private Sub FrmQuitaServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Function getValores() As String()
        Dim valores As String = ""
        Dim i As Integer
        For i = 0 To gvInfo.Rows.Count - 1
            If gvInfo.Rows(i).Cells(0).Value = True Then
                valores = valores + gvInfo.Rows(i).Cells("Clv_UnicaNet").Value.ToString + ","
            End If
        Next i
        Dim listaValores As String()
        listaValores = valores.Split(",")
        Return listaValores
    End Function

    Private Function Elimina(ByVal listaValores As String()) As Boolean
        Dim confirma As Boolean
        Dim conexion As SqlConnection = New SqlConnection(MiConexion)
        Dim query As String = ""
        Try
            conexion.Open()
            Dim i As Integer
            For i = 0 To listaValores.Length - 2
                query = "Exec QuitaServicios " + Me.tipserv.ToString + "," + listaValores(i)
                Dim com As SqlCommand = New SqlCommand(query, conexion)
                com.ExecuteNonQuery()
            Next i
            confirma = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            confirma = False
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
        Return confirma
    End Function

    Public Sub llenaGrid(ByVal tipoServ As Integer)
        Me.tipserv = tipoServ
        Dim conexion As New SqlConnection(MiConexion)
        Dim query As String = "Exec QuitaServicios " + tipoServ.ToString + ",0"
        Dim da As SqlDataAdapter = New SqlDataAdapter(query, conexion)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Try
            conexion.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            gvInfo.DataSource = bs
            Me.gvInfo.Columns(2).Width = 200
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
        Me.gvInfo.Columns("Clv_UnicaNet").Visible = False
        Me.gvInfo.Columns("Quitar").AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.gvInfo.Columns("Quitar").ReadOnly = False
        Me.gvInfo.Columns("Contrato").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gvInfo.Columns("Contrato").ReadOnly = True
        Me.gvInfo.Columns("Nombre").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        Me.gvInfo.Columns("Nombre").ReadOnly = True
        Me.gvInfo.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gvInfo.Columns("Descripcion").ReadOnly = True
        If Me.tipserv = 3 Then
            Me.gvInfo.Columns("Tipo").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            Me.gvInfo.Columns("Tipo").ReadOnly = True
        End If
        Me.gvInfo.Columns("Fecha").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gvInfo.Columns("Fecha").ReadOnly = True
    End Sub

    Private Sub btnProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
        If MsgBox("�Esta seguro que desea eliminar los servicios seleccionados?", MsgBoxStyle.OkCancel) = MsgBoxResult.Ok Then
            Dim valores As String() = Me.getValores()
            Dim confirma As Boolean = Me.Elimina(valores)
            If confirma = True Then
                MsgBox("Se han borrado los datos con �xito!", MsgBoxStyle.Information)
            End If
        End If
        Me.llenaGrid(Me.tipserv)
        Me.Refresh()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class