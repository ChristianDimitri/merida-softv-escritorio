﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConceptosPoliza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbSucursal = New System.Windows.Forms.RadioButton()
        Me.rbCliente = New System.Windows.Forms.RadioButton()
        Me.rbServicio = New System.Windows.Forms.RadioButton()
        Me.tbCuenta = New System.Windows.Forms.TextBox()
        Me.lbClave = New System.Windows.Forms.Label()
        Me.tbDescripcion = New System.Windows.Forms.TextBox()
        Me.tbPosicion = New System.Windows.Forms.TextBox()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Clv_Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripción = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Posición = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Indicador = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Normas_Reparto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.cbSucursales = New System.Windows.Forms.ComboBox()
        Me.tbBuscar = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextIndicador = New System.Windows.Forms.TextBox()
        Me.TextNormasReparto = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(563, 538)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(82, 27)
        Me.Button9.TabIndex = 15
        Me.Button9.Text = "Salir"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 81)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cuenta"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(35, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(137, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Descripción"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(35, 133)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 20)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Posición"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(50, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 16)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Tipo"
        '
        'rbSucursal
        '
        Me.rbSucursal.AutoSize = True
        Me.rbSucursal.Checked = True
        Me.rbSucursal.Location = New System.Drawing.Point(148, 11)
        Me.rbSucursal.Name = "rbSucursal"
        Me.rbSucursal.Size = New System.Drawing.Size(163, 17)
        Me.rbSucursal.TabIndex = 17
        Me.rbSucursal.TabStop = True
        Me.rbSucursal.Text = "Sucursal (Público en general)"
        Me.rbSucursal.UseVisualStyleBackColor = True
        '
        'rbCliente
        '
        Me.rbCliente.AutoSize = True
        Me.rbCliente.Location = New System.Drawing.Point(316, 11)
        Me.rbCliente.Name = "rbCliente"
        Me.rbCliente.Size = New System.Drawing.Size(89, 17)
        Me.rbCliente.TabIndex = 18
        Me.rbCliente.Text = "Cliente Físcal"
        Me.rbCliente.UseVisualStyleBackColor = True
        '
        'rbServicio
        '
        Me.rbServicio.AutoSize = True
        Me.rbServicio.Location = New System.Drawing.Point(412, 11)
        Me.rbServicio.Name = "rbServicio"
        Me.rbServicio.Size = New System.Drawing.Size(63, 17)
        Me.rbServicio.TabIndex = 19
        Me.rbServicio.Text = "Servicio"
        Me.rbServicio.UseVisualStyleBackColor = True
        '
        'tbCuenta
        '
        Me.tbCuenta.Location = New System.Drawing.Point(178, 83)
        Me.tbCuenta.Name = "tbCuenta"
        Me.tbCuenta.Size = New System.Drawing.Size(326, 20)
        Me.tbCuenta.TabIndex = 20
        '
        'lbClave
        '
        Me.lbClave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbClave.Location = New System.Drawing.Point(46, 55)
        Me.lbClave.Name = "lbClave"
        Me.lbClave.Size = New System.Drawing.Size(126, 20)
        Me.lbClave.TabIndex = 21
        Me.lbClave.Text = "Sucursal"
        Me.lbClave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbDescripcion
        '
        Me.tbDescripcion.Location = New System.Drawing.Point(178, 109)
        Me.tbDescripcion.Name = "tbDescripcion"
        Me.tbDescripcion.Size = New System.Drawing.Size(326, 20)
        Me.tbDescripcion.TabIndex = 22
        '
        'tbPosicion
        '
        Me.tbPosicion.Location = New System.Drawing.Point(178, 135)
        Me.tbPosicion.Name = "tbPosicion"
        Me.tbPosicion.Size = New System.Drawing.Size(96, 20)
        Me.tbPosicion.TabIndex = 23
        '
        'tbContrato
        '
        Me.tbContrato.Location = New System.Drawing.Point(516, 57)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(96, 20)
        Me.tbContrato.TabIndex = 24
        Me.tbContrato.Text = "0"
        Me.tbContrato.Visible = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Concepto, Me.Cuenta, Me.Descripción, Me.Posición, Me.Indicador, Me.Normas_Reparto})
        Me.DataGridView1.Location = New System.Drawing.Point(26, 253)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(531, 312)
        Me.DataGridView1.TabIndex = 25
        '
        'Clv_Concepto
        '
        Me.Clv_Concepto.DataPropertyName = "Clv_Concepto"
        Me.Clv_Concepto.HeaderText = "Clv_Concepto"
        Me.Clv_Concepto.Name = "Clv_Concepto"
        Me.Clv_Concepto.ReadOnly = True
        Me.Clv_Concepto.Visible = False
        '
        'Cuenta
        '
        Me.Cuenta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Cuenta.DataPropertyName = "Cuenta"
        Me.Cuenta.HeaderText = "Cuenta"
        Me.Cuenta.Name = "Cuenta"
        Me.Cuenta.ReadOnly = True
        '
        'Descripción
        '
        Me.Descripción.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripción.DataPropertyName = "Descripcion"
        Me.Descripción.HeaderText = "Descripción"
        Me.Descripción.Name = "Descripción"
        Me.Descripción.ReadOnly = True
        '
        'Posición
        '
        Me.Posición.DataPropertyName = "Posicion"
        Me.Posición.HeaderText = "Posición"
        Me.Posición.Name = "Posición"
        Me.Posición.ReadOnly = True
        '
        'Indicador
        '
        Me.Indicador.DataPropertyName = "Indicador"
        Me.Indicador.HeaderText = "Indicador"
        Me.Indicador.Name = "Indicador"
        Me.Indicador.ReadOnly = True
        '
        'Normas_Reparto
        '
        Me.Normas_Reparto.DataPropertyName = "Normas_Reparto"
        Me.Normas_Reparto.HeaderText = "Normas de Reparto"
        Me.Normas_Reparto.Name = "Normas_Reparto"
        Me.Normas_Reparto.ReadOnly = True
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(549, 127)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(82, 30)
        Me.btnAgregar.TabIndex = 26
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'btnQuitar
        '
        Me.btnQuitar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitar.ForeColor = System.Drawing.Color.Black
        Me.btnQuitar.Location = New System.Drawing.Point(563, 239)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(82, 30)
        Me.btnQuitar.TabIndex = 27
        Me.btnQuitar.Text = "Eliminar"
        Me.btnQuitar.UseVisualStyleBackColor = False
        '
        'cbSucursales
        '
        Me.cbSucursales.DisplayMember = "NOMBRE"
        Me.cbSucursales.FormattingEnabled = True
        Me.cbSucursales.Location = New System.Drawing.Point(178, 55)
        Me.cbSucursales.Name = "cbSucursales"
        Me.cbSucursales.Size = New System.Drawing.Size(326, 21)
        Me.cbSucursales.TabIndex = 28
        Me.cbSucursales.ValueMember = "CLV_SUCURSAL"
        '
        'tbBuscar
        '
        Me.tbBuscar.Location = New System.Drawing.Point(200, 228)
        Me.tbBuscar.Name = "tbBuscar"
        Me.tbBuscar.Size = New System.Drawing.Size(358, 20)
        Me.tbBuscar.TabIndex = 30
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(31, 228)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(168, 16)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Buscar por descripción"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(27, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(146, 20)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Indicador"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TextIndicador
        '
        Me.TextIndicador.Location = New System.Drawing.Point(178, 159)
        Me.TextIndicador.Name = "TextIndicador"
        Me.TextIndicador.Size = New System.Drawing.Size(96, 20)
        Me.TextIndicador.TabIndex = 31
        '
        'TextNormasReparto
        '
        Me.TextNormasReparto.Location = New System.Drawing.Point(178, 182)
        Me.TextNormasReparto.Name = "TextNormasReparto"
        Me.TextNormasReparto.Size = New System.Drawing.Size(96, 20)
        Me.TextNormasReparto.TabIndex = 33
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(30, 182)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(142, 18)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "Normas de Reparto"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmConceptosPoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(663, 577)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TextNormasReparto)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TextIndicador)
        Me.Controls.Add(Me.tbBuscar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cbSucursales)
        Me.Controls.Add(Me.btnQuitar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.tbPosicion)
        Me.Controls.Add(Me.tbDescripcion)
        Me.Controls.Add(Me.lbClave)
        Me.Controls.Add(Me.tbCuenta)
        Me.Controls.Add(Me.rbServicio)
        Me.Controls.Add(Me.rbCliente)
        Me.Controls.Add(Me.rbSucursal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmConceptosPoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conceptos Pólizas"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbSucursal As System.Windows.Forms.RadioButton
    Friend WithEvents rbCliente As System.Windows.Forms.RadioButton
    Friend WithEvents rbServicio As System.Windows.Forms.RadioButton
    Friend WithEvents tbCuenta As System.Windows.Forms.TextBox
    Friend WithEvents lbClave As System.Windows.Forms.Label
    Friend WithEvents tbDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents tbPosicion As System.Windows.Forms.TextBox
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents cbSucursales As System.Windows.Forms.ComboBox
    Friend WithEvents tbBuscar As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextIndicador As System.Windows.Forms.TextBox
    Friend WithEvents TextNormasReparto As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Clv_Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripción As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Posición As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Indicador As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Normas_Reparto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
