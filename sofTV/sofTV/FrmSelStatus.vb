﻿Public Class FrmSelStatus

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

        If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False And CheckBox6.Checked = False And CheckBox7.Checked = False Then
            MsgBox("Seleccione por lo menos un Status", MsgBoxStyle.Information)
            Exit Sub
        End If

        rContratado = False
        rInstalado = False
        rDesconectado = False
        rSuspendido = False
        rBaja = False
        rFarea = False
        rBTemporal = False

        If CheckBox1.Checked = True Then
            rContratado = True
        End If
        If CheckBox2.Checked = True Then
            rInstalado = True
        End If
        If CheckBox3.Checked = True Then
            rDesconectado = True
        End If
        If CheckBox4.Checked = True Then
            rSuspendido = True
        End If
        If CheckBox5.Checked = True Then
            rBaja = True
        End If
        If CheckBox6.Checked = True Then
            rFarea = True
        End If
        If CheckBox7.Checked = True Then
            rBTemporal = True
        End If


        eOpVentas = 26
        FrmImprimirComision.Show()
    End Sub
End Class