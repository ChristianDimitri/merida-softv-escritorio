Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmReporteProspectos

    Private estado As Integer = 0

    Private Sub FrmReporteProspectos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.dtpInicial.MaxDate = DateTime.Today
        Me.dtpFinal.MaxDate = DateTime.Today
        Me.llenaCbEstado()
        Me.getTipoServicios()
        Me.cbEstado.Text = "Selecciona"
        Me.cbServicios.SelectedValue = 0
    End Sub

    Private Sub cbServicios_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbServicios.SelectedIndexChanged

    End Sub

    Private Sub cbEstado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbEstado.SelectedIndexChanged
        Dim op As String = Me.cbEstado.Text
        Select Case op
            Case "Selecciona"
                Me.estado = 10
            Case "Contratado"
                Me.estado = 1
            Case "Sin Contratar"
                Me.estado = 0
            Case "Todos"
                Me.estado = 11
        End Select
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.cbEstado.SelectedValue = 0 Or Me.estado = 10 Then
            ImprimirHistorialDesconexiones.ReporteProspectos(Me.dtpInicial.Value, Me.dtpFinal.Value, Me.cbServicios.SelectedValue, Me.estado)
            ImprimirHistorialDesconexiones.Show()
            Me.Close()
        Else
            MsgBox("Faltan datos por seleccionar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub llenaCbEstado()
        Me.cbEstado.Items.Insert(0, "Selecciona")
        Me.cbEstado.Items.Insert(1, "Contratado")
        Me.cbEstado.Items.Insert(2, "Sin Contratar")
        Me.cbEstado.Items.Insert(3, "Todos")
    End Sub

    Private Sub getTipoServicios()
        Dim conexion As SqlConnection = New SqlConnection(MiConexion)
        Dim query As String = "SELECT 0 AS Clv_TipSer,'Selecciona' AS Concepto UNION SELECT Clv_TipSer,Concepto FROM TipServ WHERE Habilitar=0 AND Clv_TipSer<>4 "
        query = query + "UNION SELECT 100 AS Clv_TipSer,'Ambos' AS Concepto "
        Dim da As SqlDataAdapter = New SqlDataAdapter(query, conexion)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Try
            conexion.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            Me.cbServicios.DataSource = bs
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
        End Try
    End Sub
    
    Private Sub dtpInicial_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpInicial.ValueChanged

    End Sub

    Private Sub dtpFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFinal.ValueChanged

    End Sub
End Class