﻿Imports System.Data.SqlClient
Imports System.Text

Public Class brwRepOrdenesVentas

    Private Sub MuestraRepOrdVenta(ByVal Fecha As String, ByVal Ciudad As String, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRepOrdVenta ")
        strSQL.Append("'" & Fecha & "', ")
        strSQL.Append("'" & Ciudad & "', ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridView1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub brwRepOrdenesVentas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        BaseII.limpiaParametros()
        Me.ComboBox1.DataSource = BaseII.ConsultaDT("Usp_DameCiudadesOrdVenta")
        MuestraRepOrdVenta(String.Empty, String.Empty, 0)
    End Sub

    Private Sub ButtonNuevo_Click(sender As System.Object, e As System.EventArgs) Handles ButtonNuevo.Click
        rIDRepOrdVenta = 0

        GloRepOrdVentas = True
        eOpVentas = 25
        DameSession()
        FrmSelCiudad.Show()
    End Sub

    Private Sub ButtonConsultar_Click(sender As System.Object, e As System.EventArgs) Handles ButtonConsultar.Click

        rIDRepOrdVenta = DataGridView1.SelectedCells.Item(0).Value

        GloRepOrdVentas = True
        eOpVentas = 25

        FrmImprimirComision.Show()

    End Sub

    Private Sub DameSession()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("DameClv_Session_Servicios", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Output
        com.Parameters.Add(par)

        Try
            con.Open()
            com.ExecuteNonQuery()
            LocClv_session = par.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub

    
    Private Sub ButtonSalir_Click(sender As System.Object, e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub ButtonBusPregunta_Click(sender As System.Object, e As System.EventArgs) Handles ButtonBusPregunta.Click
        If CheckBox1.Checked = True Then
            MuestraRepOrdVenta(Me.DateTimePicker1.Text, ComboBox1.SelectedValue, 1)
        ElseIf CheckBox1.Checked = False Then
            MuestraRepOrdVenta(String.Empty, ComboBox1.SelectedValue, 1)
        End If

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            DateTimePicker1.Enabled = True
        ElseIf CheckBox1.Checked = False Then
            DateTimePicker1.Enabled = False
        End If
    End Sub
End Class