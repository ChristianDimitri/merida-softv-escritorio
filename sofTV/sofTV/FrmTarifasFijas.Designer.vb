﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTarifasFijas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTarifasFijas))
        Me.dgvTarifasFijas = New System.Windows.Forms.DataGridView()
        Me.IdTarifa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnTarifas = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        CType(Me.dgvTarifasFijas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnTarifas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnTarifas.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvTarifasFijas
        '
        Me.dgvTarifasFijas.AllowUserToAddRows = False
        Me.dgvTarifasFijas.AllowUserToDeleteRows = False
        Me.dgvTarifasFijas.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTarifasFijas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvTarifasFijas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTarifasFijas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdTarifa, Me.Descripcion, Me.Precio})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTarifasFijas.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvTarifasFijas.Location = New System.Drawing.Point(30, 40)
        Me.dgvTarifasFijas.Name = "dgvTarifasFijas"
        Me.dgvTarifasFijas.Size = New System.Drawing.Size(468, 399)
        Me.dgvTarifasFijas.TabIndex = 0
        '
        'IdTarifa
        '
        Me.IdTarifa.DataPropertyName = "IdTarifa"
        Me.IdTarifa.HeaderText = "IdTarifa"
        Me.IdTarifa.Name = "IdTarifa"
        Me.IdTarifa.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Servicio"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Width = 250
        '
        'Precio
        '
        Me.Precio.DataPropertyName = "Precio"
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Precio.DefaultCellStyle = DataGridViewCellStyle5
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        Me.Precio.Width = 150
        '
        'bnTarifas
        '
        Me.bnTarifas.AddNewItem = Nothing
        Me.bnTarifas.CountItem = Nothing
        Me.bnTarifas.DeleteItem = Nothing
        Me.bnTarifas.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnTarifas.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnTarifas.Location = New System.Drawing.Point(0, 0)
        Me.bnTarifas.MoveFirstItem = Nothing
        Me.bnTarifas.MoveLastItem = Nothing
        Me.bnTarifas.MoveNextItem = Nothing
        Me.bnTarifas.MovePreviousItem = Nothing
        Me.bnTarifas.Name = "bnTarifas"
        Me.bnTarifas.PositionItem = Nothing
        Me.bnTarifas.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnTarifas.Size = New System.Drawing.Size(532, 25)
        Me.bnTarifas.TabIndex = 4
        Me.bnTarifas.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(88, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(362, 458)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 5
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'FrmTarifasFijas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(532, 517)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnTarifas)
        Me.Controls.Add(Me.dgvTarifasFijas)
        Me.Name = "FrmTarifasFijas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tarifas Fijas de Contratación"
        CType(Me.dgvTarifasFijas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnTarifas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnTarifas.ResumeLayout(False)
        Me.bnTarifas.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvTarifasFijas As System.Windows.Forms.DataGridView
    Friend WithEvents IdTarifa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnTarifas As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnSalir As System.Windows.Forms.Button
End Class
