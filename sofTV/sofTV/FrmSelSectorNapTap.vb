﻿Imports System.Data.SqlClient

Public Class FrmSelSectorNapTap

    Private Sub MueveSeleccion_TAbla(ByVal clv_Session As Long, ByVal clv_sector As Integer, ByVal op As Integer)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "MUEVE_SeleccionaSector"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm2 As New SqlParameter("@Clv_Sector", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@op", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@clv_session", SqlDbType.Int)
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm2.Value = clv_sector
                    prm3.Value = op
                    prm4.Value = clv_Session
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Inicializa_Seleccion_TAbla(ByVal clv_Session As Long, ByVal query As String)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "Llena_Tabla_Selecciona1"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@QueryTablaOrigen", SqlDbType.VarChar, 250)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = query
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Guarda_Seleccion_TAbla(ByVal clv_Session As Long, ByVal Tabla As String)
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        Try
            If IsNumeric(clv_Session) = False Then clv_Session = 0
            If clv_Session > 0 Then

                CON4.Open()
                With cmd
                    .CommandText = "GUARDA_TABLA_Selecciona"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = CON4
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Tabla_Guardar", SqlDbType.VarChar, 150)
                    prm.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm.Value = clv_Session
                    prm2.Value = Tabla
                    .Parameters.Add(prm)
                    .Parameters.Add(prm2)
                    Dim i As Integer = .ExecuteNonQuery
                End With
                CON4.Close()
            End If


        Catch ex As System.Exception
            If CON4.State <> ConnectionState.Closed Then CON4.Close()
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub LLENA_LISBOX2(ByVal CLV_SESSION As Long)

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MUESTRATabla_SeleccionadosSector", con)
        Me.ListBox2.Items.Clear()
        Me.ListBox4.Items.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CLV_SESSION
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()
                'Llenamos los TxtBox con los datos respectivos
                'Me.ListBox1.ItemsInsert(reader(0).ToString, reader(1).ToString)
                Me.ListBox2.Items.Add(reader(1).ToString)
                Me.ListBox4.Items.Add(reader(0).ToString)
                sw = 1
            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        'If sw = 0 Then
        '    Me.ComboBox2.SelectedValue = 0
        '    Me.ComboBox3.SelectedValue = 0
        'End If

    End Sub


    Private Sub LLENA_LISBOX1(ByVal CLV_SESSION As Long)

        Dim sw As Integer = 0
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MUESTRATabla_SeleccionaSector", con)
        Me.ListBox1.Items.Clear()
        Me.ListBox3.Items.Clear()
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New Data.SqlClient.SqlParameter( _
                "@Clv_Session", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = CLV_SESSION
        cmd.Parameters.Add(prm)

        con.Open()

        Dim reader As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Try
            While reader.Read()

                'Llenamos los TxtBox con los datos respectivos
                'Me.ListBox1.ItemsInsert(reader(0).ToString, reader(1).ToString)
                Me.ListBox1.Items.Add(reader(1).ToString)
                Me.ListBox3.Items.Add(reader(0).ToString)
                sw = 1
            End While
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            reader.Close()
        End Try
        con.Close()
        'If sw = 0 Then
        '    Me.ComboBox2.SelectedValue = 0
        '    Me.ComboBox3.SelectedValue = 0
        'End If

    End Sub

    Private Sub FrmSelSector_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        colorea(Me, Me.Name)
        'Inicializa_Seleccion_TAbla(LocClv_session, "SELECT Clv_colonia,nombre FROM Colonias")
        'Inicializa_Seleccion_TAbla(LocClv_session, "select Clv_Sector ,Descripcion from dbo.Hub Order by Descripcion ")
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        CON4.Open()
        With cmd
            .CommandText = "llenaHubs"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = CON4
            Dim i As Integer = .ExecuteNonQuery
        End With
        Me.ListBox1.Items.Clear()
        Me.ListBox3.Items.Clear()
        Me.LLENA_LISBOX1(LocClv_session)
        Me.LLENA_LISBOX2(LocClv_session)
        LocPeriodo1 = False
        LocPeriodo2 = False
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("Seleccione al menos un Sector", MsgBoxStyle.Information)
            Exit Sub
        Else
            'Guarda_Seleccion_TAbla(LocClv_session, "Tabla_Seleccion_Sector")
            'Controlar impresion de reportes SAUL
            Locbndpen1 = True
            'Controlar impresion de reportes SAUL(Fin)
            If LocOp = 200 Then
                'FrmSelColonia_Rep21.Show()
                FrmSelColonia_SectorHub.Show()
            End If
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Locbndpen1 = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.ListBox3.SelectedIndex = Me.ListBox1.SelectedIndex
        If IsNumeric(Me.ListBox3.Text) = True Then
            Me.MueveSeleccion_TAbla(LocClv_session, Me.ListBox3.Text, 2)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.ListBox3.SelectedIndex = Me.ListBox1.SelectedIndex
        If Me.ListBox3.Items.Count > 0 Then
            Me.MueveSeleccion_TAbla(LocClv_session, 0, 22)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.ListBox4.SelectedIndex = Me.ListBox2.SelectedIndex
        If Me.ListBox4.Items.Count > 0 Then
            Me.MueveSeleccion_TAbla(LocClv_session, 0, 11)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ListBox4.SelectedIndex = Me.ListBox2.SelectedIndex
        If IsNumeric(Me.ListBox4.Text) = True Then
            Me.MueveSeleccion_TAbla(LocClv_session, Me.ListBox4.Text, 1)
            Me.LLENA_LISBOX2(LocClv_session)
            Me.LLENA_LISBOX1(LocClv_session)
        End If
    End Sub

    Private Sub FrmSelSector_Load_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Inicializa_Seleccion_TAbla(LocClv_session, "SELECT Clv_colonia,nombre FROM Colonias")
        'Inicializa_Seleccion_TAbla(LocClv_session, "select Clv_Sector ,Descripcion from dbo.Hub Order by Descripcion ")
        Dim cmd As New SqlClient.SqlCommand
        Dim CON4 As New SqlConnection(MiConexion)
        CON4.Open()
        With cmd
            .CommandText = "llenaSector"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = CON4
            Dim prm2 As New SqlParameter("@clv_session", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = LocClv_session
            .Parameters.Add(prm2)
            Dim i As Integer = .ExecuteNonQuery
        End With
        Me.ListBox1.Items.Clear()
        Me.ListBox3.Items.Clear()
        Me.LLENA_LISBOX1(LocClv_session)
        Me.LLENA_LISBOX2(LocClv_session)
        LocPeriodo1 = False
        LocPeriodo2 = False
    End Sub
End Class