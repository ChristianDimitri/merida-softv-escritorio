﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports CrystalDecisions.CrystalReports.Engine

Public Class ClassCambioClientePlaca

    Public contratoPlaca, nombrePlaca, callePlaca, numeroPlaca, coloniaPlaca, statusPlaca, periodoPlaca, placaActualPlaca As String

    Public Function uspBuscaClientePlaca(ByVal prmPlaca As String) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@placa", SqlDbType.VarChar, prmPlaca, 50)
        uspBuscaClientePlaca = BaseII.ConsultaDT("uspBuscaClientePlaca")
    End Function

    Public Sub uspDameDatosClientePlaca(ByVal prmContrato As Long)
        Dim DT As New DataTable

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, prmContrato)
        DT = BaseII.ConsultaDT("uspDameDatosClientePlaca")

        For Each row As DataRow In DT.Rows
            contratoPlaca = row("CONTRATO").ToString
            nombrePlaca = row("NOMBRE").ToString
            callePlaca = row("CALLE").ToString
            numeroPlaca = row("NUMERO").ToString
            coloniaPlaca = row("COLONIA").ToString
            statusPlaca = row("STATUS").ToString
            periodoPlaca = row("PERIODOPAGADO").ToString
            placaActualPlaca = row("PLACA").ToString
        Next
    End Sub

    Public Sub uspActualizaClientePlaca(ByVal prmContrato As Long, ByVal prmPlaca As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, prmContrato)
        BaseII.CreateMyParameter("@placa", SqlDbType.VarChar, prmPlaca, 50)
        BaseII.Inserta("uspActualizaClientePlaca")
    End Sub
End Class
