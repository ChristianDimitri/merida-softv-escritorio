﻿Imports System.Data.SqlClient
Public Class FrmRelAlmacenUsu
    Private Sub Llena_usuarios()
        Try
            BaseIIAlmacen.limpiaParametros()
            ComboBoxUsuario.DataSource = BaseIIAlmacen.ConsultaDT("Muestra_UsuariosAlmacen")
            ComboBoxUsuario.DisplayMember = "Nombre"
            ComboBoxUsuario.ValueMember = "Clv_usuario"
            If ComboBoxUsuario.Items.Count > 0 Then
                ComboBoxUsuario.SelectedValue = 0
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub Llena_almacen()
        Try
            BaseIIAlmacen.limpiaParametros()
            ComboBoxAlmacen.DataSource = BaseIIAlmacen.ConsultaDT("Muestra_Almacenes")
            ComboBoxAlmacen.DisplayMember = "Descripcion"
            ComboBoxAlmacen.ValueMember = "IdAlmacenEmpresa"
            If ComboBoxAlmacen.Items.Count > 0 Then
                ComboBoxAlmacen.SelectedValue = 0
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Llena_grid()
        Try
            BaseIIAlmacen.limpiaParametros()
            Rel_almacenusu.DataSource = BaseIIAlmacen.ConsultaDT("MuestraRelAlmacenUsu")
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
        
    End Sub
    Private Sub FrmRelAlmacenUsu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Llena_usuarios()
        Llena_almacen()
        Llena_grid()
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        'Agregar
        If ComboBoxAlmacen.SelectedValue = 0 Then
            MsgBox("Selecciona un Almacén")
            Exit Sub
        End If
        If ComboBoxUsuario.SelectedValue = 0 Then
            MsgBox("Selecciona un Usuario")
            Exit Sub
        End If
        Dim conexion As New SqlConnection(MiConexionAlmacen)
        Dim bnd As Integer
        conexion.Open()
        Dim comando As New SqlCommand()
        comando.Connection = conexion
        With comando
            .CommandText = "InsertaRelAlmacenUsu"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter("@IdAlmacenEmpresa", SqlDbType.Int)
            prm.Direction = ParameterDirection.Input
            prm.Value = ComboBoxAlmacen.SelectedValue
            .Parameters.Add(prm)

            Dim prm1 As New SqlParameter("@clv_usuario", SqlDbType.Int)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = ComboBoxUsuario.SelectedValue
            .Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@msg", SqlDbType.Int)
            prm2.Direction = ParameterDirection.Output
            prm2.Value = 0
            .Parameters.Add(prm2)
            Dim i As Integer = comando.ExecuteNonQuery()
            bnd = prm2.Value
        End With
        conexion.Close()
        If bnd = 1 Then
            MsgBox("El usuario " + ComboBoxUsuario.Text + " ya tiene relación con el almacén " + ComboBoxAlmacen.Text)
        End If
        If bnd = 2 Then
            Llena_grid()
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If Rel_almacenusu.Rows.Count = 0 Then
            Exit Sub
        End If
        BaseIIAlmacen.limpiaParametros()
        BaseIIAlmacen.CreateMyParameter("@id", SqlDbType.Int, Rel_almacenusu.SelectedCells(0).Value)
        BaseIIAlmacen.Inserta("EliminaRelAlmacenUsu")
        Llena_grid()
    End Sub
End Class