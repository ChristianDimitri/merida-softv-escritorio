﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDinercapDetalle
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ContratoTel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Credito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConceptoPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumMenRestantes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AdeudoTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusCredito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BtnExportar = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ContratoTel, Me.Cliente, Me.Credito, Me.Importe, Me.FechaPago, Me.ConceptoPago, Me.DescripcionPago, Me.NumMenRestantes, Me.AdeudoTotal, Me.StatusCredito, Me.StatusPago})
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(863, 735)
        Me.DataGridView1.TabIndex = 2
        Me.DataGridView1.TabStop = False
        '
        'ContratoTel
        '
        Me.ContratoTel.DataPropertyName = "ContratoTel"
        Me.ContratoTel.HeaderText = "Contrato"
        Me.ContratoTel.Name = "ContratoTel"
        Me.ContratoTel.ReadOnly = True
        '
        'Cliente
        '
        Me.Cliente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.Cliente.DataPropertyName = "Cliente"
        Me.Cliente.HeaderText = "Cliente"
        Me.Cliente.Name = "Cliente"
        Me.Cliente.ReadOnly = True
        '
        'Credito
        '
        Me.Credito.DataPropertyName = "Credito"
        Me.Credito.HeaderText = "Crédito"
        Me.Credito.Name = "Credito"
        Me.Credito.ReadOnly = True
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'FechaPago
        '
        Me.FechaPago.DataPropertyName = "FechaPago"
        Me.FechaPago.HeaderText = "Fecha Pago"
        Me.FechaPago.Name = "FechaPago"
        Me.FechaPago.ReadOnly = True
        '
        'ConceptoPago
        '
        Me.ConceptoPago.DataPropertyName = "ConceptoPago"
        Me.ConceptoPago.HeaderText = "Concepto"
        Me.ConceptoPago.Name = "ConceptoPago"
        Me.ConceptoPago.ReadOnly = True
        '
        'DescripcionPago
        '
        Me.DescripcionPago.DataPropertyName = "DescripcionPago"
        Me.DescripcionPago.HeaderText = "Descripción"
        Me.DescripcionPago.Name = "DescripcionPago"
        Me.DescripcionPago.ReadOnly = True
        '
        'NumMenRestantes
        '
        Me.NumMenRestantes.DataPropertyName = "NumMenRestantes"
        Me.NumMenRestantes.HeaderText = "#MenRes"
        Me.NumMenRestantes.Name = "NumMenRestantes"
        Me.NumMenRestantes.ReadOnly = True
        '
        'AdeudoTotal
        '
        Me.AdeudoTotal.DataPropertyName = "AdeudoTotal"
        Me.AdeudoTotal.HeaderText = "Adeudo"
        Me.AdeudoTotal.Name = "AdeudoTotal"
        Me.AdeudoTotal.ReadOnly = True
        '
        'StatusCredito
        '
        Me.StatusCredito.DataPropertyName = "StatusCredito"
        Me.StatusCredito.HeaderText = "Status Credito"
        Me.StatusCredito.Name = "StatusCredito"
        Me.StatusCredito.ReadOnly = True
        '
        'StatusPago
        '
        Me.StatusPago.DataPropertyName = "StatusPago"
        Me.StatusPago.HeaderText = "Status Pago"
        Me.StatusPago.Name = "StatusPago"
        Me.StatusPago.ReadOnly = True
        '
        'BtnExportar
        '
        Me.BtnExportar.BackColor = System.Drawing.Color.Orange
        Me.BtnExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnExportar.ForeColor = System.Drawing.Color.Black
        Me.BtnExportar.Location = New System.Drawing.Point(872, 12)
        Me.BtnExportar.Name = "BtnExportar"
        Me.BtnExportar.Size = New System.Drawing.Size(124, 44)
        Me.BtnExportar.TabIndex = 3
        Me.BtnExportar.Text = "&SALIR"
        Me.BtnExportar.UseVisualStyleBackColor = False
        '
        'FrmDinercapDetalle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 729)
        Me.Controls.Add(Me.BtnExportar)
        Me.Controls.Add(Me.DataGridView1)
        Me.MaximizeBox = False
        Me.Name = "FrmDinercapDetalle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmDinercapDetalle"
        Me.TopMost = True
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents BtnExportar As System.Windows.Forms.Button
    Friend WithEvents ContratoTel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Credito As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConceptoPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumMenRestantes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AdeudoTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusCredito As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StatusPago As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
