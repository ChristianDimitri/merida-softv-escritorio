﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmDireccionesPiratas

    Private Sub FrmDireccionesPiratas_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        BaseII.limpiaParametros()
        CbCalle.DataSource = BaseII.ConsultaDT("MUESTRACALLES")
    End Sub

    Private Sub CbCalle_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles CbCalle.SelectedIndexChanged
        If IsNumeric(Me.CbCalle.SelectedValue) = True Then
            Dim dTable As New DataTable

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_CALLE", SqlDbType.Int, Me.CbCalle.SelectedValue)
            dTable = BaseII.ConsultaDT("DAMECOLONIA_CALLE")

            If dTable.Rows.Count = 0 Then Exit Sub
            cbColonias.DataSource = dTable
            'CpTbox.Text = dTable.Rows(0)(4).ToString

            Me.uspConsultaTap(0, CInt(Me.CbCalle.SelectedValue), CInt(Me.cbColonias.SelectedValue))

        End If
    End Sub

    Private Sub uspConsultaTap(ByVal prmContrato As Long, ByVal prmClvCalle As Integer, ByVal prmClvColonia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC uspConsultaTap ")
        STR.Append(CStr(prmContrato) & ", ")
        STR.Append(CStr(prmClvCalle) & ", ")
        STR.Append(CStr(prmClvColonia))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            Me.cbTap.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub cbColonias_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbColonias.SelectedIndexChanged
        If IsNumeric(Me.cbColonias.SelectedValue) = True Then

            Dim dTable As New DataTable

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLV_Colonia", SqlDbType.Int, Me.cbColonias.SelectedValue)
            dTable = BaseII.ConsultaDT("DAMECpColonia")

            If dTable.Rows.Count = 0 Then Exit Sub
            tbCp.Text = dTable.Rows(0)(0).ToString

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Me.cbColonias.SelectedValue)
            cbCiudad.DataSource = BaseII.ConsultaDT("MuestraCVECOLCIU")

            Me.uspConsultaTap(0, CInt(Me.CbCalle.SelectedValue), CInt(Me.cbColonias.SelectedValue))

        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.CbCalle.SelectedValue) = False Then
            MsgBox("Seleccione una Calle", MsgBoxStyle.Information)
            Exit Sub
        ElseIf IsNumeric(Me.cbColonias.SelectedValue) = False Then
            MsgBox("Seleccione una Colonia", MsgBoxStyle.Information)
            Exit Sub
        ElseIf IsNumeric(Me.cbCiudad.SelectedValue) = False Then
            MsgBox("Seleccione una Ciudad", MsgBoxStyle.Information)
            Exit Sub
        ElseIf tbNoext.Text.Length = 0 Then
            MsgBox("Capture el Numero Extrerior", MsgBoxStyle.Information)
            Exit Sub
        ElseIf IsNumeric(tbCp.Text) = False Then
            MsgBox("Codigo Postal invalido", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim Mensaje As String = ""

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Calle", SqlDbType.Int, Me.CbCalle.SelectedValue)
        BaseII.CreateMyParameter("@numExt", SqlDbType.VarChar, CStr(Me.tbNoext.Text), 50)
        BaseII.CreateMyParameter("@numInt", SqlDbType.VarChar, CStr(Me.tbNoint.Text), 50)
        BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Me.cbColonias.SelectedValue)
        BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, Me.cbCiudad.SelectedValue)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("ValidaDireccionPirata")
        Mensaje = ""
        Mensaje = BaseII.dicoPar("@Msj").ToString

        If Mensaje <> "" Then
            MsgBox(Mensaje, MsgBoxStyle.Information)
            Exit Sub
        Else

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Me.TbNombre.Text, 500)
            BaseII.CreateMyParameter("@clv_Calle", SqlDbType.Int, Me.CbCalle.SelectedValue)
            BaseII.CreateMyParameter("@numExt", SqlDbType.VarChar, CStr(Me.tbNoext.Text), 50)
            BaseII.CreateMyParameter("@numInt", SqlDbType.VarChar, CStr(Me.tbNoint.Text), 50)
            BaseII.CreateMyParameter("@Entrecalles", SqlDbType.VarChar, Me.tbEntrecalles.Text, 1500)
            BaseII.CreateMyParameter("@Telefono", SqlDbType.VarChar, Me.tbTel.Text, 50)
            BaseII.CreateMyParameter("@clv_colonia", SqlDbType.Int, Me.cbColonias.SelectedValue)
            BaseII.CreateMyParameter("@cp", SqlDbType.Int, Me.tbCp.Text)
            BaseII.CreateMyParameter("@Celular", SqlDbType.VarChar, Me.tbCel.Text, 50)
            BaseII.CreateMyParameter("@clv_ciudad", SqlDbType.Int, Me.cbCiudad.SelectedValue)
            BaseII.CreateMyParameter("@idtap", SqlDbType.Int, Me.cbTap.SelectedValue)
            BaseII.CreateMyParameter("@idPirata", ParameterDirection.Output, SqlDbType.Int)
            BaseII.ProcedimientoOutPut("InsertaDireccionPirata")
            tbIdpirata.Text = BaseII.dicoPar("@idPirata").ToString

            MsgBox("Almacenado con exito", MsgBoxStyle.Information)
            Me.Close()
        End If

    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class