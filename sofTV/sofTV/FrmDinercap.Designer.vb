﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDinercap
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LblNombreArchivo = New System.Windows.Forms.Label()
        Me.TxtNombre = New System.Windows.Forms.TextBox()
        Me.BtnCargar = New System.Windows.Forms.Button()
        Me.BtnGuardar = New System.Windows.Forms.Button()
        Me.LblUserSys = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'LblNombreArchivo
        '
        Me.LblNombreArchivo.AutoSize = True
        Me.LblNombreArchivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNombreArchivo.Location = New System.Drawing.Point(25, 44)
        Me.LblNombreArchivo.Name = "LblNombreArchivo"
        Me.LblNombreArchivo.Size = New System.Drawing.Size(67, 16)
        Me.LblNombreArchivo.TabIndex = 0
        Me.LblNombreArchivo.Text = "Nombre:"
        '
        'TxtNombre
        '
        Me.TxtNombre.Location = New System.Drawing.Point(98, 40)
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.Size = New System.Drawing.Size(274, 20)
        Me.TxtNombre.TabIndex = 1
        '
        'BtnCargar
        '
        Me.BtnCargar.BackColor = System.Drawing.Color.Orange
        Me.BtnCargar.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCargar.Location = New System.Drawing.Point(322, 66)
        Me.BtnCargar.Name = "BtnCargar"
        Me.BtnCargar.Size = New System.Drawing.Size(50, 36)
        Me.BtnCargar.TabIndex = 2
        Me.BtnCargar.Text = "..."
        Me.BtnCargar.UseVisualStyleBackColor = False
        '
        'BtnGuardar
        '
        Me.BtnGuardar.BackColor = System.Drawing.Color.Orange
        Me.BtnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnGuardar.Location = New System.Drawing.Point(163, 120)
        Me.BtnGuardar.Name = "BtnGuardar"
        Me.BtnGuardar.Size = New System.Drawing.Size(99, 31)
        Me.BtnGuardar.TabIndex = 3
        Me.BtnGuardar.Text = "Guardar"
        Me.BtnGuardar.UseVisualStyleBackColor = False
        '
        'LblUserSys
        '
        Me.LblUserSys.AutoSize = True
        Me.LblUserSys.Location = New System.Drawing.Point(208, 130)
        Me.LblUserSys.Name = "LblUserSys"
        Me.LblUserSys.Size = New System.Drawing.Size(39, 13)
        Me.LblUserSys.TabIndex = 4
        Me.LblUserSys.Text = "Label1"
        Me.LblUserSys.Visible = False
        '
        'FrmDinercap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(411, 179)
        Me.Controls.Add(Me.BtnGuardar)
        Me.Controls.Add(Me.BtnCargar)
        Me.Controls.Add(Me.TxtNombre)
        Me.Controls.Add(Me.LblNombreArchivo)
        Me.Controls.Add(Me.LblUserSys)
        Me.MaximizeBox = False
        Me.Name = "FrmDinercap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmDinercap"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LblNombreArchivo As System.Windows.Forms.Label
    Friend WithEvents TxtNombre As System.Windows.Forms.TextBox
    Friend WithEvents BtnCargar As System.Windows.Forms.Button
    Friend WithEvents BtnGuardar As System.Windows.Forms.Button
    Friend WithEvents LblUserSys As System.Windows.Forms.Label
End Class
