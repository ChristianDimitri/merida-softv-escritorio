﻿Imports System.Data.SqlClient
Imports System.Text
Public Class BrwVerEncuestas
    Dim BorraEncuesta As Integer = 0

    Private Sub BrwVerEncuestas_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If rBnd = True Then
            rBnd = False
            DameListadoEncuestas()
        End If
    End Sub

    Private Sub BrwVerEncuestas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        DameListadoEncuestas()
    End Sub

    Private Sub DameListadoEncuestas()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC Usp_DameListaEncuestas ")


        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridView1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ButtonNuevo_Click(sender As System.Object, e As System.EventArgs) Handles ButtonNuevo.Click
        gloBndEncesta = "N"
        FrmSelServStatusEncuestas.Show()
    End Sub

    Private Sub ButtonContinuar_Click(sender As System.Object, e As System.EventArgs) Handles ButtonContinuar.Click
        gloBndEncesta = "C"
        GloIdEncuesta = DataGridView1.SelectedCells.Item(0).Value
        eIDEncuesta = DataGridView1.SelectedCells.Item(7).Value
        BrwQuiz.Show()
    End Sub

    Private Sub ButtonSalir_Click(sender As System.Object, e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        GloIdEncuesta = DataGridView1.SelectedCells.Item(0).Value
        BorraServStatusEncuestas()
        If BorraEncuesta = 1 Then
            MsgBox("Eliminado Correctamente", MsgBoxStyle.Information)
            rBnd = True
        ElseIf BorraEncuesta = 0 Then
            MsgBox("La encuesta esta iniciada, no se puede eliminar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub BorraServStatusEncuestas()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id_Encuesta", SqlDbType.Int, GloIdEncuesta)
        BaseII.CreateMyParameter("@Bnd", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Usp_BorraServStatusEncuestas")
        BorraEncuesta = BaseII.dicoPar("@Bnd")
    End Sub
End Class