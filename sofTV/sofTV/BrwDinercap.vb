﻿Imports System.Data
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel

Public Class BrwDinercap

    Dim UsuarioBrw As String
    Dim SiElimina As Integer

    Public Sub Sp_EliminaArchivo(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Sp_EliminaArchivo", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim par1 As New SqlParameter("@Id", SqlDbType.Int)
            par1.Direction = ParameterDirection.Input
            par1.Value = Id
            comando.Parameters.Add(par1)

            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Sp_ValidaParaCancelar(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Sp_ValidaParaCancelar", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim par1 As New SqlParameter("@Id", SqlDbType.Int)
            par1.Direction = ParameterDirection.Input
            par1.Value = Id
            comando.Parameters.Add(par1)

            Dim par5 As New SqlParameter("@Count", SqlDbType.Int)
            par5.Direction = ParameterDirection.Output
            par5.Value = ""
            comando.Parameters.Add(par5)

            conexion.Open()
            comando.ExecuteNonQuery()
            SiElimina = par5.Value

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Buscar(ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Try
            If Op = 0 Then

                Sp_Busca_TblAdeudo("", "", "", "", 0)

            ElseIf Op = 1 Then

                If (BNombre.Text) <> "" Then
                    Sp_Busca_TblAdeudo(BNombre.Text, "", "", "", 1)
                Else
                    MessageBox.Show("El campo no puede ir vacio, verifique por favor")
                End If

            ElseIf Op = 2 Then

                If (BFecha.Text) <> "" Then
                    Sp_Busca_TblAdeudo("", BFecha.Text, "", "", 2)
                Else
                    MessageBox.Show("El campo no puede ir vacio, verifique por favor")
                End If

            ElseIf Op = 3 Then

                If (BStatus.Text) <> "" Then
                    Sp_Busca_TblAdeudo("", "", BStatus.Text, "", 3)
                Else
                    MessageBox.Show("El campo no puede ir vacio, verifique por favor")
                End If

            ElseIf Op = 4 Then

                If (BUsuario.Text) <> "" Then
                    Sp_Busca_TblAdeudo("", "", "", BUsuario.Text, 4)
                Else
                    MessageBox.Show("El campo no puede ir vacio, verifique por favor")
                End If

            End If

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Sp_Busca_TblAdeudo(ByVal Nombre As String, ByVal Fecha As String, ByVal Status As String, ByVal Usuario As String, ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim CMD As New SqlClient.SqlDataAdapter()
        Dim consulta As String = Nothing
        Try

            If Len(Nombre) = 0 Then
                Nombre = "''"
            ElseIf Len(Nombre) > 0 Then
                Nombre = "'" + Nombre + "'"
            End If
            If Len(Fecha) = 0 Then
                Fecha = "''"
            ElseIf Len(Fecha) > 0 Then
                Fecha = "'" + Fecha + "'"
            End If
            If Len(Status) = 0 Then
                Status = "''"
            ElseIf Len(Status) > 0 Then
                Status = "'" + Status + "'"
            End If
            If Len(Usuario) = 0 Then
                Usuario = "''"
            ElseIf Len(Usuario) > 0 Then
                Usuario = "'" + Usuario + "'"
            End If

            consulta = "Exec Sp_Busca_TblAdeudo " + CStr(Nombre) + "," + CStr(Fecha) + "," + CStr(Status) + "," + CStr(Usuario) + "," + CStr(Op)

            CMD = New SqlDataAdapter(consulta, conexion)
            Dim dt As New DataTable
            Dim BS As New BindingSource

            CMD.Fill(dt)
            BS.DataSource = dt
            Me.DataGridView1.DataSource = BS.DataSource
            Try
                Me.LblNombre.Text = CStr(Me.DataGridView1.SelectedCells(1).Value)
                Me.LblFecha.Text = (Me.DataGridView1.SelectedCells(2).Value)
                Me.LblUsuario.Text = (Me.DataGridView1.SelectedCells(3).Value)
                Me.LblStatus.Text = (Me.DataGridView1.SelectedCells(4).Value)
            Catch ex As System.Exception

            End Try

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Sp_Busca_TblAdeudoDetalle(ByVal Id As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim CMD2 As New SqlClient.SqlDataAdapter()
        Dim consulta2 As String = Nothing
        Try

            consulta2 = "Exec Sp_Busca_TblAdeudoDetalle " + CStr(Id)

            CMD2 = New SqlDataAdapter(consulta2, conexion)
            Dim dt2 As New DataTable
            Dim BS2 As New BindingSource

            CMD2.Fill(dt2)
            BS2.DataSource = dt2
            FrmDinercapDetalle.DataGridView1.DataSource = BS2.DataSource
        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub consultar()
        Try
            If Me.DataGridView1.RowCount > 0 Then

                Sp_Busca_TblAdeudoDetalle(Me.DataGridView1.SelectedCells(0).Value)
                FrmDinercapDetalle.Show()
            Else
                MsgBox("Seleccione un registro para poder consultar ", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Try
            FrmDinercap.Show()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub BrwDinercap_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.LblUserSis.Text = FrmMiMenu.Label2.Text
        Me.Buscar(0)
    End Sub

    Private Sub BtnNombre_Click(sender As System.Object, e As System.EventArgs) Handles BtnNombre.Click
        Me.Buscar(1)
        BNombre.Clear()
    End Sub

    Private Sub BtnFecha_Click(sender As System.Object, e As System.EventArgs) Handles BtnFecha.Click
        Me.Buscar(2)
        Me.BFecha.Text = Now.ToLongDateString
    End Sub

    Private Sub BtnStatus_Click(sender As System.Object, e As System.EventArgs) Handles BtnStatus.Click
        Me.Buscar(3)
        BStatus.Clear()
    End Sub

    Private Sub BtnUsuario_Click(sender As System.Object, e As System.EventArgs) Handles BtnUsuario.Click
        Me.Buscar(4)
        BUsuario.Clear()
    End Sub

    Private Sub DataGridView1_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        Dim i As Integer
        i = DataGridView1.CurrentRow.Index

        Me.LblNombre.Text = Me.DataGridView1.Item(1, i).Value.ToString()
        Me.LblFecha.Text = Me.DataGridView1.Item(2, i).Value()
        Me.LblUsuario.Text = Me.DataGridView1.Item(3, i).Value()
        Me.LblStatus.Text = Me.DataGridView1.Item(4, i).Value()

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            If Me.DataGridView1.RowCount > 0 Then

                Sp_ValidaParaCancelar(Me.DataGridView1.SelectedCells(0).Value)

                If (Me.DataGridView1.SelectedCells(4).Value) = "Cancelado" Then
                    MsgBox("Ya se encuentra cancelado el archivo", MsgBoxStyle.Information)
                Else
                    If SiElimina >= 1 Then
                        MsgBox("No se puede cancelar el archivo, debido a que ya hay contratos cobrados", MsgBoxStyle.Information)
                    Else
                        If (MsgBox("¿Esta seguro que desea cancelar el archivo?", MsgBoxStyle.YesNo)) = MsgBoxResult.Yes Then
                            Sp_EliminaArchivo(Me.DataGridView1.SelectedCells(0).Value)
                            Buscar(0)
                            If (MsgBox("¿Desea cargar el achivo nuevamente?", MsgBoxStyle.YesNo)) = MsgBoxResult.Yes Then
                                FrmDinercap.Show()
                            End If
                        End If
                    End If
                End If
            Else
                MsgBox("Seleccione un registro para poder consultar ", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click

        Me.Cursor = Cursors.WaitCursor

        If Me.DataGridView1.RowCount > 0 Then

            'Dim connection As String = "Provider=sqloledb;Data Source=LIZ-PC;Initial Catalog=Newsoftv;User Id=sa;Password=06011975"
            'Dim conexion As New OleDb.OleDbConnection(connection)

            Dim connection As String = "Provider=sqloledb;Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;User Id=" & GloUserID & ";Password=" & GloPassword & ""
            Dim conexion As New OleDb.OleDbConnection(connection)

            Try
                ' Creamos todo lo necesario para un excel
                Dim appXL As Excel.Application
                Dim wbXl As Excel.Workbook
                Dim shXL As Excel.Worksheet
                Dim indice As Integer = 2
                appXL = CreateObject("Excel.Application")
                appXL.Visible = False 'Para que no se muestre mientras se crea
                wbXl = appXL.Workbooks.Add
                shXL = wbXl.ActiveSheet
                ' Añadimos las cabeceras de las columnas con formato en negrita
                Dim formatRange As Excel.Range
                formatRange = shXL.Range("a1")
                formatRange.EntireRow.Font.Bold = True
                shXL.Cells(1, 1).Value = "ContratoTel"
                shXL.Cells(1, 2).Value = "Cliente"
                shXL.Cells(1, 3).Value = "Credito"
                shXL.Cells(1, 4).Value = "Importe"
                shXL.Cells(1, 5).Value = "FechaPago"

                ' Obtenemos los datos que queremos exportar desde base de datos.
                Dim s As String = ("Exec Sp_Busca_TblAdeudoDetalle " + CStr(Me.DataGridView1.SelectedCells(0).Value))
                Dim myCommand As New OleDb.OleDbCommand(s)
                myCommand.Connection = conexion
                conexion.Open()
                Dim myReader As OleDb.OleDbDataReader = myCommand.ExecuteReader()

                While myReader.Read()
                    ' Cargamos la información en el excel
                    shXL.Cells(indice, 1).Value = myReader("ContratoTel")
                    shXL.Cells(indice, 2).Value = myReader("Cliente")
                    shXL.Cells(indice, 3).Value = myReader("Credito")
                    shXL.Cells(indice, 4).Value = myReader("Importe")
                    shXL.Cells(indice, 5).Value = myReader("FechaPago")
                    indice += 1
                End While

                shXL.Columns.Item(2).NumberFormat = "00000"
                shXL.Columns.Item(3).NumberFormat = "00000"
                shXL.Columns.Item(4).NumberFormat = "0.00"
                ' Cerramos el reader
                myReader.Close()
                ' Mostramos un dialog para que el usuario indique donde quiere guardar el excel
                Dim saveFileDialog1 As New SaveFileDialog()
                saveFileDialog1.Title = "Guardar documento Excel"
                saveFileDialog1.Filter = "Excel File|*.xls"
                saveFileDialog1.FileName = ""
                saveFileDialog1.ShowDialog()
                ' Guardamos el excel en la ruta que ha especificado el usuario
                wbXl.SaveAs(saveFileDialog1.FileName)
                ' Cerramos el workbook
                appXL.Workbooks.Close()
                ' Eliminamos el objeto excel
                appXL.Quit()
                MessageBox.Show("Se exportaron los datos a excel.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show("Error al exportar los datos a excel.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Finally
                ' Cerramos la conexión y ponemos el cursor por defecto de nuevo
                conexion.Close()
                Me.Cursor = Cursors.Arrow
            End Try

        End If

    End Sub


End Class