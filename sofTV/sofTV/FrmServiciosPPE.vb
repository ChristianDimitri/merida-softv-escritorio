Imports system.Data.Sql
Imports system.Data.SqlClient

Public Class FrmServiciosPPE

    Private Inicio As Boolean
    Private PtsNec As Integer
    Private PtsCli As Integer
    Private contratoGlo As Long

    Private Sub FrmServiciosPPE_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GLOCONTRATOSEL > 0 Then
            Me.txtBuscaContrato.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            Me.buscaContrato()
        End If
    End Sub

    Private Sub FrmServiciosPPE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Inicio = True
        Me.lblPuntosNecesarios.Text = Me.lblPuntosNecesarios.Text + Me.obtenerPtsNecesarios
        Me.dtmFecha.Value = DateTime.Today
        GLOCONTRATOSEL = 0
    End Sub

    Private Sub gvMuestraServicios_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gvMuestraServicios.SelectionChanged
        If Inicio = False Then
            Try
                Dim row As Integer = Convert.ToInt32(Me.gvMuestraServicios.CurrentRow.Index.ToString)
                Dim clv As String = Me.gvMuestraServicios.Rows(row).Cells("Clv_Txt").Value.ToString
                Me.llenaGridProgramacion(clv, 1)
                Me.lblClv.Text = Me.gvMuestraServicios.Rows(row).Cells("Clv_Txt").Value.ToString
                Me.lblDescripcion.Text = Me.gvMuestraServicios.Rows(row).Cells("Descripcion").Value.ToString
                Me.lblPrecio.Text = Me.gvMuestraServicios.Rows(row).Cells("Precio").Value.ToString
                Me.lblHits.Text = Me.gvMuestraServicios.Rows(row).Cells("Hits").Value.ToString
                Me.lblClasificacion.Text = Me.gvMuestraServicios.Rows(row).Cells("Clasificacion").Value.ToString
                Me.Refresh()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnBuscaContrato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscaContrato.Click
        Me.limpiaTodo()
        Me.btnAceptar.Enabled = False
        Me.btnAgregar.Enabled = False
        Me.btnQuitar.Enabled = False
        Me.lblPuntos.Text = ""
        Me.LblStatus.Text = ""
        Me.lblPuntos.ForeColor = Color.Black
        FrmSelCliente.Show()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnBusca_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusca.Click
        Me.llenaGridServicios("Buscar")
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.gvPelis.Rows.Count >= 0 Then
            If Me.guardaPelis = True Then
                MsgBox("Se han cargado las peliculas con exito", MsgBoxStyle.Information)
            End If
            Me.limpiaTodo()
        Else
            MsgBox("No se a seleccionado ninguna pelicula hasta el momento", MsgBoxStyle.Critical)
        End If
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Me.agregaPeli()
        Me.btnAceptar.Enabled = True
        Me.btnLimpiar.Visible = True
        Me.Refresh()
    End Sub

    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        If Me.gvPelis.Rows.Count > 0 Then
            Dim row As Integer = Convert.ToInt32(Me.gvPelis.CurrentRow.Index.ToString)
            Me.gvPelis.Rows.Remove(Me.gvPelis.CurrentRow)
        Else
            MsgBox("No hay peliculas que remover de la lista", MsgBoxStyle.Exclamation)
            Me.btnAceptar.Enabled = False
        End If
    End Sub

    Private Function obtenerPtsNecesarios() As String
        Dim conexion As New SqlConnection(MiConexion)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Dim valor As String = ""
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "IF(SELECT COUNT(*) FROM PUNTOSSERVICIOPPE)=0 BEGIN select 0 end ELSE BEGIN select isnull(puntos,0) from PuntosServicioPPE End"
            com.Connection = conexion
            com.Connection.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            da.Fill(tabla)
            valor = tabla.Rows(0)(0).ToString
            Me.PtsNec = Convert.ToInt32(tabla.Rows(0)(0).ToString)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            com.Connection.Close()
            com.Dispose()
            tabla.Dispose()
        End Try
        Return valor
    End Function
    '**************************************************DamePuntosPPECliente***************************************************
    Private Function obtenerPtsCliente(ByVal Contrato As String) As String
        Me.limpiaTodo()
        Dim valor As String = ""
        Dim valor1 As String = ""
        Dim conexion As New SqlConnection(MiConexion)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "Exec DamePuntosPPECliente " + Contrato
            com.Connection = conexion
            com.Connection.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            da.Fill(tabla)
            If tabla.Rows.Count > 0 Then
                If tabla.Columns.Count < 2 Then
                    MsgBox(tabla.Rows(0)(0).ToString, MsgBoxStyle.Critical)
                    Exit Function
                End If
                Me.PtsCli = tabla.Rows(0)(1).ToString
                valor = tabla.Rows(0)(0).ToString
                valor1 = tabla.Rows(0)(1).ToString + " pts."
            End If
        Catch ex As Exception

        Finally
            com.Connection.Close()
            com.Dispose()
            tabla.Dispose()
        End Try
        If PtsCli >= PtsNec Then
            'MsgBox("El cliente tiene los puntos suficientes.", MsgBoxStyle.Information)
            Me.btnAgregar.Enabled = True
            Me.btnAgregar.Visible = True
            Me.btnQuitar.Enabled = True
            Me.btnQuitar.Visible = True
            Me.llenaGridServicios("Inicio")
            Me.Refresh()
        Else
            MsgBox("El cliente no tiene los puntos suficientes.", MsgBoxStyle.Information)
            Me.btnAceptar.Enabled = False
            Me.btnAceptar.Enabled = False
            Me.lblPuntos.ForeColor = Color.Red
        End If
        Me.lblPuntos.Text = valor1
        Return valor
    End Function

    Private Sub llenaGridServicios(ByVal opcion As String)
        Dim query As String = ""
        Select Case opcion
            Case Is = "Inicio"
                query = "Exec MuestraServiciosPPE null,null,null,null,5"
            Case Is = "Buscar"
                Dim fecha As String = Me.dtmFecha.Value.ToShortDateString
                Dim fechaA As String = ""
                If fecha.Length > 0 Then
                    Dim i As Integer = 0
                    For i = 0 To fecha.Length - 1
                        If fecha(i) = "/" Then
                            fechaA = fechaA + "-"
                        Else
                            fechaA = fechaA + fecha(i)
                        End If
                    Next i
                Else
                    fecha = "null"
                End If
                query = "Exec MuestraServiciosPPE null,'" + Me.txtDescripcion.Text.ToString + "',null,'" + fechaA + "',6"
        End Select
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Try
            Dim tabla As DataTable = New DataTable
            Dim com As SqlCommand = New SqlCommand(query, con)
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            Dim bs As BindingSource = New BindingSource
            con.Open()
            da.Fill(tabla)
            If tabla.Rows.Count >= 0 Then
                bs.DataSource = tabla
                If tabla.Columns.Count = 8 Then
                    tabla.Columns.Remove(tabla.Columns("Imagen"))
                End If
                Me.gvMuestraServicios.DataSource = bs
            Else
                Return
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        Finally
            con.Close()
            con.Dispose()
        End Try
        Me.gvMuestraServicios.Columns("Clv_Servicio").Visible = False
        Me.gvMuestraServicios.Columns("Precio").Visible = False
        Me.gvMuestraServicios.Columns("Clv_PPE").Visible = False
        Me.gvMuestraServicios.Columns("Hits").Visible = False
        Me.gvMuestraServicios.Columns("Clasificacion").Visible = False
        Me.gvMuestraServicios.Columns("Clv_Txt").HeaderText = "Clave"
        Me.gvMuestraServicios.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        Inicio = False
        If Me.gvMuestraServicios.Rows.Count > 0 Then
            Me.gvMuestraServicios.Rows(0).Selected = True
            Me.gvMuestraServicios.CurrentCell = Me.gvMuestraServicios.Rows(0).Cells("Clv_Txt")
            Me.llenaGridProgramacion(Me.gvMuestraServicios.Rows(0).Cells("Clv_Txt").Value.ToString, 1)
            If Me.gvMuestraProgramacion.Rows.Count > 0 Then
                Me.gvMuestraProgramacion.Rows(0).Selected = True
                Me.gvMuestraProgramacion.CurrentCell = Me.gvMuestraProgramacion.Rows(0).Cells("Clv_Txt")
            End If
        Else
            MsgBox("No se encontraron servicios", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub llenaGridProgramacion(ByVal clv_txt As String, ByVal op As Integer)
        Dim query As String = "Exec MuestraProgramacionPPE null,'" + clv_txt + "',null," + op.ToString
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Try
            Dim tabla As DataTable = New DataTable
            Dim com As SqlCommand = New SqlCommand(query, con)
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            Dim bs As BindingSource = New BindingSource
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            Me.gvMuestraProgramacion.DataSource = bs
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            Return
        Finally
            con.Close()
            con.Dispose()
        End Try
        Me.gvMuestraProgramacion.Columns("Clv_Progra").Visible = False
        Me.gvMuestraProgramacion.Columns("Clv_Txt").HeaderText = "Clave"
        Me.gvMuestraProgramacion.Columns("Fecha").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
    End Sub

    Private Sub agregaPeli()
        Dim rowServicios As Integer
        Dim rowProgra As Integer
        Try
            rowServicios = Convert.ToInt32(Me.gvMuestraServicios.CurrentRow.Index.ToString)
            rowProgra = Convert.ToInt32(Me.gvMuestraProgramacion.CurrentRow.Index.ToString)
        Catch ex As Exception
            MsgBox("Faltan datos por seleccionar", MsgBoxStyle.Critical)
            Return
        End Try
        Dim a As Integer
        Dim clvser As String = Me.gvMuestraServicios.Rows(rowServicios).Cells("Clv_Servicio").Value.ToString
        Dim clvpro As String = Me.gvMuestraProgramacion.Rows(rowProgra).Cells("Clv_Progra").Value.ToString
        For a = 0 To gvPelis.Rows.Count - 1
            Dim clvser1 As String = Me.gvPelis.Rows(a).Cells("Clv_Servicio").Value.ToString
            Dim clvpro1 As String = Me.gvPelis.Rows(a).Cells("Clv_Progra").Value.ToString
            If clvser.Equals(clvser1) And clvpro.Equals(clvpro1) Then
                MsgBox("La pelicula que intentas agregar ya existe en la lista", MsgBoxStyle.Critical)
                Return
            End If
        Next a
        Me.gvPelis.Rows.Add(1)
        Dim nRow As Integer = Me.gvPelis.Rows.Count
        Dim j As Integer = 0
        Dim i As Integer
        For i = 0 To Me.gvPelis.Columns.Count - 1
            If i < Me.gvMuestraServicios.Columns.Count Then
                Me.gvPelis.Rows(nRow - 1).Cells(i).Value = Me.gvMuestraServicios.Rows(rowServicios).Cells(i).Value
            Else
                Me.gvPelis.Rows(nRow - 1).Cells(i).Value = Me.gvMuestraProgramacion.Rows(rowProgra).Cells(j).Value
                j = j + 1
            End If
        Next i
        Me.gvPelis.Columns("Fecha").DefaultCellStyle.Format = "d"
        Me.gvPelis.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        Me.gvPelis.Columns("Fecha").AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
    End Sub

    Private Function guardaPelis() As Boolean
        Dim regresa As Boolean
        Dim contrato As String
        Dim clvServicio As String
        Dim clvProgra As String
        Dim conexion As New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand()
        Dim a As Integer
        For a = 0 To Me.gvPelis.Rows.Count - 1
            clvServicio = Me.gvPelis.Rows(a).Cells("Clv_Servicio").Value.ToString
            clvProgra = Me.gvPelis.Rows(a).Cells("Clv_Progra").Value.ToString
            contrato = Me.contratoGlo.ToString
            Dim fecha As String = Me.gvPelis.Rows(a).Cells("Fecha").Value.ToString
            If Me.validaPeliExiste(contrato, clvServicio, clvProgra) = False Then
                Try
                    com.Connection = conexion
                    com.CommandType = CommandType.Text
                    Dim query As String = "Exec GuardaPPEpendientes " + clvServicio + "," + contrato + "," + clvProgra
                    com.CommandText = query
                    com.Connection.Open()
                    com.ExecuteNonQuery()
                    regresa = True
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical)
                    regresa = False
                Finally
                    com.Connection.Close()
                    com.Dispose()
                End Try
                bitsist(GloUsuario, CLng(contrato), LocGloSistema, Me.Text, "Nuevo", "", "Contrato: " + contrato.ToString + " / Servicio: " + clvServicio.ToString + "/ FechaHabilitar: " + fecha, LocClv_Ciudad)
            Else
                MsgBox("La pelicula que intentas agregar, ya la tiene el cliente", MsgBoxStyle.Exclamation)
            End If
        Next a
        Return regresa
    End Function

    Private Function validaPeliExiste(ByVal contrato As String, ByVal clvServicio As String, ByVal clvProgra As String) As Boolean
        Dim valida As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "Select * From ContratacionServiciosPPE Where Contrato = " + contrato + " And Clv_Servicio = " + clvServicio + " And Clv_Progra = " + clvProgra
            com.Connection = conexion
            com.Connection.Open()
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            da.Fill(tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            com.Connection.Close()
            com.Dispose()
        End Try
        If tabla.Rows.Count > 0 Then
            valida = True
        Else
            valida = False
        End If
        tabla.Dispose()
        Return valida
    End Function

    Private Sub limpiaTodo()
        Dim rows As Integer
        Dim i As Integer
        Dim tabla As DataTable = New DataTable
        rows = Me.gvPelis.Rows.Count
        If rows > 0 Then
            Me.gvPelis.Rows.Clear()
        End If
        rows = Me.gvMuestraServicios.Rows.Count
        If rows > 0 Then
            Me.gvMuestraServicios.DataSource = tabla
        End If
        rows = Me.gvMuestraProgramacion.Rows.Count
        If rows > 0 Then
            Me.gvMuestraProgramacion.DataSource = tabla
        End If
        Me.btnAceptar.Enabled = False
        Me.lblClasificacion.Text = ""
        Me.lblClv.Text = ""
        Me.lblDescripcion.Text = ""
        Me.lblHits.Text = ""
        Me.lblPrecio.Text = ""
        Me.lblPuntos.Text = ""
        Me.LblStatus.Text = ""
        Me.txtBuscaContrato.Text = ""
        Me.txtDescripcion.Text = ""
        Me.dtmFecha.Value = DateTime.Today
        Me.Refresh()
    End Sub

    Private Sub txtBuscaContrato_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscaContrato.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.buscaContrato()
        End If
    End Sub

    Private Sub txtDescripcion_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        If e.KeyChar = Chr(13) Then
            Me.btnBusca_Click(Nothing, New System.EventArgs)
        End If
    End Sub

    Private Sub btnLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiar.Click
        Me.limpiaTodo()
    End Sub

    Public Sub DameContrato()
        Me.txtBuscaContrato.Text = FrmSelCliente.CONTRATOLabel1.Text.ToString
        Me.buscaContrato()
    End Sub

    Private Sub buscaContrato()
        Try
            contratoGlo = Long.Parse(Me.txtBuscaContrato.Text.ToString)
        Catch ex As Exception
            MsgBox("El valor introducido, al parecer no es un numero", MsgBoxStyle.Critical)
            Return
        End Try
        Me.LblStatus.Text = Me.obtenerPtsCliente(Me.txtBuscaContrato.Text.ToString)
    End Sub
End Class


