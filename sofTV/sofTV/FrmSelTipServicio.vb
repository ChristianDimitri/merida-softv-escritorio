Imports System.Data.SqlClient
Public Class FrmSelTipServicio

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelTipServicio_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If GloClv_TipSer > 0 Then
            GloBndTipSer = True
        End If
    End Sub

    Private Sub FrmSelTipServicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
        Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)

        GloClv_TipSer = Me.ComboBox4.SelectedValue
        GloNom_TipSer = Me.ComboBox4.Text
        CON.Close()
    End Sub

    Private Sub ComboBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            GloNom_TipSer = Me.ComboBox4.Text
            Me.Close()
        End If
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If Me.ComboBox4.SelectedValue <> Nothing Then
            GloClv_TipSer = Me.ComboBox4.SelectedValue
            GloNom_TipSer = Me.ComboBox4.Text
        End If
    End Sub
End Class