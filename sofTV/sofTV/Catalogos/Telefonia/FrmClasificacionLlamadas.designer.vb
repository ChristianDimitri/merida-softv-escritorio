<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmClasificacionLlamadas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.gboxNuevaClasificacio = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TipoLlamadasCombo = New System.Windows.Forms.ComboBox()
        Me.TodosCheck = New System.Windows.Forms.CheckBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LadaTextBox = New System.Windows.Forms.TextBox()
        Me.EtqClaveLabel = New System.Windows.Forms.Label()
        Me.BuscarButton = New System.Windows.Forms.Button()
        Me.CodigoPaisCheckBox = New System.Windows.Forms.CheckBox()
        Me.CodigMxCheckBox = New System.Windows.Forms.CheckBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.chbxEs_LD = New System.Windows.Forms.CheckBox()
        Me.txtDescripcionClasificacion = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.dgvAux = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNombreClasificacion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gboxNuevaClasificacio.SuspendLayout()
        CType(Me.dgvAux, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(28, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(338, 24)
        Me.Label4.TabIndex = 86
        Me.Label4.Text = "DETALLES DE LA CLASIFICACIÓN"
        '
        'gboxNuevaClasificacio
        '
        Me.gboxNuevaClasificacio.Controls.Add(Me.TextBox1)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Label6)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Button6)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Label3)
        Me.gboxNuevaClasificacio.Controls.Add(Me.TipoLlamadasCombo)
        Me.gboxNuevaClasificacio.Controls.Add(Me.TodosCheck)
        Me.gboxNuevaClasificacio.Controls.Add(Me.ListBox2)
        Me.gboxNuevaClasificacio.Controls.Add(Me.ListBox1)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Label5)
        Me.gboxNuevaClasificacio.Controls.Add(Me.LadaTextBox)
        Me.gboxNuevaClasificacio.Controls.Add(Me.EtqClaveLabel)
        Me.gboxNuevaClasificacio.Controls.Add(Me.BuscarButton)
        Me.gboxNuevaClasificacio.Controls.Add(Me.CodigoPaisCheckBox)
        Me.gboxNuevaClasificacio.Controls.Add(Me.CodigMxCheckBox)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Button4)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Button3)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Button1)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Button5)
        Me.gboxNuevaClasificacio.Controls.Add(Me.chbxEs_LD)
        Me.gboxNuevaClasificacio.Controls.Add(Me.txtDescripcionClasificacion)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Button2)
        Me.gboxNuevaClasificacio.Controls.Add(Me.btnGuardar)
        Me.gboxNuevaClasificacio.Controls.Add(Me.dgvAux)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Label2)
        Me.gboxNuevaClasificacio.Controls.Add(Me.txtNombreClasificacion)
        Me.gboxNuevaClasificacio.Controls.Add(Me.Label1)
        Me.gboxNuevaClasificacio.Location = New System.Drawing.Point(32, 36)
        Me.gboxNuevaClasificacio.Name = "gboxNuevaClasificacio"
        Me.gboxNuevaClasificacio.Size = New System.Drawing.Size(663, 627)
        Me.gboxNuevaClasificacio.TabIndex = 85
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(481, 317)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(60, 22)
        Me.TextBox1.TabIndex = 109
        Me.TextBox1.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(388, 317)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 16)
        Me.Label6.TabIndex = 108
        Me.Label6.Text = "Clave Lada"
        Me.Label6.Visible = False
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(547, 315)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(112, 30)
        Me.Button6.TabIndex = 107
        Me.Button6.Text = "Buscar"
        Me.Button6.UseVisualStyleBackColor = True
        Me.Button6.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(227, 182)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(186, 16)
        Me.Label3.TabIndex = 106
        Me.Label3.Text = "Tipo De La Clasificación :"
        '
        'TipoLlamadasCombo
        '
        Me.TipoLlamadasCombo.DisplayMember = "Clasificacion"
        Me.TipoLlamadasCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoLlamadasCombo.FormattingEnabled = True
        Me.TipoLlamadasCombo.Location = New System.Drawing.Point(166, 201)
        Me.TipoLlamadasCombo.Name = "TipoLlamadasCombo"
        Me.TipoLlamadasCombo.Size = New System.Drawing.Size(318, 28)
        Me.TipoLlamadasCombo.TabIndex = 105
        Me.TipoLlamadasCombo.ValueMember = "Clv_Clasificacion"
        '
        'TodosCheck
        '
        Me.TodosCheck.AutoSize = True
        Me.TodosCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TodosCheck.Location = New System.Drawing.Point(101, 299)
        Me.TodosCheck.Name = "TodosCheck"
        Me.TodosCheck.Size = New System.Drawing.Size(61, 17)
        Me.TodosCheck.TabIndex = 104
        Me.TodosCheck.Text = "Todos"
        Me.TodosCheck.UseVisualStyleBackColor = True
        Me.TodosCheck.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.DisplayMember = "poblacion"
        Me.ListBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 15
        Me.ListBox2.Location = New System.Drawing.Point(408, 363)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.ListBox2.Size = New System.Drawing.Size(242, 214)
        Me.ListBox2.TabIndex = 103
        Me.ListBox2.ValueMember = "lada"
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 15
        Me.ListBox1.Location = New System.Drawing.Point(20, 363)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.ListBox1.Size = New System.Drawing.Size(244, 214)
        Me.ListBox1.TabIndex = 102
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(7, 246)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 16)
        Me.Label5.TabIndex = 101
        Me.Label5.Text = "Buscar Por: "
        Me.Label5.Visible = False
        '
        'LadaTextBox
        '
        Me.LadaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LadaTextBox.Location = New System.Drawing.Point(102, 317)
        Me.LadaTextBox.Name = "LadaTextBox"
        Me.LadaTextBox.Size = New System.Drawing.Size(60, 22)
        Me.LadaTextBox.TabIndex = 100
        '
        'EtqClaveLabel
        '
        Me.EtqClaveLabel.AutoSize = True
        Me.EtqClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EtqClaveLabel.Location = New System.Drawing.Point(9, 317)
        Me.EtqClaveLabel.Name = "EtqClaveLabel"
        Me.EtqClaveLabel.Size = New System.Drawing.Size(87, 16)
        Me.EtqClaveLabel.TabIndex = 99
        Me.EtqClaveLabel.Text = "Clave Lada"
        '
        'BuscarButton
        '
        Me.BuscarButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BuscarButton.Location = New System.Drawing.Point(168, 315)
        Me.BuscarButton.Name = "BuscarButton"
        Me.BuscarButton.Size = New System.Drawing.Size(112, 30)
        Me.BuscarButton.TabIndex = 98
        Me.BuscarButton.Text = "Buscar"
        Me.BuscarButton.UseVisualStyleBackColor = True
        '
        'CodigoPaisCheckBox
        '
        Me.CodigoPaisCheckBox.AutoSize = True
        Me.CodigoPaisCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CodigoPaisCheckBox.Location = New System.Drawing.Point(166, 271)
        Me.CodigoPaisCheckBox.Name = "CodigoPaisCheckBox"
        Me.CodigoPaisCheckBox.Size = New System.Drawing.Size(130, 17)
        Me.CodigoPaisCheckBox.TabIndex = 97
        Me.CodigoPaisCheckBox.Text = "Codigos de Paises"
        Me.CodigoPaisCheckBox.UseVisualStyleBackColor = True
        Me.CodigoPaisCheckBox.Visible = False
        '
        'CodigMxCheckBox
        '
        Me.CodigMxCheckBox.AutoSize = True
        Me.CodigMxCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CodigMxCheckBox.Location = New System.Drawing.Point(10, 271)
        Me.CodigMxCheckBox.Name = "CodigMxCheckBox"
        Me.CodigMxCheckBox.Size = New System.Drawing.Size(133, 17)
        Me.CodigMxCheckBox.TabIndex = 96
        Me.CodigMxCheckBox.Text = "Codigos de México"
        Me.CodigMxCheckBox.UseVisualStyleBackColor = True
        Me.CodigMxCheckBox.Visible = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(270, 390)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(132, 26)
        Me.Button4.TabIndex = 88
        Me.Button4.Text = "Agregar >>"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(270, 488)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(132, 26)
        Me.Button3.TabIndex = 91
        Me.Button3.Text = "Quitar Todos <<"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(270, 456)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(132, 26)
        Me.Button1.TabIndex = 89
        Me.Button1.Text = "Quitar <<"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(270, 422)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(132, 26)
        Me.Button5.TabIndex = 90
        Me.Button5.Text = "Agregar Todos >>"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'chbxEs_LD
        '
        Me.chbxEs_LD.AutoSize = True
        Me.chbxEs_LD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbxEs_LD.Location = New System.Drawing.Point(490, 196)
        Me.chbxEs_LD.Name = "chbxEs_LD"
        Me.chbxEs_LD.Size = New System.Drawing.Size(133, 17)
        Me.chbxEs_LD.TabIndex = 85
        Me.chbxEs_LD.Text = "Es Larga Distancia"
        Me.chbxEs_LD.UseVisualStyleBackColor = True
        Me.chbxEs_LD.Visible = False
        '
        'txtDescripcionClasificacion
        '
        Me.txtDescripcionClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcionClasificacion.Location = New System.Drawing.Point(44, 102)
        Me.txtDescripcionClasificacion.Multiline = True
        Me.txtDescripcionClasificacion.Name = "txtDescripcionClasificacion"
        Me.txtDescripcionClasificacion.Size = New System.Drawing.Size(579, 77)
        Me.txtDescripcionClasificacion.TabIndex = 4
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(353, 586)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(134, 33)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Enabled = False
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(502, 586)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(148, 33)
        Me.btnGuardar.TabIndex = 2
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'dgvAux
        '
        Me.dgvAux.AllowUserToAddRows = False
        Me.dgvAux.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.dgvAux.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvAux.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvAux.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvAux.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.Padding = New System.Windows.Forms.Padding(2)
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAux.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvAux.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAux.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvAux.Location = New System.Drawing.Point(53, 118)
        Me.dgvAux.Name = "dgvAux"
        Me.dgvAux.ReadOnly = True
        Me.dgvAux.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.dgvAux.Size = New System.Drawing.Size(83, 51)
        Me.dgvAux.TabIndex = 83
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(215, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(223, 16)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Descripción de la Clasificación"
        '
        'txtNombreClasificacion
        '
        Me.txtNombreClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombreClasificacion.Location = New System.Drawing.Point(44, 32)
        Me.txtNombreClasificacion.Multiline = True
        Me.txtNombreClasificacion.Name = "txtNombreClasificacion"
        Me.txtNombreClasificacion.Size = New System.Drawing.Size(579, 36)
        Me.txtNombreClasificacion.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(227, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(195, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre de la Clasificación"
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'FrmClasificacionLlamadas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(724, 667)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.gboxNuevaClasificacio)
        Me.MaximizeBox = False
        Me.Name = "FrmClasificacionLlamadas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clasificacio Llamadas"
        Me.gboxNuevaClasificacio.ResumeLayout(False)
        Me.gboxNuevaClasificacio.PerformLayout()
        CType(Me.dgvAux, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gboxNuevaClasificacio As System.Windows.Forms.Panel
    Friend WithEvents chbxEs_LD As System.Windows.Forms.CheckBox
    Friend WithEvents txtDescripcionClasificacion As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents dgvAux As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombreClasificacion As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BuscarButton As System.Windows.Forms.Button
    Friend WithEvents CodigoPaisCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CodigMxCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents LadaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EtqClaveLabel As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TodosCheck As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TipoLlamadasCombo As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
