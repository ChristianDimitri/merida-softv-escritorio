﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Collections.Generic
Imports System.Net.Mail
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Net

Public Class FrmEstadosCuenta

    Private Sub FrmEstadosCuenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        ConEstadoCuentaCliente()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ConEstadoCuentaCliente()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, eGloContrato)
        dgEstadoCuenta.DataSource = BaseII.ConsultaDT("ConEstadoCuentaCliente")

    End Sub

    Private Sub bnVer_Click(sender As Object, e As EventArgs) Handles bnVer.Click
        If dgEstadoCuenta.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Dim imgOxxo As Byte()
        Dim lineaOxxo As String
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.Int, dgEstadoCuenta.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@lineaOxxo", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.ProcedimientoOutPut("DameLineaOxxo")
        lineaOxxo = BaseII.dicoPar("@lineaOxxo")
        imgOxxo = GeneraCodeBar128Nuevo(lineaOxxo)
        ConEstadoCuentaAImprimir(1, Today, dgEstadoCuenta.SelectedCells(0).Value, imgOxxo, lineaOxxo)
    End Sub

    Private Sub ConEstadoCuentaAImprimir(ByVal Op As Integer, ByVal Fecha As Date, ByVal IdEstadoCuenta As Integer, ByVal imgOxxo As Byte(), ByVal lineaOxxo As String)

        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable
        Dim lp As New List(Of String)
        lp.Add("EstadoCuenta")
        lp.Add("DetEstadoCuenta")
        'lp.Add("ReporteAreaTecnicaQuejas1")


        'Dim listatablas As New List(Of String)
        'listatablas.Add("ReporteBitActPaq")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha.ToString)
        BaseII.CreateMyParameter("@IdEstadoCuenta", SqlDbType.BigInt, IdEstadoCuenta)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@imageOxxo", SqlDbType.Image, imgOxxo)
        BaseII.CreateMyParameter("@lineaOxxo", SqlDbType.VarChar, lineaOxxo, 17)
        dataSet = BaseII.ConsultaDS("ConEstadoCuentaAImprimir", lp)
        Try
            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"
            dataTable = MuestraGeneral()
            dataSet.Tables.Add(dataTable)
            dataSet.Tables(2).TableName = "General"
            'For Each dr As DataRow In dataSet.Tables(0).Rows
            '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
            'Next

            reportDocument.Load(RutaReportes + "\ReportEstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)


            FrmImprimirCentralizada.rd = reportDocument
            FrmImprimirCentralizada.Show()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub bnSalir_Click(sender As Object, e As EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Function MuestraGeneral() As DataTable
        BaseII.limpiaParametros()
        Return BaseII.ConsultaDT("MuestraGeneral")
    End Function

    Private Sub btnReenviar_Click(sender As Object, e As EventArgs) Handles btnReenviar.Click
        If dgEstadoCuenta.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Dim imgOxxo As Byte()
        Dim lineaOxxo As String
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.Int, dgEstadoCuenta.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@lineaOxxo", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.ProcedimientoOutPut("DameLineaOxxo")
        lineaOxxo = BaseII.dicoPar("@lineaOxxo")
        imgOxxo = GeneraCodeBar128Nuevo(lineaOxxo)
        ConEstadoCuentaAReenviar(1, Today, dgEstadoCuenta.SelectedCells(0).Value, imgOxxo, lineaOxxo)
    End Sub

    Private Sub ConEstadoCuentaAReenviar(ByVal Op As Integer, ByVal Fecha As Date, ByVal IdEstadoCuenta As Integer, ByVal imgOxxo As Byte(), ByVal lineaOxxo As String)

        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable
        Dim lp As New List(Of String)
        lp.Add("EstadoCuenta")
        lp.Add("DetEstadoCuenta")
        'lp.Add("ReporteAreaTecnicaQuejas1")


        'Dim listatablas As New List(Of String)
        'listatablas.Add("ReporteBitActPaq")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha.ToString)
        BaseII.CreateMyParameter("@IdEstadoCuenta", SqlDbType.BigInt, IdEstadoCuenta)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@imageOxxo", SqlDbType.Image, imgOxxo)
        BaseII.CreateMyParameter("@lineaOxxo", SqlDbType.VarChar, lineaOxxo, 17)
        dataSet = BaseII.ConsultaDS("ConEstadoCuentaAImprimir", lp)
        Try
            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"
            dataTable = MuestraGeneral()
            dataSet.Tables.Add(dataTable)
            dataSet.Tables(2).TableName = "General"
            'For Each dr As DataRow In dataSet.Tables(0).Rows
            '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
            'Next

            reportDocument.Load(RutaReportes + "\ReportEstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)


            reportDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, RutaReportes + "\EstadosCuenta\EdoCuenta" + eGloContrato.ToString() + ".pdf")
            enviarEstadoCuenta()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub enviarEstadoCuenta()
        Dim mailMessage As MailMessage
        Dim attachment As Attachment
        Try
            ConGeneralCorreo()
            ConEstadoCuentaMensaje()

            mailMessage = New MailMessage()
            attachment = New Attachment(RutaReportes + "\EstadosCuenta\EdoCuenta" + eGloContrato.ToString() + ".pdf")

            Dim smtpClient As SmtpClient = New SmtpClient()
            mailMessage.From = New MailAddress(CuentaCorreo)
            mailMessage.To.Add(EmailEnvioCorreo)
            mailMessage.Subject = MensajeCorreo
            mailMessage.Body = AsuntoCorreo
            mailMessage.IsBodyHtml = True
            mailMessage.Attachments.Add(attachment)

            smtpClient.UseDefaultCredentials = False
            smtpClient.Credentials = New System.Net.NetworkCredential(CuentaCorreo, PasswordCorreo)
            smtpClient.Port = PortCorreo
            smtpClient.Host = HostCorreo
            smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network
            smtpClient.EnableSsl = True
            'ServicePointManager.ServerCertificateValidationCallback = Function(s as Object, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true
            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslerror As System.Net.Security.SslPolicyErrors) True
            smtpClient.Send(mailMessage)

            MsgBox("Envio con éxito")
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Finally
            mailMessage.Dispose()
            Attachment.Dispose()
        End Try
    End Sub

    Private Sub ConGeneralCorreo()
        Dim dt As DataTable
        BaseII.limpiaParametros()
        dt = BaseII.ConsultaDT("ConGeneralCorreo")

        For Each row As DataRow In dt.Rows

            CuentaCorreo = row("Cuenta").ToString
            PasswordCorreo = row("Password").ToString
            HostCorreo = row("Host").ToString
            PortCorreo = row("Port").ToString
        Next
    End Sub

    Private Sub ConEstadoCuentaMensaje()
        Dim dt As DataTable
        BaseII.limpiaParametros()
        dt = BaseII.ConsultaDT("ConEstadoCuentaMensaje")

        For Each row As DataRow In dt.Rows
            AsuntoCorreo = "Reenvio " + row("Mensaje").ToString
            MensajeCorreo = "Reenvio " + row("Asunto").ToString
        Next
    End Sub
End Class