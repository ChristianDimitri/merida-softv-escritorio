Imports System.Data.SqlClient
Public Class FrmDepOrd
    Dim eCont As Integer

    Private Sub FrmDepOrd_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker1.MaxDate = Today
        Me.DateTimePicker2.Value = Today
        Me.DateTimePicker2.MinDate = Today
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DepurarOrdenesTableAdapter.Connection = CON
        Me.DepurarOrdenesTableAdapter.Fill(Me.DataSetEric.DepurarOrdenes, Me.DateTimePicker1.Value, Me.DateTimePicker2.Value, eCont)
        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Depuraron las Ordenes", "", "Se Depuraron Las Ordenes de Servicio Entre" + Me.DateTimePicker1.Text + " y " + Me.DateTimePicker2.Text, LocClv_Ciudad)
        MsgBox("N�mero de �rdenes Depuradas: " & eCont, , "Atenci�n")
        CON.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class