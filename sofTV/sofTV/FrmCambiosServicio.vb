﻿Imports System.Data.SqlClient



Public Class FrmCambiosServicio

    Dim clv_TipSer As Integer = 0
    Dim Clv_Servicio As Integer = 0
    Dim contratoNet As Long = 0
    Dim contrato As Long = 0
    Dim clv_TipSerNew As Integer = 0
    Dim Clv_ServicioNew As Integer = 0
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim clv_Combo As Integer = 0
    Dim clv_combOld As Integer = 0
    Dim Descrip As String = Nothing
    Dim tipCliente As Integer = 0
    Dim bandName1 As Boolean = False
    Dim bandName2 As Boolean = False
    Dim bandname3 As Boolean = False
    Dim Result As Integer = 0
    Dim Clv_TipSerCombo As Integer = 0

    Public Function DameServCte(ByVal oContrato As Long) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
        Dim tblDameServCte As DataTable = BaseII.ConsultaDT("DameServCte_New") 'Ejecuto Stored Procedure que regresa el listado con la Agenda

        'Regresamos tabla 
        Return tblDameServCte
    End Function

    Private Sub Llena_Servicios_del_Cliente(ByVal oContrato As Integer)
        If IsNumeric(TxtContrato.Text) Then
            Me.GridServiciosDelCliente.DataSource = DameServCte(oContrato)
        End If
        '    Me.GridServiciosDelCliente.Columns(0).Visible = False
        '    Me.GridServiciosDelCliente.Columns(1).Visible = False
        '    Me.GridServiciosDelCliente.Columns(2).Visible = False
        '    Me.GridServiciosDelCliente.Columns(3).Visible = True
        '    Me.GridServiciosDelCliente.Columns(4).Visible = False
        '    Me.GridServiciosDelCliente.Columns(5).Visible = False
        '    Me.GridServiciosDelCliente.Columns(6).Visible = True
        '    'Me.GridServiciosDelCliente.Columns(3).HeaderText = "Fecha De Programación"
        '    Me.GridServiciosDelCliente.Columns(0).HeaderText = "Contrato"
        '    Me.GridServiciosDelCliente.Columns(1).HeaderText = "ContratoNet"
        '    Me.GridServiciosDelCliente.Columns(2).HeaderText = "TipSer"
        '    Me.GridServiciosDelCliente.Columns(3).HeaderText = "Descripción"
        '    Me.GridServiciosDelCliente.Columns(3).Width = 250
        '    Me.GridServiciosDelCliente.Columns(4).HeaderText = "Clv_Servicio"
        '    Me.GridServiciosDelCliente.Columns(5).HeaderText = "Clv_Cablemodem"
        '    Me.GridServiciosDelCliente.Columns(6).HeaderText = "Aparato"
        '    Me.GridServiciosDelCliente.Columns(6).Width = 100
        '    '28/06/2011
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridServiciosDelCliente.CellContentClick
        If GridServiciosDelCliente.RowCount > 0 Then
            clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
            Clv_Servicio = Me.GridServiciosDelCliente.SelectedCells(4).Value.ToString
            contratoNet = Me.GridServiciosDelCliente.SelectedCells(1).Value.ToString
            'COLOCAR LEYENDA
            If RadioButton1.Checked = True Then
                BtnCamSer()
            End If
            If RadioButton2.Checked = True Then
                BtnCambioTipSer()
            End If
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            BtnCambioTipSer()
        End If
    End Sub

    Private Sub BtnCambioTipSer()
        If GridServiciosDelCliente.RowCount > 0 Then
            Llena_Posible_TipServ()
            If GridDiferenteTipoServicio.RowCount > 0 Then
                clv_TipSerNew = GridDiferenteTipoServicio.SelectedCells(2).Value.ToString
                Clv_ServicioNew = GridDiferenteTipoServicio.SelectedCells(0).Value.ToString
                If bandName2 = False Then
                    bandName2 = True
                    If clv_TipSer = 1 Then
                        lblServicio.Text = "Analógicos"
                        RadioButton2.Text = "Cambio de Analógico a "
                        ' RadioButton1.Text = "Analógico"
                    End If
                    If clv_TipSer = 3 Then
                        lblServicio.Text = "Digitales"
                        RadioButton2.Text = "Cambio de Digital a "
                        'RadioButton1.Text = "Analógico"
                    End If
                    If clv_TipSerNew = 1 Then
                        RadioButton2.Text = RadioButton2.Text + "Analógico"
                        LblTvs.Text = "No. de Tvs Con Pago: "
                        LblTvs2.Text = "No. de Tvs Sin Pago: "
                        LblTvs2.Enabled = True
                        UpDownTVS2.Enabled = True
                        UpDownTVS2.Value = 0
                        UpDownTVS.Value = 0
                    End If
                    If clv_TipSerNew = 3 Then
                        RadioButton2.Text = RadioButton2.Text + "Digital"
                        LblTvs.Text = "No. de Cajas: "
                        LblTvs2.Text = "No. "
                        LblTvs2.Enabled = False
                        UpDownTVS2.Enabled = False
                        UpDownTVS2.Value = 1
                        UpDownTVS.Value = 1
                    End If
                End If
            End If
        End If
        RadioButton2.Checked = True
        GridDiferenteTipoServicio.Enabled = True
        PnlPorTipoServicio.Enabled = True
        RadioButton1.Checked = False
        GridMismoServicio.Enabled = False
        RadioButton3.Checked = False
        dgrCombo.Enabled = False
        LimpiaServicios()
        LimpiaCombos()
    End Sub

    Private Sub TxtContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtContrato.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If IsNumeric(TxtContrato.Text) Then
                'Llena_Servicios_del_Cliente(Me.TxtContrato.Text)
                DatosCliente()
                If Me.TxtContrato.Text = "" Then
                    MsgBox("Cliente no encontrato!", MsgBoxStyle.Information)
                    Exit Sub
                End If
                contrato = Me.TxtContrato.Text
                Llena_Servicios_del_Cliente(Me.TxtContrato.Text)
                If GridServiciosDelCliente.RowCount > 0 Then
                    clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
                    Clv_Servicio = Me.GridServiciosDelCliente.SelectedCells(4).Value.ToString
                    contratoNet = Me.GridServiciosDelCliente.SelectedCells(1).Value.ToString
                    bandName2 = False
                    If clv_TipSer = 1 Then
                        lblServicio.Text = "Analógicos"
                        RadioButton2.Text = "Cambio de Analógico a "
                        lblCamServ.Text = "Analógico"
                    End If
                    If clv_TipSer = 3 Then
                        lblServicio.Text = "Digitales"
                        RadioButton2.Text = "Cambio de Digital a "
                        lblCamServ.Text = "Digitales"
                    End If
                    LblTvs.Text = "No. "
                    LblTvs2.Text = "No. "
                    Llena_Posibles_Servicios()
                    If GridMismoServicio.RowCount > 0 Then
                        Clv_ServicioNew = Me.GridMismoServicio.SelectedCells(0).Value.ToString
                        clv_TipSerNew = clv_TipSer
                    End If
                    RadioButton1.Checked = True
                    GridMismoServicio.Enabled = True
                    RadioButton2.Checked = False
                    GridDiferenteTipoServicio.Enabled = False
                    RadioButton3.Checked = False
                    dgrCombo.Enabled = False
                    LimpiaTipoServicio()
                Else
                    LimpiaTipoServicio()
                    LimpiaServicios()
                    MsgBox("Cliente no cuenta con servicios asignados!", MsgBoxStyle.Information)
                End If
                PnlPorTipoServicio.Enabled = False
            End If
        End If
    End Sub

    Private Sub BtnBuscarCliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBuscarCliente.Click
        eContrato = 0
        'Dim SelContrato As New BrwSelContrato
        'SelContrato.ShowDialog()
        Dim SelContrato As New BrwSelContrato55
        SelContrato.ShowDialog()
        If SelContrato.DialogResult = Windows.Forms.DialogResult.OK Then
            If eGloContrato > 0 Then
                Me.TxtContrato.Text = eGloContrato
                contrato = eGloContrato
            End If
            DatosCliente()
            If Me.TxtContrato.Text = "" Then
                MsgBox("Cliente no encontrato!", MsgBoxStyle.Information)
                Exit Sub
            End If
            Llena_Servicios_del_Cliente(Me.TxtContrato.Text)
            If GridServiciosDelCliente.RowCount > 0 Then
                clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
                Clv_Servicio = Me.GridServiciosDelCliente.SelectedCells(4).Value.ToString
                contratoNet = Me.GridServiciosDelCliente.SelectedCells(1).Value.ToString
                bandName2 = False
                If clv_TipSer = 1 Then
                    lblServicio.Text = "Analógicos"
                    lblCamServ.Text = "Analógicos"
                    RadioButton2.Text = "Cambio de Analógico a "
                    ' RadioButton1.Text = "Analógico"
                End If
                If clv_TipSer = 3 Then
                    lblServicio.Text = "Digitales"
                    lblCamServ.Text = "Digitales"
                    RadioButton2.Text = "Cambio de Digital a "
                    'RadioButton1.Text = "Analógico"
                End If
                LblTvs.Text = "No. "
                LblTvs2.Text = "No. "
                Llena_Posibles_Servicios()
                If GridMismoServicio.RowCount > 0 Then
                    Clv_ServicioNew = Me.GridMismoServicio.SelectedCells(0).Value.ToString
                    clv_TipSerNew = clv_TipSer
                End If
                RadioButton1.Checked = True
                GridMismoServicio.Enabled = True
                RadioButton2.Checked = False
                GridDiferenteTipoServicio.Enabled = False
                RadioButton3.Checked = False
                dgrCombo.Enabled = False
                LimpiaTipoServicio()
            Else
                LimpiaServicios()
                LimpiaTipoServicio()
                MsgBox("Cliente no cuenta con servicios asignados!", MsgBoxStyle.Information)
            End If
            PnlPorTipoServicio.Enabled = False
        End If
    End Sub

    Private Sub DatosCliente()
        Try
            Dim dataTable As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Me.TxtContrato.Text)
            dataTable = BaseII.ConsultaDT("uspDatosCliente")
            If dataTable.Rows.Count > 0 Then
                TxtContrato.Text = dataTable.Rows(0)(0).ToString()
                TxtNombre.Text = dataTable.Rows(0)(1).ToString()
                TxtDireccion.Text = dataTable.Rows(0)(2).ToString()
            Else
                TxtContrato.Text = ""
                TxtNombre.Text = ""
                TxtDireccion.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Llena_Posibles_Servicios()
        Try
            'clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
            'Clv_Servicio = Me.GridServiciosDelCliente.SelectedCells(4).Value.ToString
            Dim dataTable As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(TxtContrato.Text))
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(clv_TipSer))
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, CInt(Clv_Servicio))
            BaseII.CreateMyParameter("@ContratoNet", SqlDbType.BigInt, contratoNet)
            dataTable = BaseII.ConsultaDT("DameServPosiblesCte_New")
            GridMismoServicio.DataSource = dataTable
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LimpiaServicios()
        Try
            'clv_TipSer = Me.GridServiciosDelCliente.SelectedCells(2).Value.ToString
            'Clv_Servicio = Me.GridServiciosDelCliente.SelectedCells(4).Value.ToString
            Dim dataTable As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, CInt(0))
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(55))
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, CInt(0))
            BaseII.CreateMyParameter("@ContratoNet", SqlDbType.BigInt, contratoNet)
            dataTable = BaseII.ConsultaDT("DameServPosiblesCte_New")
            GridMismoServicio.DataSource = dataTable
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            BtnCamSer() 
        End If
    End Sub

    Private Sub BtnCamSer()
        If GridServiciosDelCliente.RowCount > 0 Then
            If clv_TipSer = 1 Then
                lblCamServ.Text = "Analógicos"
                lblServicio.Text = "Analógicos"
            ElseIf clv_TipSer = 3 Then
                lblCamServ.Text = "Digitales"
                lblServicio.Text = "Digitales"
            End If
            Llena_Posibles_Servicios()
            If GridMismoServicio.RowCount > 0 Then
                clv_TipSerNew = clv_TipSer
                Clv_ServicioNew = Me.GridMismoServicio.SelectedCells(0).Value.ToString
                'Me.GridMismoServicio.Enabled = True
                'Else
                '    Me.GridMismoServicio.Enabled = False
            End If
        End If
        RadioButton1.Checked = True
        GridMismoServicio.Enabled = True
        RadioButton2.Checked = False
        PnlPorTipoServicio.Enabled = False
        GridDiferenteTipoServicio.Enabled = False
        RadioButton3.Checked = False
        dgrCombo.Enabled = False
        LimpiaTipoServicio()
        LimpiaCombos()
    End Sub

    Private Sub Llena_Posible_TipServ()
        Dim dataTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, TxtContrato.Text)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, clv_TipSer)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@ContratoNet", SqlDbType.BigInt, contratoNet)
        BaseII.CreateMyParameter("@Cajas", SqlDbType.Int, UpDownTVS.Value)
        BaseII.CreateMyParameter("@Cajas2", SqlDbType.Int, UpDownTVS2.Value)
        dataTable = BaseII.ConsultaDT("DameTipServPosiblesCte_New")
        GridDiferenteTipoServicio.DataSource = dataTable
    End Sub
    Private Sub LimpiaTipoServicio()
        Dim dataTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, 0)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, 55)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@ContratoNet", SqlDbType.BigInt, contratoNet)
        BaseII.CreateMyParameter("@Cajas", SqlDbType.Int, UpDownTVS.Value)
        BaseII.CreateMyParameter("@Cajas2", SqlDbType.Int, UpDownTVS2.Value)
        dataTable = BaseII.ConsultaDT("DameTipServPosiblesCte_New")
        GridDiferenteTipoServicio.DataSource = dataTable
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        'Bnd = True
        Me.Close()
    End Sub

    Private Sub BtnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGuardar.Click
        If GridServiciosDelCliente.RowCount > 0 Then
            'contrato= eGloContrato,clv_TipSer,Clv_Servicio,contratoNet
            ValidaExist()
            If Result = 1 Then
                Result = 0
                MsgBox("Este contrato tiene un cambio pendiente!")
                Exit Sub
            End If
            If RadioButton1.Checked = True Then
                If GridMismoServicio.RowCount > 0 Then
                    CambiaServCliente()
                    MsgBox("Se guardó con éxito!", MsgBoxStyle.Information)
                    Me.Close()
                Else
                    MsgBox("No existe servicio a asignar")
                    Exit Sub
                End If
            ElseIf RadioButton2.Checked = True Then
                If GridDiferenteTipoServicio.RowCount > 0 Then
                    If clv_TipSerNew = 3 Then
                        If IsNumeric(UpDownTVS.Value) And UpDownTVS.Value > 0 Then
                            CambiaTipSerCliente()
                            MsgBox("Se guardó con éxito!", MsgBoxStyle.Information)
                            Me.Close()
                        Else
                            MsgBox("Numero de cajas debe de ser mayor a cero")
                        End If
                    End If
                    If clv_TipSerNew = 1 Then
                        CambiaTipSerCliente()
                        MsgBox("Se guardó con éxito!", MsgBoxStyle.Information)
                        Me.Close()
                    End If
                Else
                    MsgBox("No existe saervicio a asignar")
                End If
            ElseIf RadioButton3.Checked = True Then
                If dgrCombo.RowCount > 0 Then
                    CambiaComboCliente()
                    MsgBox("Se guardó con éxito!", MsgBoxStyle.Information)
                    Me.Close()
                Else
                    MsgBox("No se tiene combo por asignar")
                End If
            End If
        Else
            MsgBox("El cliente no cuenta con servicios asignados")
            Exit Sub
        End If
        'Dim CON As New SqlConnection(MiConexion)
        'Try
        'If IdSistema = "LO" Or IdSistema = "YU" Then
        '    If CInt(Me.DataGridViewActual.SelectedCells.Item(3).Value) = 0 Or CInt(Me.DataGridViewPosible.SelectedCells.Item(0).Value) = 0 Then
        '        MsgBox("Selecciona el servicio a cambiar.")
        '        Exit Sub
        '    End If
        'Else
        '    If Me.DameServCteDataGridView.RowCount = 0 Or Me.DameServPosiblesCteDataGridView.RowCount = 0 Or IsNumeric(Me.ConceptoComboBox.SelectedValue) = False Then
        '        MsgBox("Selecciona el servicio a cambiar.")
        '        Exit Sub
        '    End If
        'End If

        'If IdSistema = "LO" Or IdSistema = "YU" Then
        '    eClv_ServicioOld = CInt(Me.DataGridViewActual.SelectedCells.Item(3).Value)
        '    eClv_ServicioNew = CInt(Me.DataGridViewPosible.SelectedCells.Item(0).Value)
        '    eContratoAux = CLng(Me.DataGridViewActual.SelectedCells.Item(0).Value)
        '    eContratoNet = CLng(Me.DataGridViewActual.SelectedCells.Item(1).Value)
        '    eClv_TipSer = CInt(Me.DataGridViewActual.SelectedCells.Item(2).Value)
        'Else
        '    eClv_ServicioOld = CInt(Me.Clv_ServicioTextBox.Text)
        '    eClv_ServicioNew = CInt(Me.Clv_ServicioTextBox1.Text)
        '    eContratoAux = CLng(Me.ContratoTextBox.Text)
        '    If IsNumeric(Me.ContratoNetTextBox.Text) = True Then eContratoNet = Me.ContratoNetTextBox.Text Else eContratoNet = 0
        '    eClv_TipSer = CInt(Me.ConceptoComboBox.SelectedValue)
        'End If

        'ValidaCambioServCliente(eContratoAux, eClv_TipSer, String.Empty)
        'If eRes = 1 Then
        '    MsgBox(eMsg, MsgBoxStyle.Information)

        '    Exit Sub
        'End If

        'CambiaServCliente(eContratoAux, eContratoNet, eClv_TipSer, eClv_ServicioOld, eClv_ServicioNew)
        'bitsist(GloUsuario, eContratoAux, LocGloSistema, Me.Text, "", "Cambio de Servicio de: " + CStr(eClv_ServicioOld) + " a " + CStr(eClv_ServicioNew), GloSucursal, LocClv_Ciudad)

        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub

    'Private Sub ValidaCambioServCliente(ByVal Contrato As Long, ByVal Clv_TipSer As Integer, ByVal Mac As String)

    '    Dim conexion As New SqlConnection(MiConexion)
    '    Dim comando As New SqlCommand("ValidaCambioServCliente", conexion)
    '    comando.CommandType = CommandType.StoredProcedure

    '    Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
    '    parametro.Direction = ParameterDirection.Input
    '    parametro.Value = Contrato
    '    comando.Parameters.Add(parametro)

    '    Dim parametro1 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
    '    parametro1.Direction = ParameterDirection.Input
    '    parametro1.Value = Clv_TipSer
    '    comando.Parameters.Add(parametro1)

    '    Dim parametro2 As New SqlParameter("@Mac", SqlDbType.VarChar, 50)
    '    parametro2.Direction = ParameterDirection.Input
    '    parametro2.Value = Mac
    '    comando.Parameters.Add(parametro2)

    '    Dim parametro3 As New SqlParameter("@Res", SqlDbType.Int)
    '    parametro3.Direction = ParameterDirection.Output
    '    parametro3.Value = 0
    '    comando.Parameters.Add(parametro3)

    '    Dim parametro4 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
    '    parametro4.Direction = ParameterDirection.Output
    '    parametro4.Value = String.Empty
    '    comando.Parameters.Add(parametro4)

    '    Try
    '        eRes = 0
    '        eMsg = String.Empty
    '        conexion.Open()
    '        comando.ExecuteNonQuery()
    '        conexion.Close()
    '        eRes = CInt(parametro3.Value.ToString)
    '        eMsg = parametro4.Value.ToString

    '    Catch ex As Exception
    '        conexion.Close()
    '        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
    '    End Try

    'End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If RadioButton3.Checked = True Then
            If GridServiciosDelCliente.RowCount > 0 Then
                EsCombo()
                If Len(Descrip) > 0 And clv_combOld > 0 Then
                    lblServicio.Text = Descrip
                    bandName2 = False
                End If
                Llena_Posibles_Combos()
                If dgrCombo.RowCount > 0 Then
                    clv_Combo = dgrCombo.SelectedCells(0).Value.ToString
                    Clv_TipSerCombo = dgrCombo.SelectedCells(3).Value.ToString
                    If Clv_TipSerCombo = 1 Then
                        lblComboCajas.Text = "No. de Tvs: "
                    ElseIf Clv_TipSerCombo = 3 Then
                        lblComboCajas.Text = "No. de Cajas: "
                    End If
                End If
            End If
            RadioButton1.Checked = False
            GridMismoServicio.Enabled = False
            RadioButton2.Checked = False
            PnlPorTipoServicio.Enabled = False
            GridDiferenteTipoServicio.Enabled = False
            RadioButton3.Checked = True
            dgrCombo.Enabled = True
            LimpiaTipoServicio()
            LimpiaServicios()
        End If
    End Sub
    Private Sub Llena_Posibles_Combos()
        Dim datTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, 1)
        If clv_combOld > 0 And tipCliente > 0 Then
            BaseII.CreateMyParameter("@descuento", SqlDbType.Int, clv_combOld)
            BaseII.CreateMyParameter("@tipCliente", SqlDbType.Int, tipCliente)
        Else
            BaseII.CreateMyParameter("@descuento", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@tipCliente", SqlDbType.Int, 0)
        End If
        datTable = BaseII.ConsultaDT("DameServCombo_New")
        dgrCombo.DataSource = datTable
    End Sub
    Private Sub LimpiaCombos()
        Dim datTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@descuento", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@tipCliente", SqlDbType.Int, 0)
        datTable = BaseII.ConsultaDT("DameServCombo_New")
        dgrCombo.DataSource = datTable
    End Sub

    Private Sub GridMismoServicio_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridMismoServicio.CellContentClick
        Clv_ServicioNew = Me.GridMismoServicio.SelectedCells(0).Value.ToString
        'COLOCAR LEYENDA
    End Sub

    Private Sub GridDiferenteTipoServicio_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridDiferenteTipoServicio.CellContentClick
        clv_TipSerNew = Me.GridDiferenteTipoServicio.SelectedCells(2).Value.ToString
        Clv_ServicioNew = Me.GridDiferenteTipoServicio.SelectedCells(0).Value.ToString
        'COLOCAR LEYENDA
    End Sub

    Private Sub dgrCombo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgrCombo.CellContentClick
        If dgrCombo.RowCount > 0 Then
            clv_Combo = Me.dgrCombo.SelectedCells(0).Value.ToString
            Clv_TipSerCombo = dgrCombo.SelectedCells(3).Value.ToString
            If Clv_TipSerCombo = 1 Then
                lblComboCajas.Text = "No. de Tvs: "
            ElseIf Clv_TipSerCombo = 3 Then
                lblComboCajas.Text = "No. de Cajas: "
            End If
        End If
    End Sub

    Private Sub CambiaServCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contrato)
            BaseII.CreateMyParameter("@ContratoNet", SqlDbType.BigInt, contratoNet)
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, clv_TipSer)
            BaseII.CreateMyParameter("@Clv_ServOld", SqlDbType.Int, Clv_Servicio)
            BaseII.CreateMyParameter("@Clv_ServNew", SqlDbType.Int, Clv_ServicioNew)
            BaseII.Inserta("CambiaServCliente_New")
        Catch ex As Exception
            MsgBox("Error al Guardar", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub CambiaTipSerCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contrato)
            BaseII.CreateMyParameter("@ContratoNet", SqlDbType.BigInt, contratoNet)
            BaseII.CreateMyParameter("@Clv_TipSerOld", SqlDbType.Int, clv_TipSer)
            BaseII.CreateMyParameter("@Clv_ServOld", SqlDbType.Int, Clv_Servicio)
            BaseII.CreateMyParameter("@Clv_ServNew", SqlDbType.Int, Clv_ServicioNew)
            BaseII.CreateMyParameter("@Clv_TipSerNew", SqlDbType.Int, clv_TipSerNew)
            BaseII.CreateMyParameter("@Cajas", SqlDbType.Int, UpDownTVS.Value)
            BaseII.CreateMyParameter("@Cajas2", SqlDbType.Int, UpDownTVS2.Value)
            BaseII.Inserta("CambiaTipServCliente_New")
        Catch ex As Exception
            MsgBox("Error al Guardar", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub ValidaExist()
        Try
            Dim con As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("uspExisteCambio", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
            par.Value = contrato
            cmd.Parameters.Add(par)

            Dim par1 As New SqlParameter("@Exist", SqlDbType.Int)
            par1.Direction = ParameterDirection.Output
            par1.Value = 0
            cmd.Parameters.Add(par1)

            con.Open()
            cmd.ExecuteNonQuery()
            Result = par1.Value
            con.Close()
            con.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub EsCombo()
        Try
            Dim con As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("uspEsCombo", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
            par.Value = contrato
            cmd.Parameters.Add(par)

            Dim par1 As New SqlParameter("@Descripcion", SqlDbType.VarChar)
            par1.Direction = ParameterDirection.Output
            par1.Value = ""
            cmd.Parameters.Add(par1)

            Dim par2 As New SqlParameter("@descuento", SqlDbType.Int)
            par2.Direction = ParameterDirection.Output
            par2.Value = 0
            cmd.Parameters.Add(par2)

            Dim par3 As New SqlParameter("@TipClient", SqlDbType.Int)
            par3.Direction = ParameterDirection.Output
            par3.Value = 0
            cmd.Parameters.Add(par3)

            con.Open()
            cmd.ExecuteNonQuery()
            Descrip = par1.Value
            clv_combOld = par2.Value
            tipCliente = par3.Value

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CambiaComboCliente()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, contrato)
            If clv_combOld > 0 And tipCliente > 0 Then
                BaseII.CreateMyParameter("@DescuentoOld", SqlDbType.Int, clv_combOld)
                BaseII.CreateMyParameter("@tipCliente", SqlDbType.Int, tipCliente)
            Else
                BaseII.CreateMyParameter("@DescuentoOld", SqlDbType.Int, 0)
                BaseII.CreateMyParameter("@tipCliente", SqlDbType.Int, 0)
            End If
            BaseII.CreateMyParameter("@TipSer", SqlDbType.Int, Clv_TipSerCombo)
            If Clv_TipSerCombo > 0 Then
                BaseII.CreateMyParameter("@Cajas", SqlDbType.Int, CajasCombo.Value)
            Else
                BaseII.CreateMyParameter("@Cajas", SqlDbType.Int, 0)
            End If
            BaseII.CreateMyParameter("@DescuentoNew", SqlDbType.Int, clv_Combo)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FrmCambiosServicio_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub UpDownTVS_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpDownTVS.ValueChanged
        If GridServiciosDelCliente.RowCount > 0 Then
            Llena_Posible_TipServ()
        End If
    End Sub

    Private Sub UpDownTVS2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpDownTVS2.ValueChanged
        If GridServiciosDelCliente.RowCount > 0 Then
            Llena_Posible_TipServ()
        End If
    End Sub
End Class