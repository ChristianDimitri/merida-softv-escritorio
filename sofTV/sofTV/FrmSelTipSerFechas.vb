Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelTipSerFechas

    Private Sub MuestraTipServEric(ByVal Clv_TipSer As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipServEric ")
        strSQL.Append(CStr(Clv_TipSer) & ", ")
        strSQL.Append(CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub DameClv_Session()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmSelTipSerFechas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
        MuestraTipServEric(0, 0)
        DameClv_Session()
        If eOpVentas = 73 Then
            Me.Text = "Cancelaciones por Ejecutivo"
        End If
        If rdesconexiones = True Then
            Me.Text = "Historial Desconexiones"
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eTipSer = Me.ComboBox1.SelectedValue
        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value
        eEjecutivo = True
        If rdesconexiones = True Then
            rdesconexiones = False
            Dim reporte As ImprimirHistorialDesconexiones = New ImprimirHistorialDesconexiones
            reporte.ReporteDesconexiones(eFechaIni, eFechaFin, eTipSer)
            reporte.Show()
            Me.Close()
            Return
        End If
        FrmSelServicioE.Show()
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class