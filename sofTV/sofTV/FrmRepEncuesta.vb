﻿Imports System.Data.SqlClient
Public Class FrmRepEncuesta

    Private Sub ComboBoxEncuesta_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEncuesta.SelectedIndexChanged

    End Sub
    Private Sub Label1_Click(sender As System.Object, e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub FrmRepEncuesta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)

        MuestraEncuestas(0)

        Me.DateTimePicker1.MaxDate = Today
        Me.DateTimePicker2.MaxDate = Today
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
    End Sub

    Private Sub MuestraEncuestas(ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC MuestraEncuestas " & CStr(Op) & ",'',''", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxEncuesta.DataSource = bindingSource
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButtonSalir_Click(sender As System.Object, e As System.EventArgs) Handles ButtonSalir.Click

        If DateTimePicker1.Value > DateTimePicker2.Value Then
            MsgBox("La fecha inicial no puede ser mayor a la fecha final", MsgBoxStyle.Information)
            Exit Sub
        End If

        eOpVentas = 76
        GloEncFecIni = Me.DateTimePicker1.Value
        GloEncFecFin = Me.DateTimePicker2.Value
        eIDEncuesta = ComboBoxEncuesta.SelectedValue 'Guarda el IDEncuesta
        FrmImprimirComision.Show()

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class