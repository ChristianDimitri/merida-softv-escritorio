﻿Imports System.Data.SqlClient
Public Class FrmPuestoTEc
    Dim Descripcion As String = Nothing
    Private Sub guardabitacora(ByVal op)
        If opcion = "M" Then
            If op = 0 Then
                'Descripcion = Me.DescripcionTextBox.Text
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.DescripcionTextBox.Name + ",Clave: " + CStr(gloClave), Descripcion, Me.DescripcionTextBox.Text, LocClv_Ciudad)
                damedatosbitacora()
            ElseIf op = 1 Then
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Puesto Del Técnico", "", "Se Elimino Un Puesto Del Técnico:" + CStr(gloClave), LocClv_Ciudad)
            End If
        ElseIf opcion = "N" Then
            bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Creo Un Puesto Del Técnico", "", "Se Creo Un Puesto Del Técnico:" + Me.ClaveTextBox.Text, LocClv_Ciudad)
        End If
    End Sub
    Private Sub damedatosbitacora()
        Try
            Descripcion = Me.DescripcionTextBox.Text
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONPUESTOTECTableAdapter.Connection = CON
        Me.CONPUESTOTECTableAdapter.Delete(gloClave)
        CON.Close()
        guardabitacora(1)
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub CONPUESTOTECBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONPUESTOTECBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONPUESTOTECBindingSource.EndEdit()
        Me.CONPUESTOTECTableAdapter.Connection = CON
        Me.CONPUESTOTECTableAdapter.Update(Me.NewSofTvDataSet.CONPUESTOTEC)
        CON.Close()
        MsgBox(mensaje5)
        guardabitacora(0)
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONPUESTOTECBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONPUESTOTECTableAdapter.Connection = CON
            Me.CONPUESTOTECTableAdapter.Fill(Me.NewSofTvDataSet.CONPUESTOTEC, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            CON.Close()
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmPuestoTEc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONPUESTOTECBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub ClaveTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaveTextBox.TextChanged
        gloClave = Me.ClaveTextBox.Text
    End Sub

    Private Sub DescripcionTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DescripcionTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(DescripcionTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub DescripcionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DescripcionTextBox.TextChanged

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class