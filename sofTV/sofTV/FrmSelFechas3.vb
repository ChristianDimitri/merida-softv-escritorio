Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmSelFechas3
    Private Dig As Boolean = False
    Private Net As Boolean = False
    Private fechaI As DateTime = Today
    Private fechaF As DateTime = Today

    Private Sub FrmSelFechas3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            Dig = True
        Else
            Dig = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            Net = True
        Else
            Net = False
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        fechaI = DateTimePicker1.Value.ToShortDateString
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        fechaF = DateTimePicker2.Value.ToShortDateString
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim serv As Integer = 0
        If Dig = True Then
            serv = 3

        ElseIf Net = True Then
            serv = 2
        Else
            MsgBox("Favor de seleccionar un servicio", MsgBoxStyle.Information, "Aviso")
            Return
        End If
        eServicio = Me.RadioButton1.Text
        If fechaF >= fechaI Then
            Dim reporte As ImprimirHistorialDesconexiones = New ImprimirHistorialDesconexiones
            reporte.ReporteMaterialInstalaciones(Convert.ToInt64(LocClv_session), serv, fechaI, fechaF)
            reporte.Show()
            Me.Close()
        Else
            MsgBox("El rango de fechas es incorrecto favor de verificar.", MsgBoxStyle.Critical, "Error")
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class