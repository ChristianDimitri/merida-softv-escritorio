Imports System.Data.SqlClient
Imports System.Text
Public Class FrmPlazoForzoso

    Private Sub MuestraTipServEric(ByVal Clv_TipSer As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraTipServEric ")
        strSQL.Append(CStr(Clv_TipSer) & ", ")
        strSQL.Append(CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ComboBox1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub DameClv_Session()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameClv_Session", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Session = CLng(parametro.Value.ToString())
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub PlazoCubierto()
        Me.GroupBox2.Text = "�Plazo Cubierto?"
        Me.CheckBox1.Enabled = True
        Me.CheckBox2.Enabled = True
        Me.CheckBox3.Enabled = True
        Me.CheckBox4.Enabled = True
        Me.CheckBox5.Enabled = True
    End Sub

    Private Sub AdeudoPagado()
        Me.GroupBox2.Text = "�Adeudo Pagado?"
        Me.CheckBox1.Enabled = False
        Me.CheckBox2.Enabled = False
        Me.CheckBox3.Enabled = False
        Me.CheckBox4.Enabled = False
        Me.CheckBox5.Enabled = True
        Me.CheckBox5.Checked = True
    End Sub

    Private Sub Aceptar()

        Dim eRes As Integer = 0

        If Me.RadioButton1.Checked = True Then
            eOpVentas = 74
        Else
            eOpVentas = 75
        End If

        If Me.CheckBoxSi.Checked = True And Me.CheckBoxNo.Checked = True Then
            eTipo = "BOTH"
        ElseIf Me.CheckBoxSi.Checked = True Then
            eTipo = "YES"
        ElseIf Me.CheckBoxNo.Checked = True Then
            eTipo = "NO"
        Else
            eRes = MsgBox(Me.GroupBox2.Text, MsgBoxStyle.YesNo)
            If eRes = 6 Then
                Me.CheckBoxSi.Checked = True
                eTipo = "YES"
            Else
                Me.CheckBoxNo.Checked = True
                eTipo = "NO"
            End If
        End If

        eTipSer = Me.ComboBox1.SelectedValue

        If Me.CheckBox1.Enabled = True And Me.CheckBox1.Checked = True Then eC = True
        If Me.CheckBox2.Enabled = True And Me.CheckBox2.Checked = True Then eI = True
        If Me.CheckBox3.Enabled = True And Me.CheckBox3.Checked = True Then eD = True
        If Me.CheckBox4.Enabled = True And Me.CheckBox4.Checked = True Then eS = True
        If Me.CheckBox5.Enabled = True And Me.CheckBox5.Checked = True Then eB = True

        If eC = False And eI = False And eD = False And eS = False And eB = False Then
            MsgBox("Selecciona al menos un Status.", MsgBoxStyle.Information)
            Exit Sub
        End If

        FrmSelServicioE.Show()
        Me.Close()

    End Sub

    Private Sub Cancelar()
        Me.Close()
    End Sub

    Private Sub FrmPlazoForzoso_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric(0, 0)
        DameClv_Session()
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Aceptar()
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Cancelar()
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            PlazoCubierto()
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            AdeudoPagado()
        End If
    End Sub
End Class