﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwVerEncuestas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ButtonContinuar = New System.Windows.Forms.Button()
        Me.ButtonSalir = New System.Windows.Forms.Button()
        Me.ButtonNuevo = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.IDEncuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoBusqueda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha_Ini = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaFin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Encuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdTipoEncuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEncuesta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Finalizada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioGenera = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDEncuesta, Me.TipServ, Me.TipoBusqueda, Me.Status, Me.TipoFecha, Me.Fecha_Ini, Me.FechaFin, Me.Encuesta, Me.IdTipoEncuesta, Me.FechaEncuesta, Me.Finalizada, Me.UsuarioGenera})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Location = New System.Drawing.Point(12, 23)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(842, 695)
        Me.DataGridView1.TabIndex = 1
        '
        'ButtonContinuar
        '
        Me.ButtonContinuar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonContinuar.Location = New System.Drawing.Point(862, 102)
        Me.ButtonContinuar.Name = "ButtonContinuar"
        Me.ButtonContinuar.Size = New System.Drawing.Size(145, 36)
        Me.ButtonContinuar.TabIndex = 8
        Me.ButtonContinuar.Text = "&CONTINUAR"
        Me.ButtonContinuar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(867, 682)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(136, 36)
        Me.ButtonSalir.TabIndex = 7
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'ButtonNuevo
        '
        Me.ButtonNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonNuevo.Location = New System.Drawing.Point(862, 60)
        Me.ButtonNuevo.Name = "ButtonNuevo"
        Me.ButtonNuevo.Size = New System.Drawing.Size(145, 36)
        Me.ButtonNuevo.TabIndex = 9
        Me.ButtonNuevo.Text = "&NUEVO"
        Me.ButtonNuevo.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(862, 144)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(145, 36)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "&ELIMINAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'IDEncuesta
        '
        Me.IDEncuesta.DataPropertyName = "ID_Encuesta"
        Me.IDEncuesta.HeaderText = "IDEncuesta"
        Me.IDEncuesta.Name = "IDEncuesta"
        Me.IDEncuesta.ReadOnly = True
        Me.IDEncuesta.Visible = False
        '
        'TipServ
        '
        Me.TipServ.DataPropertyName = "TipServ"
        Me.TipServ.HeaderText = "Tipo Servicio"
        Me.TipServ.Name = "TipServ"
        Me.TipServ.ReadOnly = True
        '
        'TipoBusqueda
        '
        Me.TipoBusqueda.DataPropertyName = "TipoBusqueda"
        Me.TipoBusqueda.HeaderText = "Tipo Busqueda"
        Me.TipoBusqueda.Name = "TipoBusqueda"
        Me.TipoBusqueda.ReadOnly = True
        Me.TipoBusqueda.Width = 150
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        Me.Status.Width = 50
        '
        'TipoFecha
        '
        Me.TipoFecha.DataPropertyName = "TipoFecha"
        Me.TipoFecha.HeaderText = "Tipo Fecha"
        Me.TipoFecha.Name = "TipoFecha"
        Me.TipoFecha.ReadOnly = True
        '
        'Fecha_Ini
        '
        Me.Fecha_Ini.DataPropertyName = "Fecha_Ini"
        Me.Fecha_Ini.HeaderText = "Fecha Inicio"
        Me.Fecha_Ini.Name = "Fecha_Ini"
        Me.Fecha_Ini.ReadOnly = True
        Me.Fecha_Ini.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'FechaFin
        '
        Me.FechaFin.DataPropertyName = "Fecha_Fin"
        Me.FechaFin.HeaderText = "Fecha Fin"
        Me.FechaFin.Name = "FechaFin"
        Me.FechaFin.ReadOnly = True
        '
        'Encuesta
        '
        Me.Encuesta.DataPropertyName = "Encuesta"
        Me.Encuesta.HeaderText = "Encuesta"
        Me.Encuesta.Name = "Encuesta"
        Me.Encuesta.ReadOnly = True
        '
        'IdTipoEncuesta
        '
        Me.IdTipoEncuesta.DataPropertyName = "IdTipoEncuesta"
        Me.IdTipoEncuesta.HeaderText = "IdTipoEncuesta"
        Me.IdTipoEncuesta.Name = "IdTipoEncuesta"
        Me.IdTipoEncuesta.ReadOnly = True
        Me.IdTipoEncuesta.Visible = False
        '
        'FechaEncuesta
        '
        Me.FechaEncuesta.DataPropertyName = "FechaGeneraEncuesta"
        Me.FechaEncuesta.HeaderText = "Fecha Encuesta"
        Me.FechaEncuesta.Name = "FechaEncuesta"
        Me.FechaEncuesta.ReadOnly = True
        '
        'Finalizada
        '
        Me.Finalizada.DataPropertyName = "Finalizada"
        Me.Finalizada.HeaderText = "Finalizada"
        Me.Finalizada.Name = "Finalizada"
        Me.Finalizada.ReadOnly = True
        '
        'UsuarioGenera
        '
        Me.UsuarioGenera.DataPropertyName = "UsuarioGenera"
        Me.UsuarioGenera.HeaderText = "UsuarioGenera"
        Me.UsuarioGenera.Name = "UsuarioGenera"
        Me.UsuarioGenera.ReadOnly = True
        '
        'BrwVerEncuestas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1015, 744)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ButtonNuevo)
        Me.Controls.Add(Me.ButtonContinuar)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "BrwVerEncuestas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Encuestas"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ButtonContinuar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents ButtonNuevo As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents IDEncuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoBusqueda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha_Ini As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaFin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Encuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdTipoEncuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaEncuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Finalizada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioGenera As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
