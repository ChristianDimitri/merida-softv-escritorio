Imports System.Data.SqlClient
Public Class FrmGuardaMensaje
    Dim contador As Integer
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        FrmProgramacion_msjs.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.TextBox1.Text = "" Then
            MsgBox("Se Debe Capturar un Nombre ")
            Exit Sub
        ElseIf Me.TextBox2.Text = "" Then
            MsgBox("Se Debe Capturar una Anotación Acerca de la Programación")
            Exit Sub
        Else
            Dim conlidia2 As New SqlClient.SqlConnection(MiConexion)
            conlidia2.Open()
            Dim comando As New SqlClient.SqlCommand
            ''==== CREA TRABAJOS 
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = conlidia2
                .CommandText = "checar_progs "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                ' Create a SqlParameter for each parameter in the stored procedure.
                Dim prm As New SqlParameter("@Nombre", SqlDbType.VarChar, 300)
                Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output
                prm.Value = Me.TextBox1.Text
                prm1.Value = 0
                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                Dim i As Integer = comando.ExecuteNonQuery()
                contador = prm1.Value
            End With

            If contador = 0 Then
                conlidia2.Close()
                job = Me.TextBox1.Text
                descrip_job = Me.TextBox2.Text
                name_prog = Me.TextBox1.Text
                FrmProgramacion_msjs.Genera_Job()
            ElseIf contador > 0 Then
                MsgBox("El Nombre de la Programación Ya Existe", MsgBoxStyle.Information)
            End If
           
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
        FrmProgramacion_msjs.Close()

    End Sub

End Class