Imports System.Data.SqlClient
Public Class FrmSelColonia

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GlobndClv_Colonia = True
            GloNumClv_Colonia = Me.ComboBox1.SelectedValue
            GLONOMCOLONIA = Me.ComboBox1.Text
            Me.Close()
        Else
            MsgBox("Seleccione la Colonia")
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmSelColonia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_sector", SqlDbType.Int, GloNumClv_Sector)
        ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRACOLONIAS_PorSector")
        ComboBox1.DisplayMember = "Nombre"
        ComboBox1.ValueMember = "Clv_Colonia"
    End Sub
End Class