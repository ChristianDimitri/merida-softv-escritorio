﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmFiltroReporteResumen

    Dim ClvSession As String

    Private Sub FrmFiltroReporteResumen_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, 0)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
    End Sub
    Private Sub FrmFiltroReporteResumen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        DameClvSessionRepResumen()
        MuestraTipServ()
        MuestraOrdenQueja()
        'llenalbtodos()
        eOpPPE = 6
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Private Sub llenalbtodos()
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        'lbtodos.DataSource = BaseII.ConsultaDT("MuestraTrabajosResumen")
        Dim dtllenalbtodos As New DataTable
        Dim dallenalbtodos As New SqlDataAdapter

        Dim conexion As New SqlConnection(MiConexion)
        dallenalbtodos = New SqlDataAdapter("EXEC MuestraTrabajosResumen_New " & CInt(ComboBox1.SelectedValue) & ", " & CStr(ComboBox2.SelectedValue) & ", " & CInt(ClvSession), conexion)
        dallenalbtodos.Fill(dtllenalbtodos)
        lbtodos.DataSource = dtllenalbtodos
    End Sub
    Private Sub llenalbseleccion()
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, eClv_Session)
        'lbseleccion.DataSource = BaseII.ConsultaDT("MuestraSeleccionTrabajos")

        If Label7.Text = "" Then
            Label7.Text = 1
        End If
        If Label8.Text = "" Then
            Label8.Text = "S"
        End If

        If (ComboBox1.SelectedIndex = 0 Or ComboBox1.SelectedIndex = -1) Then
            Label7.Text = 1
        Else
            Label7.Text = ComboBox1.SelectedValue
        End If
        If (ComboBox2.SelectedIndex = 0 Or ComboBox2.SelectedIndex = -1) Then
            Label8.Text = "S"
        Else
            Label8.Text = ComboBox2.SelectedValue
        End If

        Dim dtllenalbseleccion As New DataTable
        Dim dallenalbseleccion As New SqlDataAdapter

        Dim conexion As New SqlConnection(MiConexion)
        dallenalbseleccion = New SqlDataAdapter("EXEC MuestraSeleccionTrabajos_New " & CInt(Label7.Text) & ", " & Label8.Text & ", " & CInt(ClvSession), conexion)
        dallenalbseleccion.Fill(dtllenalbseleccion)
        lbseleccion.DataSource = dtllenalbseleccion

    End Sub
    Private Sub btSeleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionaUno.Click
        If lbtodos.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, lbtodos.SelectedValue)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub btDeseleccionaUno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDeseleccionaUno.Click
        If lbseleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, lbseleccion.SelectedValue)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub btSeleccionarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSeleccionarTodos.Click
        If lbtodos.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 2)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, 0)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If lbseleccion.Items.Count = 0 Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 3)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, ClvSession)
        BaseII.CreateMyParameter("@clv_trabajo", SqlDbType.Int, 0)
        BaseII.Inserta("InsertaEliminaSeleccionTrabajo")
        llenalbtodos()
        llenalbseleccion()
    End Sub

    Private Sub rbsolicitud_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbsolicitud.CheckedChanged
        If rbsolicitud.Checked Then
            eOpPPE = 6
        End If
    End Sub

    Private Sub dtini_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtini.ValueChanged
        If dtini.Value > dtfin.Value Then
            MsgBox("La fecha inicial no puede ser mayor a la fecha final.")
            dtini.Value = Today
        End If
    End Sub

    Private Sub dtfin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtfin.ValueChanged
        If dtfin.Value < dtini.Value Then
            MsgBox("La fecha final no puede ser menor a la fecha inicial.")
            dtfin.Value = Today
        End If
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        'eFechaIniPPE = dtini.Value
        'eFechaFinPPE = dtfin.Value
        eFechaIniPPE = CStr(dtini.Value)
        eFechaFinPPE = CStr(dtfin.Value)
        Label10.Text = eFechaIniPPE.ToString("yyyy/MM/dd")
        Label11.Text = eFechaFinPPE.ToString("yyyy/MM/dd")
        FrmImprimirPPE.Show()
    End Sub

    Private Sub rbejecucion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbejecucion.CheckedChanged
        If rbejecucion.Checked Then
            eOpPPE = 7
        End If
    End Sub

    Private Sub MuestraTipServ()
        Dim dtMuestraTipServ As New DataTable
        Dim daMuestraTipServ As New SqlDataAdapter

        Dim conexion As New SqlConnection(MiConexion)
        daMuestraTipServ = New SqlDataAdapter("EXEC MuestraTipSerPrincipalOQ", conexion)
        daMuestraTipServ.Fill(dtMuestraTipServ)

        ComboBox1.DataSource = dtMuestraTipServ
        ComboBox1.DisplayMember = "Concepto"
        ComboBox1.ValueMember = "Clv_TipSerPrincipal"

    End Sub

    Private Sub MuestraOrdenQueja()
        Dim dtMuestraOrdenQueja As New DataTable
        Dim daMuestraOrdenQueja As New SqlDataAdapter

        Dim conexion As New SqlConnection(MiConexion)
        daMuestraOrdenQueja = New SqlDataAdapter("EXEC MuestraTipOrdQuejaPrincipal", conexion)
        daMuestraOrdenQueja.Fill(dtMuestraOrdenQueja)

        ComboBox2.DataSource = dtMuestraOrdenQueja
        ComboBox2.DisplayMember = "Concepto"
        ComboBox2.ValueMember = "tipo"

    End Sub

    Private Sub DameClvSessionRepResumen()
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Dame_ClvSession_RepResumen", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.Parameters.Add("@ClvSession", SqlDbType.BigInt)
            comando.Parameters("@ClvSession").Direction = ParameterDirection.Output
            conexion.Open()
            comando.ExecuteNonQuery()
            ClvSession = comando.Parameters("@ClvSession").Value.ToString()
            Label9.Text = ClvSession
        Catch ex As Exception
            'Llenalog(ex.Message)
        Finally
            conexion.Close()
        End Try



    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        Dim dtllenalbtodos As New DataTable
        Dim dallenalbtodos As New SqlDataAdapter

        If Label7.Text = "" Then
            Label7.Text = 1
        End If
        If Label8.Text = "" Then
            Label8.Text = "S"
        End If

        If (ComboBox1.SelectedIndex = 0) Then
            Label7.Text = 1
        Else
            Label7.Text = ComboBox1.SelectedValue
        End If
        If (ComboBox2.SelectedIndex = 0 Or ComboBox2.SelectedIndex = -1) Then
            Label8.Text = "S"
        Else
            Label8.Text = ComboBox2.SelectedValue
        End If

        Dim conexion As New SqlConnection(MiConexion)
        dallenalbtodos = New SqlDataAdapter("EXEC MuestraTrabajosResumen_New " & CInt(Label7.Text) & ", " & Label8.Text & ", " & CInt(ClvSession), conexion)
        dallenalbtodos.Fill(dtllenalbtodos)
        lbtodos.DataSource = dtllenalbtodos
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

        Dim eliminar As String = "delete from Seleccion_trabajo2 where clv_session=" & CInt(ClvSession)
        Dim conexion2 As New SqlConnection(MiConexion)
        Dim delete As New SqlCommand(eliminar, conexion2)
        conexion2.Open()
        delete.ExecuteNonQuery()
        conexion2.Close()
        llenalbseleccion()

        Dim dtllenalbtodos As New DataTable
        Dim dallenalbtodos As New SqlDataAdapter

        If Label7.Text = "" Then
            Label7.Text = 1
        End If
        If Label8.Text = "" Then
            Label8.Text = "S"
        End If

        If (ComboBox1.SelectedIndex = 0 Or ComboBox1.SelectedIndex = -1) Then
            Label7.Text = 1
        Else
            Label7.Text = ComboBox1.SelectedValue
        End If
        If (ComboBox2.SelectedIndex = 0 Or ComboBox2.SelectedIndex = -1) Then
            Label8.Text = "S"
        Else
            Label8.Text = ComboBox2.SelectedValue
        End If

        Dim conexion As New SqlConnection(MiConexion)
        dallenalbtodos = New SqlDataAdapter("EXEC MuestraTrabajosResumen_New " & CInt(Label7.Text) & ", " & Label8.Text & ", " & CInt(ClvSession), conexion)
        dallenalbtodos.Fill(dtllenalbtodos)
        lbtodos.DataSource = dtllenalbtodos
    End Sub


End Class