﻿Public Class FrmConceptosPoliza

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub FrmConceptosPoliza_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaSucursales()
        llenaGrid()
    End Sub

    Private Sub rbServicio_CheckedChanged(sender As Object, e As EventArgs) Handles rbServicio.CheckedChanged
        Try
            If rbServicio.Checked Then
                cbSucursales.Visible = True
                lbClave.Text = "Sucursal"
                lbClave.Visible = True
                tbDescripcion.Text = ""
                'tbContrato.Visible = False
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub rbSucursal_CheckedChanged(sender As Object, e As EventArgs) Handles rbSucursal.CheckedChanged
        Try
            If rbSucursal.Checked Then
                cbSucursales.Visible = True
                'tbContrato.Visible = False
                lbClave.Text = "Sucursal"
                tbDescripcion.Text = ""
                lbClave.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub rbCliente_CheckedChanged(sender As Object, e As EventArgs) Handles rbCliente.CheckedChanged
        Try
            If rbCliente.Checked Then
                cbSucursales.Visible = True
                lbClave.Text = "Sucursal"
                'tbContrato.Visible = True
                tbDescripcion.Text = "Cliente Facturacion"
                tbContrato.Text = 0
                lbClave.Visible = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaSucursales()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, 0)
            cbSucursales.DataSource = BaseII.ConsultaDT("MUESTRASUCURSALES3")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tbContrato_TextChanged(sender As Object, e As EventArgs) Handles tbContrato.TextChanged
        'Try
        '    If tbContrato.Text.ToString.Length > 0 Then
        '        If IsNumeric(tbContrato.Text) = True Then



        '            BaseII.limpiaParametros()
        '            BaseII.CreateMyParameter("@contrato", SqlDbType.Int, 0)
        '            BaseII.CreateMyParameter("@nombre", ParameterDirection.Output, SqlDbType.VarChar, 500)
        '            BaseII.ProcedimientoOutPut("sp_dameNombreContrato")

        '            If BaseII.dicoPar("@nombre") = Nothing Then
        '                tbDescripcion.Text = ""
        '            Else
        '                tbDescripcion.Text = BaseII.dicoPar("@nombre")
        '            End If
        '        Else
        '            tbDescripcion.Text = ""
        '        End If
        '    Else
        '        tbDescripcion.Text = ""
        '    End If
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        'If rbCliente.Checked And tbContrato.Text = "" Then
        '    MsgBox("Debe ingresar todos los campos.")
        '    Exit Sub
        'End If
        If cbSucursales.SelectedValue = Nothing And (rbSucursal.Checked Or rbCliente.Checked) Then
            MsgBox("Debe ingresar todos los campos")
            Exit Sub
        End If
        If tbPosicion.Text = "" Or Not IsNumeric(tbPosicion.Text) Or tbCuenta.Text = "" Or tbDescripcion.Text = "" Then
            MsgBox("Debe ingresar todos los campos.")
            Exit Sub
        End If
        If TextIndicador.Text = "" Then
            MsgBox("Debe ingresar todos los campos.")
            Exit Sub
        End If
        If TextNormasReparto.Text = "" Then
            MsgBox("Debe ingresar todos los campos.")
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@cuenta", SqlDbType.VarChar, tbCuenta.Text)
            If rbCliente.Checked Then
                BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, "Cliente Facturacion " & cbSucursales.Text)
            Else
                BaseII.CreateMyParameter("@descripcion", SqlDbType.VarChar, tbDescripcion.Text)
            End If
            BaseII.CreateMyParameter("@posicion", SqlDbType.Int, tbPosicion.Text)
            If rbCliente.Checked Then
                BaseII.CreateMyParameter("@clave", SqlDbType.Int, cbSucursales.SelectedValue)
                BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "cliente")
            End If
            If rbServicio.Checked Then
                BaseII.CreateMyParameter("@clave", SqlDbType.Int, cbSucursales.SelectedValue)
                BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "servicio")
            End If
            If rbSucursal.Checked Then
                BaseII.CreateMyParameter("@clave", SqlDbType.Int, cbSucursales.SelectedValue)
                BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, "sucursal")
            End If
            BaseII.CreateMyParameter("@Indicador", SqlDbType.VarChar, TextIndicador.Text, 50)
            BaseII.CreateMyParameter("@Normas_Reparto", SqlDbType.VarChar, TextNormasReparto.Text, 150)
            BaseII.CreateMyParameter("@error", ParameterDirection.Output, SqlDbType.Int)

            BaseII.ProcedimientoOutPut("sp_guardaConceptoCuenta")
            If BaseII.dicoPar("@error") = 1 Then
                MsgBox("Ya existe una cuenta para el cliente/sucursal.")
            ElseIf BaseII.dicoPar("@error") = 3 Then
                MsgBox("Ya existe una concepto de servicios con la misma cuenta.")
            ElseIf BaseII.dicoPar("@error") = 2 Then
                MsgBox("El cliente al que se quiere asignar una cuenta no tiene datos fiscales registrados.")
            Else
                llenaGrid()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub llenaGrid()
        Try
            BaseII.limpiaParametros()
            DataGridView1.DataSource = BaseII.ConsultaDT("sp_llenaConceptos")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Me.Close()
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If DataGridView1.Rows.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@clv_concepto", SqlDbType.Int, DataGridView1.SelectedRows(0).Cells("Clv_Concepto").Value.ToString()) 'DataGridView1.SelectedCells(0).Value)
            BaseII.Inserta("sp_eliminaConcepto")
            llenaGrid()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tbBuscar_TextChanged(sender As Object, e As EventArgs) Handles tbBuscar.TextChanged
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@buscar", SqlDbType.VarChar, tbBuscar.Text)
            DataGridView1.DataSource = BaseII.ConsultaDT("sp_llenaConceptos")


        Catch ex As Exception

        End Try

    End Sub
End Class