﻿Imports System.Data.SqlClient

Public Class BrwHub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConSector1TableAdapter.Connection = CON
        'Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, 0, Me.TextBox1.Text, "", 1)
        ConSector(0, Me.TextBox1.Text, "", 1)
        CON.Close()

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            'Me.ConSector1TableAdapter.Connection = CON
            'Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, 0, Me.TextBox1.Text, "", 1)
            ConSector(0, Me.TextBox1.Text, "", 1)
        End If
        CON.Close()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConSector1TableAdapter.Connection = CON
        'Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, 0, "", Me.TextBox2.Text, 2)
        ConSector(0, "", Me.TextBox2.Text, 2)
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            'Me.ConSector1TableAdapter.Connection = CON
            'Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, 0, "", Me.TextBox2.Text, 2)
            ConSector(0, "", Me.TextBox2.Text, 2)
        End If
        CON.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmHub.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConSectorDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClv_Sector = ConSectorDataGridView.SelectedCells(0).Value.ToString()
            FrmHub.Show()
        Else
            MsgBox("Selecciona un Registro a Consultar.", , "Atención")
        End If


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConSectorDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_Sector = ConSectorDataGridView.SelectedCells(0).Value.ToString()
            FrmHub.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub BrwSectore_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConSector1TableAdapter.Connection = CON
        'Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, 0, "", "", 0)
        ConSector(0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub BrwSectore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Me.ConSector1TableAdapter.Connection = CON
        'Me.ConSector1TableAdapter.Fill(Me.DataSetEric.ConSector1, 0, "", "", 0)

        ConSector(0, "", "", 0)

        CON.Close()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub ConSector(ByVal Clv_Sector As Integer, ByVal Clv_Txt As String, ByVal Descripcion As String, ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim dtConHub As New DataTable
        Dim daConHub As New SqlDataAdapter

        Dim comando2 As New SqlCommand("ConHub", conexion)
        comando2.CommandType = CommandType.StoredProcedure

        comando2.Parameters.AddWithValue("@Clv_Sector", Clv_Sector)
        comando2.Parameters("@Clv_Sector").Direction = ParameterDirection.Input
        comando2.Parameters.AddWithValue("@Clv_Txt", Clv_Txt)
        comando2.Parameters("@Clv_Txt").Direction = ParameterDirection.Input
        comando2.Parameters.AddWithValue("@Descripcion", Descripcion)
        comando2.Parameters("@Descripcion").Direction = ParameterDirection.Input
        comando2.Parameters.AddWithValue("@Op", Op)
        comando2.Parameters("@Op").Direction = ParameterDirection.Input

        daConHub = New SqlDataAdapter(comando2)

        conexion.Open()

        daConHub.Fill(dtConHub)
        ConSectorDataGridView.DataSource = dtConHub
        ConSectorDataGridView.AutoGenerateColumns = False
        ConSectorDataGridView.Columns("Clv_Sector").Visible = False
        ConSectorDataGridView.Columns("Clv_Txt").Visible = True
        ConSectorDataGridView.Columns("Descripcion").Visible = True
        ConSectorDataGridView.Columns(2).Width = 400
        ConSectorDataGridView.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        With ConSectorDataGridView
            .Columns("Clv_Sector").DataPropertyName = "Clv_Sector"
            .Columns("Clv_Txt").DataPropertyName = "Clv_Txt"
            .Columns("Descripcion").DataPropertyName = "Descripcion"
        End With
        conexion.Close()

        TextBox1.Clear()
        TextBox2.Clear()

    End Sub


    Private Sub ConSectorDataGridView_CellMouseClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles ConSectorDataGridView.CellMouseClick
        If ConSectorDataGridView.Rows.Count > 0 Then
            Clv_TxtLabel1.Text = ConSectorDataGridView.SelectedCells(1).Value.ToString()
            DescripcionLabel1.Text = ConSectorDataGridView.SelectedCells(2).Value.ToString()
        End If
    End Sub

End Class