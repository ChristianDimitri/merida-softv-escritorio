﻿Imports System.IO
Imports Microsoft.Office.Interop.Excel
Imports System
Imports System.Collections.Generic
Imports System.Array
Imports System.Data
Imports System.Data.SqlClient

Public Class FrmDinercap

    Dim NameFile As String
    Dim RutaArchivo As String
    Dim archivoExcel As String
    Dim ContratoTel As String
    Dim Cliente As String
    Dim Credito As String
    Dim Importe As String
    Dim ConceptoPa As String
    Dim DescPago As String
    Dim NumMensuaRes As String
    Dim AdeutoTotal As String
    Dim StatusCredito As String
    Dim factura As String
    Dim IdTblAdeudoMaster As Integer
    Dim ValidaName As Integer

    Public Sub ValidaNombreArchivo(ByVal Name As String)
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Sp_ValidaArchivoNombre", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim par1 As New SqlParameter("@Nombre", SqlDbType.VarChar, 250)
            par1.Direction = ParameterDirection.Input
            par1.Value = Name
            comando.Parameters.Add(par1)

            Dim par5 As New SqlParameter("@Count", SqlDbType.Int)
            par5.Direction = ParameterDirection.Output
            par5.Value = ""
            comando.Parameters.Add(par5)

            conexion.Open()
            comando.ExecuteNonQuery()
            ValidaName = par5.Value

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Add_TblAdeudoMaster(ByVal Name As String, ByVal User As String, ByVal Status As String)
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Sp_AddTblAdeudoMaster", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim par1 As New SqlParameter("@NombreArchivo", SqlDbType.VarChar, 250)
            par1.Direction = ParameterDirection.Input
            par1.Value = Name
            comando.Parameters.Add(par1)

            Dim par2 As New SqlParameter("@Usuario", SqlDbType.VarChar, 10)
            par2.Direction = ParameterDirection.Input
            par2.Value = User
            comando.Parameters.Add(par2)

            Dim par4 As New SqlParameter("@Status", SqlDbType.VarChar, 25)
            par4.Direction = ParameterDirection.Input
            par4.Value = Status
            comando.Parameters.Add(par4)

            Dim par5 As New SqlParameter("@Id", SqlDbType.BigInt)
            par5.Direction = ParameterDirection.Output
            par5.Value = ""
            comando.Parameters.Add(par5)

            conexion.Open()
            comando.ExecuteNonQuery()
            IdTblAdeudoMaster = par5.Value

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Public Sub Add_TblAdeudoDetalle(ByVal IdRel As Integer, ByVal ContratoTel As Integer, ByVal Cliente As Integer, ByVal Credito As Integer, ByVal Importe As Decimal, ByVal ConceptoPago As String, ByVal DescripcionPago As String, ByVal NumMenRestantes As Integer, ByVal AdeudoTotal As Decimal, ByVal StatusCredito As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Try

            Dim comando As New SqlCommand("Sp_AddTblAdeudoDetalle", conexion)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim par1 As New SqlParameter("@IdRel", SqlDbType.Int)
            par1.Direction = ParameterDirection.Input
            par1.Value = IdRel
            comando.Parameters.Add(par1)

            Dim par2 As New SqlParameter("@ContratoTel", SqlDbType.Int)
            par2.Direction = ParameterDirection.Input
            par2.Value = ContratoTel
            comando.Parameters.Add(par2)

            Dim par3 As New SqlParameter("@Cliente", SqlDbType.Int)
            par3.Direction = ParameterDirection.Input
            par3.Value = Cliente
            comando.Parameters.Add(par3)

            Dim par4 As New SqlParameter("@Credito", SqlDbType.Int)
            par4.Direction = ParameterDirection.Input
            par4.Value = Credito
            comando.Parameters.Add(par4)

            Dim par5 As New SqlParameter("@Importe", SqlDbType.Decimal)
            par5.Direction = ParameterDirection.Input
            par5.Value = Importe
            comando.Parameters.Add(par5)

            Dim par6 As New SqlParameter("@ConceptoPago", SqlDbType.VarChar, 250)
            par6.Direction = ParameterDirection.Input
            par6.Value = ConceptoPago
            comando.Parameters.Add(par6)

            Dim par7 As New SqlParameter("@DescripcionPago", SqlDbType.VarChar, 250)
            par7.Direction = ParameterDirection.Input
            par7.Value = DescripcionPago
            comando.Parameters.Add(par7)

            Dim par8 As New SqlParameter("@NumMenRestantes", SqlDbType.Int)
            par8.Direction = ParameterDirection.Input
            par8.Value = NumMenRestantes
            comando.Parameters.Add(par8)

            Dim par9 As New SqlParameter("@AdeudoTotal", SqlDbType.Decimal)
            par9.Direction = ParameterDirection.Input
            par9.Value = AdeudoTotal
            comando.Parameters.Add(par9)

            Dim par10 As New SqlParameter("@StatusCredito", SqlDbType.Int)
            par10.Direction = ParameterDirection.Input
            par10.Value = StatusCredito
            comando.Parameters.Add(par10)

            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            'MsgBox(ex.Message)
        Finally
            conexion.Close()
        End Try
    End Sub

    Private Sub BtnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles BtnGuardar.Click
        If TxtNombre.Text = "" Then
            MsgBox("Debe seleccionar un archivo.")
            Exit Sub
        End If

        Try
            Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream()
            BaseII.limpiaParametros()

            Dim oExcel As Application = CreateObject("Excel.Application")
            Dim oBook As Workbook = oExcel.Workbooks.Open(RutaArchivo, , False)
            Dim oSheet As Worksheet
            oSheet = oBook.Worksheets("Hoja1")

            Dim UltLinea As Integer
            UltLinea = oSheet.Range("A1").CurrentRegion.Rows.Count

            Dim B1 As Integer
            Dim B2 As Integer
            Dim B3 As Integer
            Dim B4 As Integer
            Dim B5 As Integer
            Dim B6 As Integer
            Dim B7 As Integer
            Dim B8 As Integer
            Dim B9 As Integer

            ContratoTel = oSheet.Range("A" & 2).Text
            Cliente = oSheet.Range("B" & 2).Text
            Credito = oSheet.Range("C" & 2).Text
            Importe = oSheet.Range("D" & 2).Text
            ConceptoPa = oSheet.Range("E" & 2).Text
            DescPago = oSheet.Range("F" & 2).Text
            NumMensuaRes = oSheet.Range("G" & 2).Text
            AdeutoTotal = oSheet.Range("H" & 2).Text
            StatusCredito = oSheet.Range("I" & 2).Text

            If ContratoTel = "CONTRATO TELECABLE" Then
                B1 = 1
            End If
            If Cliente.ToUpper = "CLIENTE" Then
                B2 = 1
            End If
            If Credito.ToUpper = "CREDITO" Then
                B3 = 1
            End If
            If Importe.ToUpper = "IMPORTE" Then
                B4 = 1
            End If
            If ConceptoPa.ToUpper = "CONCEPTO DE PAGO" Then
                B5 = 1
            End If
            If DescPago.ToUpper = "DESCRIPCION DE PAGO" Then
                B6 = 1
            End If
            If NumMensuaRes.ToUpper = "NUMERO DE MENSUALIDADES QUE RESTAN" Then
                B7 = 1
            End If
            If AdeutoTotal.ToUpper = "ADEUDO TOTAL DEL CLIENTE" Then
                B8 = 1
            End If
            If StatusCredito.ToUpper = "STATUS CREDITO" Then
                B9 = 1
            End If

            If B1 = 1 And B2 = 1 And B3 = 1 And B4 = 1 And B5 = 1 And B6 = 1 And B7 = 1 And B8 = 1 And B9 = 1 Then

                Add_TblAdeudoMaster(NameFile, LblUserSys.Text, "Recibido")

                Dim fila As Integer = 3
                While fila <= UltLinea

                    ContratoTel = ""
                    Cliente = ""
                    Credito = ""
                    Importe = ""
                    ConceptoPa = ""
                    DescPago = ""
                    NumMensuaRes = ""
                    AdeutoTotal = ""
                    StatusCredito = ""
                    factura = ""

                    If (oSheet.Range("A" & fila).Text) <> "" Then
                        ContratoTel = oSheet.Range("A" & fila).Text
                        Cliente = oSheet.Range("B" & fila).Text
                        Credito = oSheet.Range("C" & fila).Text
                        Importe = oSheet.Range("D" & fila).Text
                        ConceptoPa = oSheet.Range("E" & fila).Text
                        DescPago = oSheet.Range("F" & fila).Text
                        NumMensuaRes = oSheet.Range("G" & fila).Text
                        AdeutoTotal = oSheet.Range("H" & fila).Text
                        StatusCredito = oSheet.Range("I" & fila).Text

                        Add_TblAdeudoDetalle(IdTblAdeudoMaster, ContratoTel, Cliente, Credito, Importe, ConceptoPa, DescPago, NumMensuaRes, AdeutoTotal, StatusCredito)
                        fila += 1
                    Else
                        fila = UltLinea + 1
                    End If
                End While
                MessageBox.Show("Datos leídos correctamente.")
                'Cerramos el libro
                oBook.Close()
                Me.Close()
                BrwDinercap.Buscar(0)
            Else
                MessageBox.Show("El archivo no corresponde con el layout a cargar.")
                TxtNombre.Clear()
            End If


        Catch ex As Exception

        End Try

    End Sub

    Private Sub BtnCargar_Click(sender As System.Object, e As System.EventArgs) Handles BtnCargar.Click
        Dim ofd As New OpenFileDialog()
        ofd.Title = "Selecciona un archivo."
        'ofd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        ofd.Filter = "All files (*xlsx*)|*xlsx*|All files (*xlsx*)|*xlsx*"
        ofd.FilterIndex = 1
        ofd.RestoreDirectory = True
        If ofd.ShowDialog() = DialogResult.OK Then
            TxtNombre.Text = ofd.FileName()
            RutaArchivo = TxtNombre.Text
            NameFile = Path.GetFileName(RutaArchivo)

            ValidaNombreArchivo(NameFile)
            If ValidaName >= 1 Then
                MsgBox("Ya existe un archivo con ese nombre.")
                TxtNombre.Clear()
                Exit Sub
            Else
                MessageBox.Show("Archivo " & NameFile & " cargado correctamente")
            End If
        End If
    End Sub

    Private Sub FrmDinercap_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.LblUserSys.Text = BrwDinercap.LblUserSis.Text
    End Sub

    '14/05/2018

End Class