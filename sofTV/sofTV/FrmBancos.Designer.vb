﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBancos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim ClaveRelLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBancos))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.NombreTextBox = New System.Windows.Forms.TextBox
        Me.CONBANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.CONBANCOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.CONBANCOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.ClaveRelTextBox = New System.Windows.Forms.TextBox
        Me.Clv_TxtTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.CONBANCOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONBANCOSTableAdapter
        ClaveLabel = New System.Windows.Forms.Label
        NombreLabel = New System.Windows.Forms.Label
        ClaveRelLabel = New System.Windows.Forms.Label
        Clv_TxtLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONBANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONBANCOSBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(137, 59)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(50, 15)
        ClaveLabel.TabIndex = 0
        ClaveLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(53, 118)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(134, 15)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Nombre del Banco :"
        '
        'ClaveRelLabel
        '
        ClaveRelLabel.AutoSize = True
        ClaveRelLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveRelLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveRelLabel.Location = New System.Drawing.Point(221, 118)
        ClaveRelLabel.Name = "ClaveRelLabel"
        ClaveRelLabel.Size = New System.Drawing.Size(72, 15)
        ClaveRelLabel.TabIndex = 4
        ClaveRelLabel.Text = "Clave Rel:"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_TxtLabel.Location = New System.Drawing.Point(49, 91)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(138, 15)
        Clv_TxtLabel.TabIndex = 6
        Clv_TxtLabel.Text = "Clave Alfanumerica :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(Me.CONBANCOSBindingNavigator)
        Me.Panel1.Controls.Add(ClaveLabel)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(ClaveRelLabel)
        Me.Panel1.Controls.Add(Me.ClaveRelTextBox)
        Me.Panel1.Controls.Add(Clv_TxtLabel)
        Me.Panel1.Controls.Add(Me.Clv_TxtTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(566, 287)
        Me.Panel1.TabIndex = 0
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONBANCOSBindingSource, "nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(193, 116)
        Me.NombreTextBox.MaxLength = 80
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(290, 21)
        Me.NombreTextBox.TabIndex = 1
        '
        'CONBANCOSBindingSource
        '
        Me.CONBANCOSBindingSource.DataMember = "CONBANCOS"
        Me.CONBANCOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CONBANCOSBindingNavigator
        '
        Me.CONBANCOSBindingNavigator.AddNewItem = Nothing
        Me.CONBANCOSBindingNavigator.BindingSource = Me.CONBANCOSBindingSource
        Me.CONBANCOSBindingNavigator.CountItem = Nothing
        Me.CONBANCOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONBANCOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONBANCOSBindingNavigatorSaveItem})
        Me.CONBANCOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONBANCOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONBANCOSBindingNavigator.MoveLastItem = Nothing
        Me.CONBANCOSBindingNavigator.MoveNextItem = Nothing
        Me.CONBANCOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONBANCOSBindingNavigator.Name = "CONBANCOSBindingNavigator"
        Me.CONBANCOSBindingNavigator.PositionItem = Nothing
        Me.CONBANCOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONBANCOSBindingNavigator.Size = New System.Drawing.Size(566, 25)
        Me.CONBANCOSBindingNavigator.TabIndex = 2
        Me.CONBANCOSBindingNavigator.TabStop = True
        Me.CONBANCOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONBANCOSBindingNavigatorSaveItem
        '
        Me.CONBANCOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONBANCOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONBANCOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONBANCOSBindingNavigatorSaveItem.Name = "CONBANCOSBindingNavigatorSaveItem"
        Me.CONBANCOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONBANCOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONBANCOSBindingSource, "clave", True))
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(193, 59)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ClaveTextBox.TabIndex = 100
        Me.ClaveTextBox.TabStop = False
        '
        'ClaveRelTextBox
        '
        Me.ClaveRelTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveRelTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.ClaveRelTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONBANCOSBindingSource, "ClaveRel", True))
        Me.ClaveRelTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveRelTextBox.Location = New System.Drawing.Point(242, 116)
        Me.ClaveRelTextBox.Name = "ClaveRelTextBox"
        Me.ClaveRelTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ClaveRelTextBox.TabIndex = 3
        Me.ClaveRelTextBox.TabStop = False
        '
        'Clv_TxtTextBox
        '
        Me.Clv_TxtTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_TxtTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_TxtTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONBANCOSBindingSource, "Clv_Txt", True))
        Me.Clv_TxtTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_TxtTextBox.Location = New System.Drawing.Point(193, 89)
        Me.Clv_TxtTextBox.MaxLength = 5
        Me.Clv_TxtTextBox.Name = "Clv_TxtTextBox"
        Me.Clv_TxtTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Clv_TxtTextBox.TabIndex = 0
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(442, 305)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONBANCOSTableAdapter
        '
        Me.CONBANCOSTableAdapter.ClearBeforeFill = True
        '
        'FrmBancos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 350)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmBancos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Bancos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONBANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONBANCOSBindingNavigator.ResumeLayout(False)
        Me.CONBANCOSBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONBANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONBANCOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONBANCOSTableAdapter
    Friend WithEvents CONBANCOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONBANCOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ClaveRelTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_TxtTextBox As System.Windows.Forms.TextBox
End Class
