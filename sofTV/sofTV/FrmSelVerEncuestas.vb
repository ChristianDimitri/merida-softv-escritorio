﻿Public Class FrmSelVerEncuestas

    Private Sub FrmSelVerEncuestas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If CheckBox9.Checked = False And CheckBox8.Checked = False Then
            MsgBox("Seleccione una Opción", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CheckBox8.Checked = True Then
            BrwVerEncuestas.Show()
        ElseIf CheckBox9.Checked = True Then
            FrmSelServStatusEncuestas.Show()
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If CheckBox8.Checked = True Then
            CheckBox9.Checked = False
        End If
    End Sub

    Private Sub CheckBox9_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox9.CheckedChanged
        If CheckBox9.Checked = True Then
            CheckBox8.Checked = False
        End If
    End Sub
End Class