﻿Public Class BrwClasificacionProblemas

#Region "Eventos Controles"
    Private Sub BrwClasificacionProblemas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Consultar()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub btnBuscarClave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarClave.Click
        If IsNumeric(Me.txtBuscarClave.Text) = False Then
            MsgBox("Solo se aceptan valores Numéricos", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.txtBuscarClave.Text.Length = 0 Then
            Consultar()
        Else
            Consultar(CLng(Me.txtBuscarClave.Text))
        End If
    End Sub

    Private Sub btnBuscarDescripcion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarDescripcion.Click
        If Me.txtBuscarDescripcion.Text.Length = 0 Then
            Consultar()
        Else
            Consultar(Me.txtBuscarDescripcion.Text)
        End If
    End Sub

    Private Sub dgvProblemas_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvProblemas.CurrentCellChanged
        Try
            If dgvProblemas.RowCount > 0 Then 'AQUÍ LLENA LOS CONTROLES DEL GROUP BOX DE LA PARTE INFERIOR IZQUIERDA CADA VEZ QUE SE SELECCIONA ALGÚN REGISTRO EN EL GRID VIEW
                Me.txtClaveEspecif.Text = dgvProblemas.SelectedCells(0).Value
                Me.txtDescripcionEspecif.Text = dgvProblemas.SelectedCells(1).Value
                Me.cbxActivo.Checked = CBool(dgvProblemas.SelectedCells(2).Value)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Dim FRM As New FrmClasificacionProblemas
        FRM.ProblemaClvProblema = 0
        FRM.ProblemaDescripcion = ""
        FRM.ProblemaActivo = False
        FRM.ProblemaOpcion = "N"
        FRM.ShowDialog()
        Consultar()
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        If dgvProblemas.RowCount = 0 Then
            MsgBox("No hay Registros en la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CLng(dgvProblemas.SelectedCells(0).Value) > 0 Then
            Dim FRM As New FrmClasificacionProblemas
            FRM.ProblemaClvProblema = CLng(dgvProblemas.SelectedCells(0).Value)
            FRM.ProblemaDescripcion = dgvProblemas.SelectedCells(1).Value.ToString
            FRM.ProblemaActivo = CBool(dgvProblemas.SelectedCells(2).Value)
            FRM.ProblemaOpcion = "C"
            FRM.ShowDialog()
            Consultar()
        Else
            MsgBox("Seleccione al menos un Registro de la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If dgvProblemas.RowCount = 0 Then
            MsgBox("No hay Registros en la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CLng(dgvProblemas.SelectedCells(0).Value) > 0 Then
            Dim FRM As New FrmClasificacionProblemas
            FRM.ProblemaClvProblema = CLng(dgvProblemas.SelectedCells(0).Value)
            FRM.ProblemaDescripcion = dgvProblemas.SelectedCells(1).Value.ToString
            FRM.ProblemaActivo = CBool(dgvProblemas.SelectedCells(2).Value)
            FRM.ProblemaOpcion = "M"
            FRM.ShowDialog()
            Consultar()
        Else
            MsgBox("Seleccione al menos un Registro de la Lista", MsgBoxStyle.Information)
            Exit Sub
        End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
#End Region

#Region "Consultar"
    Private Sub Consultar() 'MÉTODO QUE LLENA EL GRID VIEW
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = 0
        Problema.Descripcion = String.Empty
        Problema.OpBusqueda = 1
        Me.dgvProblemas.DataSource = Problema.uspConsultaTblClasificacionProblemas()
    End Sub

    Private Sub Consultar(ByVal prmClvProblema As Long) 'METODO QUE BUSCA POR CLV_PROBLEMA
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = prmClvProblema
        Problema.Descripcion = String.Empty
        Problema.OpBusqueda = 2
        Me.dgvProblemas.DataSource = Problema.uspConsultaTblClasificacionProblemas()
    End Sub

    Private Sub Consultar(ByVal prmDescripcion As String) 'METODO QUE BUSCA POR DESCRIPCION
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = 0
        Problema.Descripcion = prmDescripcion
        Problema.OpBusqueda = 3
        Me.dgvProblemas.DataSource = Problema.uspConsultaTblClasificacionProblemas()
    End Sub
#End Region

    
End Class