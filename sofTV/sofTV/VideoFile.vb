'
'    VideoFile class for C#
'		Version: 1.0		Date: 2002/04/09
'
'
'    Copyright � 2002, The KPD-Team
'    All rights reserved.
'    http://www.mentalis.org/
'
'  Redistribution and use in source and binary forms, with or without
'  modification, are permitted provided that the following conditions
'  are met:
'
'    - Redistributions of source code must retain the above copyright
'       notice, this list of conditions and the following disclaimer. 
'
'    - Neither the name of the KPD-Team, nor the names of its contributors
'       may be used to endorse or promote products derived from this
'       software without specific prior written permission. 
'
'  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
'  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
'  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
'  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
'  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
'  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
'  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
'  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
'  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
'  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
'  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
'  OF THE POSSIBILITY OF SUCH DAMAGE.
'

Imports System
Imports System.IO
Imports System.Text
Imports System.Drawing
Imports System.Windows.Forms

Namespace Org.Mentalis.Multimedia
    '/// <summary>This class represents any video file type supported by MCI.</summary>
    '/// <remarks>This class should be able to handle at least AVI on any system. Playing MPEG, DivX and other formats is also available on most systems.</remarks>
    Public Class VideoFile
        Inherits MediaFile
        '/// <summary>
        '/// Initializes a new VideoFile object.
        '/// </summary>
        '/// <param name="file">The video file to open.</param>
        '/// <exception cref="ArgumentNullException">The file parameter is null (Nothing in VB.NET).</exception>
        '/// <exception cref="FileNotFoundException">The specified file could not be found.</exception>
        '/// <exception cref="MediaException">An error occured while opening the specified file.</exception>
        Public Sub New(ByVal file As String)
            MyBase.new(file, Nothing)
        End Sub
        '/// <summary>
        '/// Initializes a new VideoFile object.
        '/// </summary>
        '/// <param name="file">The video file to open.</param>
        '/// <param name="owner">The window to play the movie in. This parameter can be null (Nothing in VB.NET).</param>
        '/// <exception cref="ArgumentNullException">The file parameter is null (Nothing in VB.NET).</exception>
        '/// <exception cref="FileNotFoundException">The specified file could not be found.</exception>
        '/// <exception cref="MediaException">An error occured while opening the specified file.</exception>
        '/// <remarks>If the owner is set to null (Nothing in VB.NET), MCI will create a new window and play the movie on top of that one.</remarks>
        Public Sub New(ByVal file As String, ByVal owner As IWin32Window)
            MyBase.new(file, owner)
            TimeFormat = "ms"  ' use milliseconds
        End Sub
        '/// <summary>
        '/// Specifies the MCI string that should be used when opening the video file.
        '/// </summary>
        '/// <returns>An MCI string that should be used when opening the video file.</returns>
        Protected Overrides Function GetOpenString() As String
            If Owner Is Nothing Then
                Return "OPEN " + File + " TYPE MPEGVideo ALIAS " + AliasName
            Else
                Return "OPEN " + File + " TYPE MPEGVideo ALIAS " + AliasName + " PARENT " + Owner.Handle.ToInt64().ToString() + " STYLE child"
            End If
        End Function
        '/// <summary>
        '/// Gets or sets the width of the client area on the destination window.
        '/// </summary>
        '/// <value>An integer that specifies the width of the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property Width() As Integer
            Get
                Return OutputRect.Width
            End Get
            Set(ByVal Value As Integer)
                Dim output As Rectangle = OutputRect
                OutputRect = New Rectangle(output.X, output.Y, Value, output.Height)
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the height of the client area on the destination window.
        '/// </summary>
        '/// <value>An integer that specifies the height of the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property Height() As Integer
            Get
                Return OutputRect.Height
            End Get
            Set(ByVal Value As Integer)
                Dim output As Rectangle = OutputRect
                OutputRect = New Rectangle(output.X, output.Y, output.Width, Value)
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the left position of the client area on the destination window.
        '/// </summary>
        '/// <value>An integer that specifies the left position on the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property Left() As Integer
            Get
                Return OutputRect.X
            End Get
            Set(ByVal Value As Integer)
                Dim output As Rectangle = OutputRect
                OutputRect = New Rectangle(Value, output.Y, output.Width, output.Height)
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the top position of the client area on the destination window.
        '/// </summary>
        '/// <value>An integer that specifies the top position of the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property Top() As Integer
            Get
                Return OutputRect.Y
            End Get
            Set(ByVal Value As Integer)
                Dim output As Rectangle = OutputRect
                OutputRect = New Rectangle(output.X, Value, output.Width, output.Height)
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the width and the height of the client area on the destination window.
        '/// </summary>
        '/// <value>A Size instance that holds the width and height of the client area on the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property Size() As Size
            Get
                Dim output As Rectangle = OutputRect
                Return New Size(OutputRect.Width, OutputRect.Height)
            End Get
            Set(ByVal Value As Size)
                Dim output As Rectangle = OutputRect
                OutputRect = New Rectangle(output.X, output.Y, Value.Width, Value.Height)
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the left and top position of the client area on the destination window.
        '/// </summary>
        '/// <value>A Point instance that holds the left and top of the client area on the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property Location() As Point
            Get
                Dim output As Rectangle = OutputRect
                Return New Point(OutputRect.Left, OutputRect.Top)
            End Get
            Set(ByVal Value As Point)
                Dim output As Rectangle = OutputRect
                OutputRect = New Rectangle(Value.X, Value.Y, output.Width, output.Height)
            End Set
        End Property
        '/// <summary>
        '/// Gets or sets the left, top, width and height values of the client area on the destination window.
        '/// </summary>
        '/// <value>A Rectangle instance that holds the left, top, width and height values of the client area on the destination window.</value>
        '/// <exception cref="MediaException">The specified value is invalid -or- there was an error querying the media file.</exception>
        Public Property OutputRect() As Rectangle
            Get
                Dim buffer As StringBuilder = New StringBuilder(255)
                Dim ret As Integer = mciSendString("WHERE " + AliasName + " DESTINATION", buffer, buffer.Capacity, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
                Try
                    Dim parts() As String = buffer.ToString().Split(" "c)
                    Return New Rectangle(Integer.Parse(parts(0)), Integer.Parse(parts(1)), Integer.Parse(parts(2)) - Integer.Parse(parts(0)), Integer.Parse(parts(3)) - Integer.Parse(parts(1)))
                Catch
                    Throw New MediaException("Unrecognized MCI reply.")
                End Try
            End Get
            Set(ByVal Value As Rectangle)
                Dim ret As Integer = mciSendString("PUT " + AliasName + " WINDOW AT " + Value.X.ToString() + " " + Value.Y.ToString() + " " + (Value.Width).ToString() + " " + (Value.Height).ToString(), Nothing, 0, IntPtr.Zero)
                If ret <> 0 Then Throw New MediaException(GetMciError(ret))
            End Set
        End Property
    End Class
End Namespace
