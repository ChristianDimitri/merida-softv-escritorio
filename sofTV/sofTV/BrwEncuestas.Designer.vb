<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwEncuestas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.ButtonNuevo = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBoxNombre = New System.Windows.Forms.TextBox
        Me.DataGridViewEncuestas = New System.Windows.Forms.DataGridView
        Me.ButtonConsultar = New System.Windows.Forms.Button
        Me.ButtonModificar = New System.Windows.Forms.Button
        Me.ButtonSalir = New System.Windows.Forms.Button
        Me.TextBoxDescripcion = New System.Windows.Forms.TextBox
        Me.ButtonBusNombre = New System.Windows.Forms.Button
        Me.ButtonBusDescripcion = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.CIDEncuesta = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CNombre = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CActiva = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.CDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CFecha = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridViewEncuestas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ButtonNuevo
        '
        Me.ButtonNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonNuevo.Location = New System.Drawing.Point(868, 19)
        Me.ButtonNuevo.Name = "ButtonNuevo"
        Me.ButtonNuevo.Size = New System.Drawing.Size(136, 36)
        Me.ButtonNuevo.TabIndex = 0
        Me.ButtonNuevo.Text = "&NUEVO"
        Me.ButtonNuevo.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(210, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Buscar Encuesta por:"
        '
        'TextBoxNombre
        '
        Me.TextBoxNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNombre.Location = New System.Drawing.Point(16, 124)
        Me.TextBoxNombre.Name = "TextBoxNombre"
        Me.TextBoxNombre.Size = New System.Drawing.Size(247, 21)
        Me.TextBoxNombre.TabIndex = 2
        '
        'DataGridViewEncuestas
        '
        Me.DataGridViewEncuestas.AllowUserToAddRows = False
        Me.DataGridViewEncuestas.AllowUserToDeleteRows = False
        Me.DataGridViewEncuestas.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewEncuestas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewEncuestas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewEncuestas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CIDEncuesta, Me.CNombre, Me.CActiva, Me.CDescripcion, Me.CFecha})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewEncuestas.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewEncuestas.Location = New System.Drawing.Point(286, 19)
        Me.DataGridViewEncuestas.Name = "DataGridViewEncuestas"
        Me.DataGridViewEncuestas.ReadOnly = True
        Me.DataGridViewEncuestas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewEncuestas.Size = New System.Drawing.Size(545, 689)
        Me.DataGridViewEncuestas.TabIndex = 3
        '
        'ButtonConsultar
        '
        Me.ButtonConsultar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonConsultar.Location = New System.Drawing.Point(868, 61)
        Me.ButtonConsultar.Name = "ButtonConsultar"
        Me.ButtonConsultar.Size = New System.Drawing.Size(136, 36)
        Me.ButtonConsultar.TabIndex = 4
        Me.ButtonConsultar.Text = "&CONSULTAR"
        Me.ButtonConsultar.UseVisualStyleBackColor = True
        '
        'ButtonModificar
        '
        Me.ButtonModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonModificar.Location = New System.Drawing.Point(868, 103)
        Me.ButtonModificar.Name = "ButtonModificar"
        Me.ButtonModificar.Size = New System.Drawing.Size(136, 36)
        Me.ButtonModificar.TabIndex = 5
        Me.ButtonModificar.Text = "&MODIFICAR"
        Me.ButtonModificar.UseVisualStyleBackColor = True
        '
        'ButtonSalir
        '
        Me.ButtonSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSalir.Location = New System.Drawing.Point(868, 672)
        Me.ButtonSalir.Name = "ButtonSalir"
        Me.ButtonSalir.Size = New System.Drawing.Size(136, 36)
        Me.ButtonSalir.TabIndex = 6
        Me.ButtonSalir.Text = "&SALIR"
        Me.ButtonSalir.UseVisualStyleBackColor = True
        '
        'TextBoxDescripcion
        '
        Me.TextBoxDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxDescripcion.Location = New System.Drawing.Point(16, 224)
        Me.TextBoxDescripcion.Name = "TextBoxDescripcion"
        Me.TextBoxDescripcion.Size = New System.Drawing.Size(247, 21)
        Me.TextBoxDescripcion.TabIndex = 7
        '
        'ButtonBusNombre
        '
        Me.ButtonBusNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusNombre.Location = New System.Drawing.Point(16, 151)
        Me.ButtonBusNombre.Name = "ButtonBusNombre"
        Me.ButtonBusNombre.Size = New System.Drawing.Size(75, 23)
        Me.ButtonBusNombre.TabIndex = 8
        Me.ButtonBusNombre.Text = "Buscar"
        Me.ButtonBusNombre.UseVisualStyleBackColor = True
        '
        'ButtonBusDescripcion
        '
        Me.ButtonBusDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonBusDescripcion.Location = New System.Drawing.Point(16, 251)
        Me.ButtonBusDescripcion.Name = "ButtonBusDescripcion"
        Me.ButtonBusDescripcion.Size = New System.Drawing.Size(75, 23)
        Me.ButtonBusDescripcion.TabIndex = 9
        Me.ButtonBusDescripcion.Text = "Buscar"
        Me.ButtonBusDescripcion.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 106)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 15)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Nombre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 206)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 15)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Descripción"
        '
        'CIDEncuesta
        '
        Me.CIDEncuesta.DataPropertyName = "IDEncuesta"
        Me.CIDEncuesta.HeaderText = "Encuesta"
        Me.CIDEncuesta.Name = "CIDEncuesta"
        Me.CIDEncuesta.ReadOnly = True
        '
        'CNombre
        '
        Me.CNombre.DataPropertyName = "Nombre"
        Me.CNombre.HeaderText = "Nombre"
        Me.CNombre.Name = "CNombre"
        Me.CNombre.ReadOnly = True
        Me.CNombre.Width = 200
        '
        'CActiva
        '
        Me.CActiva.DataPropertyName = "Activa"
        Me.CActiva.HeaderText = "Activa"
        Me.CActiva.Name = "CActiva"
        Me.CActiva.ReadOnly = True
        Me.CActiva.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CActiva.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'CDescripcion
        '
        Me.CDescripcion.DataPropertyName = "Descripcion"
        Me.CDescripcion.HeaderText = "Descripcion"
        Me.CDescripcion.Name = "CDescripcion"
        Me.CDescripcion.ReadOnly = True
        Me.CDescripcion.Width = 500
        '
        'CFecha
        '
        Me.CFecha.DataPropertyName = "Fecha"
        Me.CFecha.HeaderText = "CFecha"
        Me.CFecha.Name = "CFecha"
        Me.CFecha.ReadOnly = True
        Me.CFecha.Visible = False
        '
        'BrwEncuestas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ButtonBusDescripcion)
        Me.Controls.Add(Me.ButtonBusNombre)
        Me.Controls.Add(Me.TextBoxDescripcion)
        Me.Controls.Add(Me.ButtonSalir)
        Me.Controls.Add(Me.ButtonModificar)
        Me.Controls.Add(Me.ButtonConsultar)
        Me.Controls.Add(Me.DataGridViewEncuestas)
        Me.Controls.Add(Me.TextBoxNombre)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonNuevo)
        Me.Name = "BrwEncuestas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo Encuestas"
        CType(Me.DataGridViewEncuestas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ButtonNuevo As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxNombre As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewEncuestas As System.Windows.Forms.DataGridView
    Friend WithEvents ButtonConsultar As System.Windows.Forms.Button
    Friend WithEvents ButtonModificar As System.Windows.Forms.Button
    Friend WithEvents ButtonSalir As System.Windows.Forms.Button
    Friend WithEvents TextBoxDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents ButtonBusNombre As System.Windows.Forms.Button
    Friend WithEvents ButtonBusDescripcion As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CIDEncuesta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CActiva As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CFecha As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
