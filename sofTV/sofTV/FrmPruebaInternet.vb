﻿Public Class FrmPruebaInternet

    Private Sub MUESTRACablemodesDelCliente(ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        dgvCablemodems.DataSource = BaseII.ConsultaDT("MUESTRACablemodesDelCliente")
    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        If Not IsNumeric(tbContrato.Text) Then
            Exit Sub
        End If
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, tbContrato.Text)
        cbServicio.DataSource = BaseII.ConsultaDT("MuestraServiciosEricPruebaInternet")
    End Sub

    Private Sub NUEtblPruebaInternet(ByVal Contrato As Integer, ByVal Clv_UnicaNet As Integer, ByVal Clv_Cablemodem As Integer, ByVal Clv_Servicio As Integer, ByVal Clv_ServicioPrueba As Integer, ByVal FechaInicio As DateTime, ByVal FechaFin As DateTime, ByVal Clv_Usuario As String)
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
            BaseII.CreateMyParameter("@Clv_UnicaNet", SqlDbType.Int, Clv_UnicaNet)
            BaseII.CreateMyParameter("@Clv_Cablemodem", SqlDbType.Int, Clv_Cablemodem)
            BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
            BaseII.CreateMyParameter("@Clv_ServicioPrueba", SqlDbType.Int, Clv_ServicioPrueba)
            BaseII.CreateMyParameter("@FechaInicio", SqlDbType.Date, FechaInicio)
            BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
            BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario, 5)
            BaseII.Inserta("NUEtblPruebaInternet")

            MessageBox.Show("Se realizó con éxito.")
            tbContrato.Text = ""
            MUESTRACablemodesDelCliente(0)
            MuestraServiciosEric(2, 0, 7)
            DAMEFECHADELSERVIDOR()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DAMEFECHADELSERVIDOR()
        Dim Fecha As DateTime
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", ParameterDirection.Output, SqlDbType.DateTime)
        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDOR")
        Fecha = DateTime.Parse(BaseII.dicoPar("@Fecha").ToString)
        dtpInicio.Value = Fecha
        dtpFin.Value = Fecha
    End Sub

    Private Sub FrmPruebaInternet_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraServiciosEric(2, 0, 7)
        DAMEFECHADELSERVIDOR()
    End Sub

    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        MUESTRACablemodesDelCliente(tbContrato.Text)
        MuestraServiciosEric(2, 0, 7)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvCablemodems.RowCount = 0 Then
            MessageBox.Show("Selecciona un cablemodem del Cliente.")
            Exit Sub
        End If
        If cbServicio.Text.Length = 0 Then
            MessageBox.Show("Selecciona un Servicio de prueba.")
            Exit Sub
        End If
        If cbServicio.SelectedValue = 0 Then
            MessageBox.Show("Selecciona un Servicio de prueba.")
            Exit Sub
        End If
        If dtpInicio.Value > dtpFin.Value Then
            MessageBox.Show("La fecha inico no se puede ser mayor a la fecha fin.")
            Exit Sub
        End If
        NUEtblPruebaInternet(tbContrato.Text, dgvCablemodems.SelectedCells(0).Value, dgvCablemodems.SelectedCells(1).Value, dgvCablemodems.SelectedCells(3).Value, cbServicio.SelectedValue, dtpInicio.Value, dtpFin.Value, GloUsuario)
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class