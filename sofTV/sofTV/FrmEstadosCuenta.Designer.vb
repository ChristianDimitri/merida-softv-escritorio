﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEstadosCuenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.bnVer = New System.Windows.Forms.Button()
        Me.dgEstadoCuenta = New System.Windows.Forms.DataGridView()
        Me.IdEstadoCuenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalAPagar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.btnReenviar = New System.Windows.Forms.Button()
        CType(Me.dgEstadoCuenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(9, 13)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(127, 15)
        Me.CMBLabel2.TabIndex = 9
        Me.CMBLabel2.Text = "Estados de Cuenta"
        '
        'bnVer
        '
        Me.bnVer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnVer.Location = New System.Drawing.Point(796, 31)
        Me.bnVer.Name = "bnVer"
        Me.bnVer.Size = New System.Drawing.Size(114, 28)
        Me.bnVer.TabIndex = 8
        Me.bnVer.Text = "Exportar"
        Me.bnVer.UseVisualStyleBackColor = True
        '
        'dgEstadoCuenta
        '
        Me.dgEstadoCuenta.AllowUserToAddRows = False
        Me.dgEstadoCuenta.AllowUserToDeleteRows = False
        Me.dgEstadoCuenta.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstadoCuenta.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgEstadoCuenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstadoCuenta.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdEstadoCuenta, Me.Contrato, Me.Nombre, Me.TotalAPagar, Me.Fecha})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgEstadoCuenta.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgEstadoCuenta.Location = New System.Drawing.Point(12, 31)
        Me.dgEstadoCuenta.Name = "dgEstadoCuenta"
        Me.dgEstadoCuenta.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgEstadoCuenta.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgEstadoCuenta.RowHeadersVisible = False
        Me.dgEstadoCuenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEstadoCuenta.Size = New System.Drawing.Size(765, 332)
        Me.dgEstadoCuenta.TabIndex = 7
        '
        'IdEstadoCuenta
        '
        Me.IdEstadoCuenta.DataPropertyName = "IdEstadoCuenta"
        Me.IdEstadoCuenta.HeaderText = "IdEstadoCuenta"
        Me.IdEstadoCuenta.Name = "IdEstadoCuenta"
        Me.IdEstadoCuenta.ReadOnly = True
        Me.IdEstadoCuenta.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 450
        '
        'TotalAPagar
        '
        Me.TotalAPagar.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.TotalAPagar.DataPropertyName = "TotalAPagar"
        DataGridViewCellStyle6.Format = "C2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TotalAPagar.DefaultCellStyle = DataGridViewCellStyle6
        Me.TotalAPagar.HeaderText = "Total"
        Me.TotalAPagar.Name = "TotalAPagar"
        Me.TotalAPagar.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.bnSalir.Location = New System.Drawing.Point(796, 335)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(114, 28)
        Me.bnSalir.TabIndex = 10
        Me.bnSalir.Text = "&Salir"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'btnReenviar
        '
        Me.btnReenviar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReenviar.Location = New System.Drawing.Point(796, 65)
        Me.btnReenviar.Name = "btnReenviar"
        Me.btnReenviar.Size = New System.Drawing.Size(114, 28)
        Me.btnReenviar.TabIndex = 11
        Me.btnReenviar.Text = "Reenviar"
        Me.btnReenviar.UseVisualStyleBackColor = True
        '
        'FrmEstadosCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 384)
        Me.Controls.Add(Me.btnReenviar)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.bnVer)
        Me.Controls.Add(Me.dgEstadoCuenta)
        Me.Name = "FrmEstadosCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Estados de Cuenta"
        CType(Me.dgEstadoCuenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents bnVer As System.Windows.Forms.Button
    Friend WithEvents dgEstadoCuenta As System.Windows.Forms.DataGridView
    Friend WithEvents IdEstadoCuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalAPagar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents btnReenviar As System.Windows.Forms.Button
End Class
