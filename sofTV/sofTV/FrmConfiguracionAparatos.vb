﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Public Class FrmConfiguracionAparatos

    Private Sub FrmConfiguracionAparatos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaServiciosAparatos()
        llenaServiciosAparatosAdicionales()
    End Sub

    Private Function llenaServiciosAparatos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)

        Dim DS As New DataSet
        DS.Clear()
        Dim listatablas As New List(Of String)
        listatablas.Add("SinAsignar")
        listatablas.Add("Asignada")

        DS = BaseII.ConsultaDS("DameRelServiciosAparatos", listatablas)

        ComboBox1.DataSource = DS.Tables.Item("SinAsignar")
        ComboBox1.DisplayMember = "concepto"
        ComboBox1.ValueMember = "TipoAparato"

        DataGridView1.DataSource = DS.Tables.Item("Asignada")
    End Function

    Private Function llenaServiciosAparatosAdicionales()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)

        Dim DS As New DataSet
        DS.Clear()
        Dim listatablas As New List(Of String)
        listatablas.Add("SinAsignar")
        listatablas.Add("Asignada")

        DS = BaseII.ConsultaDS("DameRelServiciosAparatosAdicionales", listatablas)

        ComboBox2.DataSource = DS.Tables.Item("SinAsignar")
        ComboBox2.DisplayMember = "concepto"
        ComboBox2.ValueMember = "TipoAparato"

        DataGridView2.DataSource = DS.Tables.Item("Asignada")
    End Function

    Private Sub BtnAgregar_Click(sender As Object, e As EventArgs) Handles BtnAgregar.Click
        Try
            If Not ComboBox1.SelectedValue.ToString.Length > 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, ComboBox1.SelectedValue, 5)
        BaseII.Inserta("insertaRelServiciosAparatos")
        llenaServiciosAparatos()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If Not DataGridView1.SelectedRows(0).Cells("TipoAparato").Value.ToString().Length > 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, DataGridView1.SelectedRows(0).Cells("TipoAparato").Value.ToString(), 5)
        BaseII.Inserta("DeleteRelServiciosAparatos")
        llenaServiciosAparatos()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            If Not ComboBox2.SelectedValue.ToString.Length > 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, ComboBox2.SelectedValue, 5)
        BaseII.Inserta("insertaRelServiciosAparatosAdicional")
        llenaServiciosAparatosAdicionales()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            If Not DataGridView2.SelectedRows(0).Cells("TipoAparatoAdicional").Value.ToString().Length > 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.BigInt, GloClv_Servicio)
        BaseII.CreateMyParameter("@TipoAparato", SqlDbType.VarChar, DataGridView2.SelectedRows(0).Cells("TipoAparatoAdicional").Value.ToString(), 5)
        BaseII.Inserta("DeleteRelServiciosAparatosAdicional")
        llenaServiciosAparatosAdicionales()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class