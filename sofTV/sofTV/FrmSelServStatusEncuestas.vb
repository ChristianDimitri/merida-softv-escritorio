﻿Public Class FrmSelServStatusEncuestas

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If Me.ComboBox1.SelectedValue = 0 Then
            MsgBox("Selecciona un Tipo de Servicio", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CheckBox9.Checked = False And CheckBox8.Checked = False Then
            MsgBox("Seleccione un Tipo de Busqueda", MsgBoxStyle.Information)
            Exit Sub
        End If

        If CheckBox8.Checked = True Then
            If CheckBox1.Checked = False And CheckBox2.Checked = False And CheckBox3.Checked = False And CheckBox4.Checked = False And CheckBox5.Checked = False And CheckBox6.Checked = False And CheckBox7.Checked = False Then
                MsgBox("Selecciona un Status", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If

        If CheckBox9.Checked = True Then
            If CheckBox10.Checked = False And CheckBox11.Checked = False And CheckBox12.Checked = False Then
                MsgBox("Seleccione un Tipo de fecha", MsgBoxStyle.Information)
                Exit Sub
            End If

            If DateTimePicker1.Value > DateTimePicker2.Value Then
                MsgBox("La fecha inicial no puede ser mayor a la fecha final", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If


        GuardaServStatusEncuestas()

        If GloIdEncuesta = 0 Then
            MsgBox("No se pudo almecenar los datos", MsgBoxStyle.Exclamation)
        End If

        FrmSelEncuesta.Show()
        Me.Close()
    End Sub

    Private Sub GuardaServStatusEncuestas()
        Dim Status As String = ""
        Dim TipoBusqueda As Integer = 0
        Dim TipoFecha As Integer = 0

        If CheckBox8.Checked = True Then
            TipoBusqueda = 1
        ElseIf CheckBox9.Checked = True Then
            TipoBusqueda = 2
        End If

        If CheckBox1.Checked = True Then
            Status = "C"
        ElseIf CheckBox2.Checked = True Then
            Status = "B"
        ElseIf CheckBox3.Checked = True Then
            Status = "I"
        ElseIf CheckBox4.Checked = True Then
            Status = "D"
        ElseIf CheckBox5.Checked = True Then
            Status = "S"
        ElseIf CheckBox6.Checked = True Then
            Status = "F"
        ElseIf CheckBox7.Checked = True Then
            Status = "T"
        End If

        If CheckBox10.Checked = True Then
            TipoFecha = 1
        ElseIf CheckBox11.Checked = True Then
            TipoFecha = 2
        ElseIf CheckBox12.Checked = True Then
            TipoFecha = 3
        End If

        GloIdEncuesta = 0

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_Tipser", SqlDbType.Int, ComboBox1.SelectedValue)
        BaseII.CreateMyParameter("@TipoBusqueda", SqlDbType.Int, TipoBusqueda)
        BaseII.CreateMyParameter("@Status", SqlDbType.VarChar, Status)
        BaseII.CreateMyParameter("@TipoFecha", SqlDbType.Int, TipoFecha)
        BaseII.CreateMyParameter("@fecha_Ini", SqlDbType.Date, DateTimePicker1.Value)
        BaseII.CreateMyParameter("@fecha_Fin", SqlDbType.Date, DateTimePicker2.Value)
        BaseII.CreateMyParameter("@UsuarioGenera", SqlDbType.VarChar, GloUsuario, 20)
        BaseII.CreateMyParameter("@id_Encuesta", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Usp_InsertServStatusEncuestas")
        GloIdEncuesta = BaseII.dicoPar("@id_Encuesta")


    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmSelServStatusEncuestas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)

        BaseII.limpiaParametros()
        Me.ComboBox1.DataSource = BaseII.ConsultaDT("MuestraTipSerPrincipal_SER")

        Me.DateTimePicker1.MaxDate = Today
        Me.DateTimePicker2.MaxDate = Today
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            'CheckBox1.Checked = True
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked = True Then
            CheckBox1.Checked = False
            'CheckBox2.Checked = True
            CheckBox3.Checked = False
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked = True Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            'CheckBox3.Checked = True
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        End If
    End Sub

    Private Sub CheckBox4_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox4.CheckedChanged
        If CheckBox4.Checked = True Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            'CheckBox4.Checked = True
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        End If
    End Sub

    Private Sub CheckBox5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox5.CheckedChanged
        If CheckBox5.Checked = True Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            CheckBox4.Checked = False
            'CheckBox5.Checked = True
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        End If
    End Sub

    Private Sub CheckBox6_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox6.CheckedChanged
        If CheckBox6.Checked = True Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            'CheckBox6.Checked = True
            CheckBox7.Checked = False
        End If
    End Sub

    Private Sub CheckBox7_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox7.CheckedChanged
        If CheckBox7.Checked = True Then
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            'CheckBox7.Checked = True
        End If
    End Sub

    Private Sub CheckBox10_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox10.CheckedChanged
        If CheckBox10.Checked = True Then
            CheckBox11.Checked = False
            CheckBox12.Checked = False
        End If
    End Sub

    Private Sub CheckBox8_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If CheckBox8.Checked = True Then
            GroupBox1.Enabled = True
            GroupBox3.Enabled = False
            CheckBox9.Checked = False
            CheckBox10.Checked = False
            CheckBox11.Checked = False
            CheckBox12.Checked = False
        Else
            GroupBox1.Enabled = False
        End If
    End Sub

    Private Sub CheckBox9_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox9.CheckedChanged
        If CheckBox9.Checked = True Then
            GroupBox1.Enabled = False
            GroupBox3.Enabled = True
            CheckBox8.Checked = False
            CheckBox1.Checked = False
            CheckBox2.Checked = False
            CheckBox3.Checked = False
            CheckBox4.Checked = False
            CheckBox5.Checked = False
            CheckBox6.Checked = False
            CheckBox7.Checked = False
        Else
            GroupBox3.Enabled = False
        End If

    End Sub

    Private Sub CheckBox11_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox11.CheckedChanged
        If CheckBox11.Checked = True Then
            CheckBox10.Checked = False
            CheckBox12.Checked = False
        End If
    End Sub

    Private Sub CheckBox12_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox12.CheckedChanged
        If CheckBox12.Checked = True Then
            CheckBox10.Checked = False
            CheckBox11.Checked = False
        End If
    End Sub
End Class