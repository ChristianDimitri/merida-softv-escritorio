Imports System.Data.SqlClient
Public Class FrmSelTrabajosE

    Private Sub FrmSelTrabajosE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSelTrabajosTmpTableAdapter.Connection = CON
        Me.ConSelTrabajosTmpTableAdapter.Fill(Me.DataSetEric2.ConSelTrabajosTmp, eTipSer, eClv_Session, 0)
        CON.Close()
        eBndAtenTelGraf = True
    End Sub

    Private Sub Refrescar()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.ConSelTrabajosTmpTableAdapter.Connection = CON2
        Me.ConSelTrabajosTmpTableAdapter.Fill(Me.DataSetEric2.ConSelTrabajosTmp, 0, eClv_Session, 1)
        Me.ConSelTrabajosTableAdapter.Connection = CON2
        Me.ConSelTrabajosTableAdapter.Fill(Me.DataSetEric2.ConSelTrabajos, eClv_Session)
        CON2.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.DescripcionListBox.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.InsertarSelTrabajosTableAdapter.Connection = CON
            Me.InsertarSelTrabajosTableAdapter.Fill(Me.DataSetEric2.InsertarSelTrabajos, CInt(Me.DescripcionListBox.SelectedValue), eClv_Session, 0)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.DescripcionListBox.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.InsertarSelTrabajosTableAdapter.Connection = CON
            Me.InsertarSelTrabajosTableAdapter.Fill(Me.DataSetEric2.InsertarSelTrabajos, 0, eClv_Session, 1)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DescripcionListBox1.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.EliminarSelTrabajosTableAdapter.Connection = CON
            Me.EliminarSelTrabajosTableAdapter.Fill(Me.DataSetEric2.EliminarSelTrabajos, CInt(Me.DescripcionListBox1.SelectedValue), eClv_Session, 0)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DescripcionListBox1.Items.Count > 0 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.EliminarSelTrabajosTableAdapter.Connection = CON
            Me.EliminarSelTrabajosTableAdapter.Fill(Me.DataSetEric2.EliminarSelTrabajos, 0, eClv_Session, 1)
            CON.Close()
            Refrescar()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.DescripcionListBox1.Items.Count > 0 Then
            If eOpVentas = 50 Then
                FrmImprimirComision.Show()
            Else
                FrmSelUsuariosE.Show()
            End If
            Me.Close()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub
End Class