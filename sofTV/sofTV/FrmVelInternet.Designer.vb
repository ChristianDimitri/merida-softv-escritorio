﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmVelInternet
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Clv_MOTCANLabel = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONMotivoCancelacionBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Clv_EqTextBox = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.VelBajTextBox = New System.Windows.Forms.TextBox()
        Me.VelSubTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_MOTCANLabel
        '
        Me.Clv_MOTCANLabel.AutoSize = True
        Me.Clv_MOTCANLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_MOTCANLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Clv_MOTCANLabel.Location = New System.Drawing.Point(62, 33)
        Me.Clv_MOTCANLabel.Name = "Clv_MOTCANLabel"
        Me.Clv_MOTCANLabel.Size = New System.Drawing.Size(124, 15)
        Me.Clv_MOTCANLabel.TabIndex = 3
        Me.Clv_MOTCANLabel.Text = "Clave equivalente:"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONMotivoCancelacionBindingNavigatorSaveItem})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(407, 25)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(61, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        Me.BindingNavigatorDeleteItem.Visible = False
        '
        'CONMotivoCancelacionBindingNavigatorSaveItem
        '
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Name = "CONMotivoCancelacionBindingNavigatorSaveItem"
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Size = New System.Drawing.Size(105, 22)
        Me.CONMotivoCancelacionBindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Clv_EqTextBox
        '
        Me.Clv_EqTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_EqTextBox.Enabled = False
        Me.Clv_EqTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_EqTextBox.Location = New System.Drawing.Point(218, 33)
        Me.Clv_EqTextBox.Name = "Clv_EqTextBox"
        Me.Clv_EqTextBox.Size = New System.Drawing.Size(141, 21)
        Me.Clv_EqTextBox.TabIndex = 4
        Me.Clv_EqTextBox.TabStop = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label17.Location = New System.Drawing.Point(62, 62)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(147, 15)
        Me.Label17.TabIndex = 639
        Me.Label17.Text = "*Velocidad de subida:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label18.Location = New System.Drawing.Point(62, 86)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(148, 15)
        Me.Label18.TabIndex = 641
        Me.Label18.Text = "*Velocidad de bajada:"
        '
        'VelBajTextBox
        '
        Me.VelBajTextBox.BackColor = System.Drawing.Color.White
        Me.VelBajTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.VelBajTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VelBajTextBox.Location = New System.Drawing.Point(218, 83)
        Me.VelBajTextBox.MaxLength = 10
        Me.VelBajTextBox.Name = "VelBajTextBox"
        Me.VelBajTextBox.Size = New System.Drawing.Size(141, 21)
        Me.VelBajTextBox.TabIndex = 640
        '
        'VelSubTextBox
        '
        Me.VelSubTextBox.BackColor = System.Drawing.Color.White
        Me.VelSubTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.VelSubTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VelSubTextBox.Location = New System.Drawing.Point(218, 58)
        Me.VelSubTextBox.MaxLength = 10
        Me.VelSubTextBox.Name = "VelSubTextBox"
        Me.VelSubTextBox.Size = New System.Drawing.Size(141, 21)
        Me.VelSubTextBox.TabIndex = 638
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Clv_MOTCANLabel)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.VelBajTextBox)
        Me.GroupBox1.Controls.Add(Me.VelSubTextBox)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Clv_EqTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(407, 120)
        Me.GroupBox1.TabIndex = 642
        Me.GroupBox1.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(257, 165)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(102, 36)
        Me.Button5.TabIndex = 643
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(12, 195)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(111, 13)
        Me.Label1.TabIndex = 644
        Me.Label1.Text = "*Velocidad en Kbs"
        '
        'FrmVelInternet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 219)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(423, 257)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(423, 257)
        Me.Name = "FrmVelInternet"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Velocidad FTTH"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents CONMotivoCancelacionBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_EqTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents VelBajTextBox As System.Windows.Forms.TextBox
    Friend WithEvents VelSubTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Clv_MOTCANLabel As System.Windows.Forms.Label
End Class
