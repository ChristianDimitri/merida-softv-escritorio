using System;

namespace Softv.Entities
{
  /// <summary>
  /// Class                   : Softv.Entities.PrioridadQuejaEntity.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : PrioridadQueja entity
  /// File                    : PrioridadQuejaEntity.cs
  /// Creation date           : 23/06/2011
  /// Creation time           : 06:05:58 p.m.
  /// </summary>
  public class PrioridadQuejaEntity
  {
  
      #region attributes
  
      private int? _clvPrioridadQueja;
      /// <summary>
      /// clvPrioridadQueja
      /// </summary>
      public int? clvPrioridadQueja
      {
          get { return _clvPrioridadQueja; }
          set { _clvPrioridadQueja = value; }
      }
  
      private String _Descripcion;
      /// <summary>
      /// Descripcion
      /// </summary>
      public String Descripcion
      {
          get { return _Descripcion; }
          set { _Descripcion = value; }
      }

      #endregion

      #region Constructors
      /// <summary>
      /// Default constructor
      /// </summary>

      public PrioridadQuejaEntity()
      {
      }

      /// <summary>
      ///  Parameterized constructor
      /// </summary>
      /// <param name="clvPrioridadQueja"></param>
      /// <param name="Descripcion"></param>
      public PrioridadQuejaEntity(int?  clvPrioridadQueja,String Descripcion)
      {
          this._clvPrioridadQueja = clvPrioridadQueja;
          this._Descripcion = Descripcion;
      }
      #endregion

  }
}
