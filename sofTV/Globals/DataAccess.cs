﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;


namespace Globals
{
    /// <summary>
    /// Clase           : TestSimulator.DAO.Global.DataAccess
    /// Descripc      : Funciona como clase base para todos los objetos DAO, contiene elementos clave como cache, conexión y duración del caché
    /// Archivo       : DataAccess.cs
    /// Fecha crea  : martes, 22 de mayo de 2007
    /// Fecha ult     : martes, 22 de mayo de 2007
    /// Versión        : 1.0
    /// </summary>
    public abstract class DataAccess
    {
        private static string globalConectionString;

        /// <summary>
        /// Gets connection String from App.config or Web.config
        /// </summary>
        public static string GlobalConectionString
        {
            get
            {
                //if (globalConectionString == null)
                //{
                //    globalConectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                //    if (String.IsNullOrEmpty(globalConectionString))
                //        globalConectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\ASPNETDB.MDF;Integrated Security=True;User Instance=True";
                //}
                return globalConectionString;
            }

            set 
            {
                globalConectionString = value;
            }
        }

        #region Aquí están los métodos
        /// <summary>
        /// Método que ejecuta sentencias de acción
        /// </summary>
        protected int ExecuteNonQuery(DbCommand cmd)
        {
            return cmd.ExecuteNonQuery();
        }
        /// <summary>
        /// Método que ejecuta sentencias de selección de solo lectura
        /// </summary>
        protected IDataReader ExecuteReader(DbCommand cmd)
        {
            return ExecuteReader(cmd, CommandBehavior.Default);
        }
        /// <summary>
        /// Método que ejecuta sentencias de selección de solo lectura
        /// </summary>
        protected IDataReader ExecuteReader(DbCommand cmd, CommandBehavior behavior)
        {
            return cmd.ExecuteReader(behavior);
        }
        /// <summary>
        /// Método que obtiene un dato de la bd
        /// </summary>
        protected object ExecuteScalar(DbCommand cmd)
        {
            return cmd.ExecuteScalar();
        }
        #endregion
    }
}
